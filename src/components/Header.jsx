
import { useEffect, useState } from 'react'
import { Bars3Icon } from '@heroicons/react/24/outline'

export default function Header({sidebarOpen, setSidebarOpen}){

    const [top, setTop] = useState(true);
    useEffect(() => {
        const scrollHandler = () => {
        window.scrollY > 10 ? setTop(false) : setTop(true);
        };
        window.addEventListener("scroll", scrollHandler);
        return () => window.removeEventListener("scroll", scrollHandler);
    }, [top]);

    return(
        <div className={`sticky top-0 z-10 flex shrink-0 items-center gap-x-4 px-4 py-4 sm:gap-x-6 sm:px-6 lg:px-8 bg-theme-gray2 ${
        !top && `shadow-sm`
      }`}>
            <button type="button" className="-m-2.5 p-2.5 text-gray-700 lg:hidden" onClick={() => setSidebarOpen(!sidebarOpen)}>
              <span className="sr-only">Open sidebar</span>
              <Bars3Icon className="h-6 w-6" aria-hidden="true" />
            </button>

            {/* Separator */}
            <div className="h-6 w-px bg-gray-200 lg:hidden" aria-hidden="true" />

            <div className="flex flex-1 gap-x-4 self-stretch lg:gap-x-6">
              <form className="relative flex flex-1" action="#" method="GET">
                <div className="box-wrapper w-full flex items-center">
                    <div className="flex items-center w-full">
                      <span className='w-[200px]'>CryptoBind CertiLife</span>
                      {/* <div className='bg-white rounded-full w-full flex p-3 border border-gray-200'>
                        <input type="search" name="" id="" placeholder="search for images" x-model="q" className="w-full pl-4 text-sm outline-none focus:outline-none bg-transparent" />
                        <button className="outline-none focus:outline-none px-4">
                          <svg className=" w-5 text-gray-600 h-5 cursor-pointer" fill="none" strokeWidth="2" stroke="currentColor" viewBox="0 0 24 24"><path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path></svg>
                        </button>
                        <div className="select">
                          <select name="" id="" x-model="image_type" className="text-sm outline-none focus:outline-none bg-transparent">
                            <option value="all" defaultValue>All</option>
                            <option value="Resource Name">Resource Name</option>
                            <option value="SSH User Name">SSH User Name</option>
                            <option value="SSH Key Name">SSH Key Name</option>
                            <option value="Common Name">Common Name</option>
                            <option value="SAN">SAN</option>
                            <option value="DNS Name / IP Address">DNS Name / IP Address</option>
                          </select>
                        </div>
                      </div> */}
                      
                    </div>
                </div>
              </form>
              {/* <div className="flex items-center gap-x-4 lg:gap-x-6">
                <button className='bg-theme-red1 text-white hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center'>
                  <span>Download</span>
                  <span>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 15" fill="currentColor" className="h-4 w-4 shrink-0">
                      <path d="M11.25 7.262C11.4087 7.262 11.544 7.32267 11.656 7.444C11.7773 7.556 11.838 7.69133 11.838 7.85V10.188C11.838 10.5053 11.7587 10.7947 11.6 11.056C11.4413 11.3267 11.2267 11.5413 10.956 11.7C10.6947 11.8587 10.4053 11.938 10.088 11.938H1.912C1.59467 11.938 1.30067 11.8587 1.03 11.7C0.768667 11.5413 0.558667 11.3267 0.4 11.056C0.241333 10.7947 0.162 10.5053 0.162 10.188V7.85C0.162 7.69133 0.218 7.556 0.33 7.444C0.451333 7.32267 0.591333 7.262 0.75 7.262C0.908667 7.262 1.044 7.32267 1.156 7.444C1.27733 7.556 1.338 7.69133 1.338 7.85V10.188C1.338 10.3467 1.394 10.482 1.506 10.594C1.618 10.706 1.75333 10.762 1.912 10.762H10.088C10.2467 10.762 10.382 10.706 10.494 10.594C10.606 10.482 10.662 10.3467 10.662 10.188V7.85C10.662 7.69133 10.718 7.556 10.83 7.444C10.9513 7.32267 11.0913 7.262 11.25 7.262ZM5.58 8.27L3.256 5.932C3.16267 5.85733 3.09733 5.76867 3.06 5.666C3.02267 5.56333 3.018 5.456 3.046 5.344C3.074 5.232 3.12533 5.13867 3.2 5.064C3.284 4.98 3.382 4.924 3.494 4.896C3.606 4.868 3.71333 4.87267 3.816 4.91C3.91867 4.94733 4.00733 5.01267 4.082 5.106L5.412 6.45V0.85C5.412 0.691333 5.468 0.556 5.58 0.444C5.70133 0.322666 5.84133 0.262 6 0.262C6.15867 0.262 6.294 0.322666 6.406 0.444C6.52733 0.556 6.588 0.691333 6.588 0.85V6.45L7.918 5.106C8.03933 5.01267 8.17 4.97067 8.31 4.98C8.45933 4.98933 8.58533 5.05 8.688 5.162C8.8 5.26467 8.86067 5.39067 8.87 5.54C8.87933 5.68 8.83733 5.81067 8.744 5.932L6.42 8.27C6.364 8.31667 6.29867 8.354 6.224 8.382C6.07467 8.44733 5.92533 8.44733 5.776 8.382C5.70133 8.354 5.636 8.31667 5.58 8.27Z" />
                    </svg>
                  </span>  
                </button>

                <button className='bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center'>
                  <span>Feedback</span>
                  <span>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 15" fill="currentColor" className="h-4 w-4 shrink-0">
                      <path d="M8.912 3.188C9.052 3.20667 9.16867 3.272 9.262 3.384C9.36467 3.496 9.416 3.62667 9.416 3.776C9.416 3.916 9.36467 4.042 9.262 4.154C9.16867 4.266 9.052 4.33133 8.912 4.35H3.088C2.948 4.33133 2.82667 4.266 2.724 4.154C2.63067 4.042 2.584 3.916 2.584 3.776C2.584 3.62667 2.63067 3.496 2.724 3.384C2.82667 3.272 2.948 3.20667 3.088 3.188H8.912ZM8.912 5.512C9.07067 5.512 9.206 5.57267 9.318 5.694C9.43933 5.806 9.5 5.94133 9.5 6.1C9.5 6.25867 9.43933 6.39867 9.318 6.52C9.206 6.632 9.07067 6.688 8.912 6.688H3.088C2.92933 6.688 2.78933 6.632 2.668 6.52C2.556 6.39867 2.5 6.25867 2.5 6.1C2.5 5.94133 2.556 5.806 2.668 5.694C2.78933 5.57267 2.92933 5.512 3.088 5.512H8.912ZM10.088 0.262C10.4053 0.262 10.6947 0.341333 10.956 0.499999C11.2267 0.658666 11.4413 0.873333 11.6 1.144C11.7587 1.40533 11.838 1.69467 11.838 2.012V11.35C11.838 11.4713 11.8053 11.5787 11.74 11.672C11.6747 11.7653 11.586 11.8353 11.474 11.882C11.3993 11.9193 11.3247 11.938 11.25 11.938C11.082 11.938 10.942 11.882 10.83 11.77L8.674 9.6H1.912C1.59467 9.6 1.30067 9.52067 1.03 9.362C0.768667 9.20333 0.558667 8.99333 0.4 8.732C0.241333 8.46133 0.162 8.16733 0.162 7.85V2.012C0.162 1.69467 0.241333 1.40533 0.4 1.144C0.558667 0.873333 0.768667 0.658666 1.03 0.499999C1.30067 0.341333 1.59467 0.262 1.912 0.262H10.088ZM10.662 9.95V2.012C10.662 1.85333 10.606 1.718 10.494 1.606C10.382 1.494 10.2467 1.438 10.088 1.438H1.912C1.75333 1.438 1.618 1.494 1.506 1.606C1.394 1.718 1.338 1.85333 1.338 2.012V7.85C1.338 8.00867 1.394 8.14867 1.506 8.27C1.618 8.382 1.75333 8.438 1.912 8.438H8.912C9.08 8.438 9.22 8.494 9.332 8.606L10.662 9.95Z" />
                    </svg>
                  </span>  
                </button>
                <button type="button" className="-m-2.5 p-2.5 text-gray-400 hover:text-gray-500">
                  <span className="sr-only">View notifications</span>
                  <BellIcon className="h-6 w-6" aria-hidden="true" />
                </button>

                <div className="hidden lg:block lg:h-6 lg:w-px lg:bg-gray-200" aria-hidden="true" />

                <Menu as="div" className="relative">
                  <Menu.Button className="-m-1.5 flex items-center p-1.5">
                    <span className="sr-only">Open user menu</span>
                    <img
                      className="h-8 w-8 rounded-full bg-gray-50"
                      src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
                      alt=""
                    />
                    <span className="hidden lg:flex lg:items-center">
                      <span className="ml-4 text-sm font-semibold leading-6 text-gray-900" aria-hidden="true">
                        Tom Cook
                      </span>
                      <ChevronDownIcon className="ml-2 h-5 w-5 text-gray-400" aria-hidden="true" />
                    </span>
                  </Menu.Button>
                  <Transition
                    as={Fragment}
                    enter="transition ease-out duration-100"
                    enterFrom="transform opacity-0 scale-95"
                    enterTo="transform opacity-100 scale-100"
                    leave="transition ease-in duration-75"
                    leaveFrom="transform opacity-100 scale-100"
                    leaveTo="transform opacity-0 scale-95"
                  >
                    <Menu.Items className="absolute right-0 z-10 mt-2.5 w-32 origin-top-right rounded-md bg-white py-2 shadow-lg ring-1 ring-gray-900/5 focus:outline-none">
                      {userNavigation.map((item) => (
                        <Menu.Item key={item.name}>
                          {({ active }) => (
                            <a
                              href={item.href}
                              className={classNames(
                                active ? 'bg-gray-50' : '',
                                'block px-3 py-1 text-sm leading-6 text-gray-900'
                              )}
                            >
                              {item.name}
                            </a>
                          )}
                        </Menu.Item>
                      ))}
                    </Menu.Items>
                  </Transition>
                </Menu>


              </div> */}
            </div>
          </div>
    )
}