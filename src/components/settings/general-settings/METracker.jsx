import { Link, useOutletContext } from 'react-router-dom'
import Header from '../../Header'
import HelpBox from '../../HelpBox';
import { useState } from 'react';

import { 
    ArrowLeftIcon,
    MagnifyingGlassIcon,
    InformationCircleIcon,
    ClockIcon,
    } from '@heroicons/react/24/outline';

export default function METracker(){
    const [sidebarOpen, setSidebarOpen, setSidebarShrink, sidebarShrink] = useOutletContext();
    const [password, setPassword] = useState('password');

    const helpTxt = [
        "Specify the server name, port number, user credentials, and the path of the load balancer from which certificates have to be discovered. currently supports certificate discovery from Nginx and F5 load balancers.",
        "Choose the Select Key option to access password-less resources through key based authentication. Upload the private key associated with the required account and specify the key passphrase.",
        "Discover certificate list fetches all the certificates available in the specified path and allows you to choose certificates that need to be imported.",
        "Certificate files with extensions .keystore and .pfx are grouped separately under JKS / PKCS. <br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To import these certificates, click JKS / PKCS, choose the certificate files that you wish to import, provide the file passphrase and click Import.",
    ]

    return (
        <div>
            <div className="px-4 sm:px-6 lg:px-8">
                <div className="text-xl theme-gray8 flex w-full">
                    <h1 className="text-xl theme-gray8  flex w-full gap-3 items-center">
                        <span>ME Tracker</span>
                    </h1>
                </div>
            </div>
                    
                    <div className="px-4 sm:px-6 lg:px-8 py-5">
                        <hr />
                    </div>  

                    <div className="px-4 sm:px-6 lg:px-8 py-3">
                        <div className="grid grid-cols-[30%_70%] max-w-lg">
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Select Certificate
                            </label>
                            <div>
                                 <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="file"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                    
                                </div>
                                <small>(File format should be .cer/.crt/.pfx/.p12/.pkcs12/.pem/.der/.jks/.keystore)</small>
                            </div>
                           
                        </div>  

                        <div className="grid grid-cols-[30%_70%] max-w-lg mb-7">
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Server Port
                            </label>
                            <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                <input
                                    type="text"
                                    autoComplete=""
                                    className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                    placeholder=""
                                />
                            </div>
                        </div>  
                        
                        <div className="grid grid-cols-[30%_70%] max-w-lg">
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Dashboard Type
                            </label>
                            <div className="flex max-w-md w-full">
                                <legend className="sr-only">Dashboard Type</legend>
                                <div className="space-y-4 sm:flex sm:items-center sm:space-x-10 sm:space-y-0">
                                    <div className="flex items-center">
                                        <input
                                            id="password"
                                            name="password"
                                            type="radio"
                                            defaultChecked={password === 'password'}
                                            value="password"
                                            onClick={() => setPassword('password')}
                                            className="accent-theme-red1 h-4 w-4 border-gray-300"
                                        />
                                        <label htmlFor="password" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                            SSH
                                        </label>
                                    </div>
                                    <div className="flex items-center">
                                        <input
                                            id="select-key"
                                            name="password"
                                            type="radio"
                                            defaultChecked={password === 'select-key'}
                                            value="select-key"
                                            onClick={() => setPassword('select-key')}
                                            className="accent-theme-red1 h-4 w-4 border-gray-300 text-theme-red1 focus:ring-theme-red1"
                                        />
                                        <label htmlFor="select-key" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                            SSL
                                        </label>
                                    </div>
                                    <div className="flex items-center">
                                        <input
                                            id="select-key"
                                            name="password"
                                            type="radio"
                                            value="select-key"
                                            className="accent-theme-red1 h-4 w-4 border-gray-300 text-theme-red1 focus:ring-theme-red1"
                                        />
                                        <label htmlFor="select-key" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                            Both
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>


                                             
                    <div className="px-4 sm:px-6 lg:px-8 py-5">
                        <hr />
                    </div>     

                    <div className="px-4 sm:px-6 lg:px-8 py-3">
                        <button className='hover:shadow-lg transition duration-300 ease-in-out bg-black text-white hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center'>
                            <span>Save</span>
                        </button>
                    </div>       

                    <HelpBox helpTxt={helpTxt}/>                          
        
      </div> 
    )
}