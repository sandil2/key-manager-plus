import { Fragment, useState } from 'react'
import { Dialog, Disclosure, Menu, Popover, Transition } from '@headlessui/react'
import { XMarkIcon } from '@heroicons/react/24/outline'
import { ChevronDownIcon } from '@heroicons/react/20/solid'

const filters = [
  {
    id: 'category',
    name: 'Show',
    options: [
            
      { value: 'expired', label: 'Expired' },
      { value: 'revoked', label: 'Revoked' },
      { value: 'rejected', label: 'Rejected' },
      { value: 'other', label: 'Other' },
    ],
  },
]

function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}

export default function Example() {
  const [open, setOpen] = useState(false)

  return (
        <div className="mx-auto max-w-3xl text-center lg:max-w-7xl">
            <div className="flex items-center justify-between">
                <Popover.Group className="hidden sm:flex sm:items-baseline sm:space-x-8">
                {filters.map((section, sectionIdx) => (
                    <Popover
                    as="div"
                    key={section.name}
                    id={`desktop-menu-${sectionIdx}`}
                    className="relative inline-block text-left"
                    >
                    <div >
                        <Popover.Button className="text-theme-gray3 inline-flex w-full justify-center rounded-full px-4 py-3 font-medium border border-theme-gray2 text-theme-default-gray focus:outline-none">
                        <span>{section.name}</span>
                        <ChevronDownIcon
                            className="-mr-1 ml-1 h-5 w-5 flex-shrink-0 text-gray-400 group-hover:text-gray-500"
                            aria-hidden="true"
                        />
                        </Popover.Button>
                    </div>

                    <Transition
                        as={Fragment}
                        enter="transition ease-out duration-100"
                        enterFrom="transform opacity-0 scale-95"
                        enterTo="transform opacity-100 scale-100"
                        leave="transition ease-in duration-75"
                        leaveFrom="transform opacity-100 scale-100"
                        leaveTo="transform opacity-0 scale-95"
                    >
                        <Popover.Panel className="absolute right-0 z-10 mt-2 origin-top-right rounded-md bg-white p-4 shadow-2xl ring-1 ring-black ring-opacity-5 focus:outline-none">
                        <form className="space-y-4">
                            {section.options.map((option, optionIdx) => (
                            <div key={option.value} className="flex items-center">
                                <input
                                id={`filter-${section.id}-${optionIdx}`}
                                name={`${section.id}[]`}
                                defaultValue={option.value}
                                type="checkbox"
                                className="h-4 w-4 rounded border-gray-300 text-theme-red1 focus:text-theme-red1"
                                />
                                <label
                                htmlFor={`filter-${section.id}-${optionIdx}`}
                                className="ml-3 whitespace-nowrap pr-6 text-sm font-medium text-gray-900"
                                >
                                {option.label}
                                </label>
                            </div>
                            ))}
                        </form>
                        </Popover.Panel>
                    </Transition>
                    </Popover>
                ))}
                </Popover.Group>
            </div>
        </div>
  )
}
