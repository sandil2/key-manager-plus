import React, { useMemo, useState } from 'react';
import { Link } from 'react-router-dom';

import { 
    ArrowDownTrayIcon,
    UserGroupIcon, 
    ArrowLeftIcon,
    } from '@heroicons/react/24/outline';
    
export default function StoreSectigoOrderCertificate({setMasterTab}){
    const [selType,setSelType] = useState("dmz");
	return (
        <>
            <div className="px-4 sm:px-6 lg:px-8 items-center gap-4 text">
                <h1 className='text-xl theme-gray8 my-3 flex'>
                    <span>Order Certificate</span>
                    <small className='ml-auto'>
                        <Link to="/ssl/store-sectigo" className="hover:shadow-lg transition duration-300 ease-in-out bg-theme-gray2 rounded-full flex px-5 py-2 gap-3 items-center border border-theme-gray2 flex text-xs font-light">
                            <ArrowLeftIcon className="w-4 h-4"/> <span> Back</span>
                        </Link>
                    </small>
                </h1>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Common Name
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-xs">
                        Product Name
                    </label>

                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full max-w-sm">
                        <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                            <option value="RSA">RSA</option>
                            <option value="DSA">DSA</option>
                            <option value="ECDSA">ECDSA</option>
                            <option value="ED25519">ED25519</option>
                        </select>
                    </div>
                    
                    <label htmlFor="ip-address-range" className="text-xs">
                        Selection Certificates
                    </label>
                    <div className="flex w-full my-5">
                        <legend className="sr-only">Agents</legend>
                        <div className="flex w-full gap-7">
                            <div className="flex items-center">
                                <input
                                    id="dmz"
                                    name="discover-by"
                                    type="radio"
                                    defaultChecked={selType === 'dmz'}
                                    value="dmz"
                                    onClick={() => setSelType('dmz')}
                                    className="accent-theme-red1 h-4 w-4 border-gray-300"
                                />
                                <label htmlFor="dmz" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                    Select Specific Certificate
                                </label>
                            </div>
                            <div className="flex items-center">
                                <input
                                    id="cs"
                                    name="discover-by"
                                    type="radio"
                                    defaultChecked={selType === 'cs'}
                                    value="cs"
                                    onClick={() => setSelType('cs')}
                                    className="accent-theme-red1 h-4 w-4 border-gray-300 text-theme-red1 focus:ring-theme-red1"
                                />
                                <label htmlFor="cs" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                    By Criteria
                                </label>
                            </div>
                        </div>
                    </div>

                    <label htmlFor="ip-address-range" className="text-xs">
                        &nbsp;
                    </label>

                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full max-w-sm">
                        <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                            <option value="RSA">RSA</option>
                            <option value="DSA">DSA</option>
                            <option value="ECDSA">ECDSA</option>
                            <option value="ED25519">ED25519</option>
                        </select>
                    </div>
                    <label htmlFor="ip-address-range" className="text-xs">
                        Email Group
                    </label>

                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full max-w-sm">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>

                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Save</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </div>
        </>	
	);
};

