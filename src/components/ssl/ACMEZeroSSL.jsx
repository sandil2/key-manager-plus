import React, { useMemo, useState } from 'react';
import DataTable from 'react-data-table-component';
import HelpBox from '../HelpBox';
import Modal from '../Modal';
import { 
    CreditCardIcon,
    ArrowDownOnSquareIcon,
    ArrowUpOnSquareIcon,
    EyeIcon,
    EnvelopeIcon,
    MagnifyingGlassIcon,
    InformationCircleIcon,
    } from '@heroicons/react/24/outline';
import zeroSslLogo from "../../images/zero-ssl.svg" 
import Dropdown from '../Dropdown';
import { Menu } from '@headlessui/react';
import { Link } from 'react-router-dom';

export default function ACMEZeroSSL({setMasterTab}){
    const columns = [
        {
            name: 'Domain Names',
            selector: row => row.DomainName,
            sortable: true,  
        },
        {
            name: 'Serial Number',
            selector: row => row.SerialNumber,
            sortable: true,            
        },
        {
            name: 'Valid From',
            selector: row => row.ValidFrom,
            sortable: true,
        },
        {
            name: 'Valid To',
            selector: row => row.ValidTo,
            sortable: true,
        },
        {
            name: 'Requested By',
            selector: row => row.RequestedBy,
            sortable: true,
        },
        {
            name: 'Authorized',
            selector: row => row.Authorized,
            sortable: true,
        },
        {
            name: 'Processed By',
            selector: row => row.ProcessedBy,
            sortable: true,
        },
        {
            name: 'Requsted Status',
            selector: row => row.RequstedStatus,
            sortable: true,
        },
        {
            name: 'Certificate Status',
            selector: row => row.CertificateStatus,
            sortable: true,
        },
        {
            name: '',
            selector: row => row.CertificateHistory,
            sortable: false,
            maxWidth: "30px",
            minWidth: "50px"
        },
        {
            name: '',
            selector: row => row.PassPhrase,
            sortable: false,
            maxWidth: "30px",
            minWidth: "50px"
        },
    ];    

const data = [
    {
        DomainName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        SerialNumber :   "-",
        ValidFrom : "Jan 28, 2019 18:16",
        ValidTo : "Jan 28, 2019 18:16",
        RequestedBy : "demouser",
        Authorized : "demouser",
        ProcessedBy : "-",
        RequstedStatus : <a href="javascript:;">Pending</a>,
        CertificateStatus : <span className='min-w-12 bg-theme-gray2 p-2 block text-center rounded'>NA</span>,
        CertificateHistory: <CreditCardIcon className='w-5 h-5' />,
        PassPhrase:<EyeIcon className='w-5 h-5' />,
    }, 
    {
        DomainName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        SerialNumber :   "-",
        ValidFrom : "Jan 28, 2019 18:16",
        ValidTo : "Jan 28, 2019 18:16",
        RequestedBy : "demouser",
        Authorized : "demouser",
        ProcessedBy : "-",
        RequstedStatus : <a href="javascript:;">Pending</a>,
        CertificateStatus : <span className='min-w-12 bg-theme-gray2 p-2 block text-center rounded'>NA</span>,
        CertificateHistory: <CreditCardIcon className='w-5 h-5' />,
        PassPhrase:<EyeIcon className='w-5 h-5' />,
    }, 
    {
        DomainName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        SerialNumber :   "-",
        ValidFrom : "Jan 28, 2019 18:16",
        ValidTo : "Jan 28, 2019 18:16",
        RequestedBy : "demouser",
        Authorized : "demouser",
        ProcessedBy : "-",
        RequstedStatus : <a href="javascript:;">Pending</a>,
        CertificateStatus : <span className='min-w-12 bg-theme-gray2 p-2 block text-center rounded'>Expired</span>,
        CertificateHistory: <CreditCardIcon className='w-5 h-5' />,
        PassPhrase:<EyeIcon className='w-5 h-5' />,
    }, 
    {
        DomainName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        SerialNumber :   "-",
        ValidFrom : "Jan 28, 2019 18:16",
        ValidTo : "Jan 28, 2019 18:16",
        RequestedBy : "demouser",
        Authorized : "demouser",
        ProcessedBy : "-",
        RequstedStatus : <a href="javascript:;">Pending</a>,
        CertificateStatus : <span className='min-w-12 bg-theme-gray2 p-2 block text-center rounded'>Expired</span>,
        CertificateHistory: <CreditCardIcon className='w-5 h-5' />,
        PassPhrase:<EyeIcon className='w-5 h-5' />,
    }, 
    {
        DomainName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        SerialNumber :   "-",
        ValidFrom : "Jan 28, 2019 18:16",
        ValidTo : "Jan 28, 2019 18:16",
        RequestedBy : "demouser",
        Authorized : "demouser",
        ProcessedBy : "-",
        RequstedStatus : <a href="javascript:;">Pending</a>,
        CertificateStatus : <span className='min-w-12 bg-theme-gray2 p-2 block text-center rounded'>Expired</span>,
        CertificateHistory: <CreditCardIcon className='w-5 h-5' />,
        PassPhrase:<EyeIcon className='w-5 h-5' />,
    }, 
   
];

const [records,setRecords] = useState(data);

const [isOpenModal,setIsOpenModal] = useState(false);
const [isOpenModalDelete2,setIsOpenModalDelete2] = useState(false);
const [isOpenModalDNS,setIsOpenModalDNS] = useState(false);


function handleFilter(event){
    const newData = records.filter(row=>{
        return row.ResourceName.toLowerCase().includes(event.target.value.toLowerCase())
    })
    setRecords(newData)
}

	return (
        <>
            <div className="px-4 sm:px-6 lg:px-8 flex items-center text">
                <h1 className='text-xl theme-gray8 my-3 flex w-full gap-3 items-center'>
                   <span className='w-5 h-5 '><img src={zeroSslLogo} alt="" /></span><span>ZeroSSL</span>
                </h1>
            </div> 
            <div className="px-4 sm:px-6 lg:px-8">
            <div className='flex flex-wrap gap-1 my-4 text-sm'>
                <Link to="/ssl/zero-ssl-cr" className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                    <span>Certificate Request</span>
                </Link>

                <a href="javascript:;" className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                    <span>Renew Certificate</span>
                </a>

                <a href="javascript:;" className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                    <span>Revoke Certificate</span>
                </a>

                <div className='z-[10]'>
                    <Dropdown DropText={"More"}>
                        <div className="px-1 py-1 " role="none">
                            <Menu.Item onClick={()=>setIsOpenModalDNS(true)}>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Assign DNS
                                </button>
                                )}
                            </Menu.Item>
                            <Menu.Item onClick={()=>setIsOpenModalDelete2(true)}>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Delete
                                </button>
                                )} 
                            </Menu.Item>
                        </div>
                    </Dropdown>
                </div>
                <Link to="/ssl/zero-ssl-manage" className="ml-auto hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                    <span>Manage</span>
                </Link>
            </div>
            <div className='max-w-[500px] w-full mb-5 block'>
                <div className="flex">
                    <label htmlFor="search-dropdown" className="mb-2 text-sm font-medium text-gray-900 sr-only">Your Email</label>
                    <div className="relative w-full">
                        <input type="search" id="search-dropdown" className="block p-3 w-full z-20 text-sm text-gray-900 bg-gray-50 rounded-full border-s-2 border border-gray-300 focus:ring-blue-500 focus:border-blue-500" placeholder="Search..."/>
                        <button type="button" className="absolute top-0 end-0 p-3 text-sm font-medium h-full focus:ring-4 focus:outline-none focus:ring-blue-300">
                            <MagnifyingGlassIcon className='w-4 h-4'/>
                            <span className="sr-only">Search</span>
                        </button>
                    </div>
                </div>
            </div>
            
            <DataTable 
                columns={columns} 
                data={data} 
                fixedHeader
                pagination 
                selectableRows 
            />
            </div>

            <Modal title={'CSR Template'} isOpenModal={isOpenModal} setIsOpenModal={setIsOpenModal}>
                <label htmlFor="ip-address-range" className="block text-sm font-medium leading-6 text-gray-900 mb-7 sr-only">
                    CSR Template
                </label>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-2">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Sign Type
                    </label>
                    <div>
                        <div className='flex justify-between rounded-md shadow-sm ring-1 focus:ring-0 ring-inset ring-gray-300 w-full'>
                            <select
                            className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            >
                                <option>Windows</option>
                                <option>Linux/Mac OS</option>
                            </select>
                        </div>
                        <p className='text-right'>
                            <small>
                                <a href="javascript:;" className='text-theme-red1'>Manage CSR Templates</a>
                            </small>
                        </p>
                    </div>
                </div>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-2">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Server Name
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                </div>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-2">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Certificate Authority
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                </div>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-2">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Template Name / OID
                    </label>
                    <div>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                            <input
                                type="text"
                                autoComplete=""
                                className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                placeholder=""
                            />
                        </div>
                        <p className='text-right'>
                            <small>
                                <a href="javascript:;">Get Templates</a>
                            </small>
                        </p>
                    </div>
                    
                </div>
                
                <hr />
                <div className='w-full flex gap-5 py-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Sign</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <InformationCircleIcon className="w-4 h-4" /><small>If you choose the "Elevate to root user" option, ensure the resource has both "root" and non-root user credentials.</small>
                </div>  
                
            </Modal>
            <Modal title={'Confirmation'} isOpenModal={isOpenModalDelete2} setIsOpenModal={setIsOpenModalDelete2}>
                <div className="grid grid-cols-[100%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                        Delete selected Template(s)?
                    </label>
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Ok</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>
            <Modal title={'DNS'} isOpenModal={isOpenModalDNS} setIsOpenModal={setIsOpenModalDNS}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-2">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        DNS
                    </label>
                    <div>
                        <div className='flex justify-between rounded-md shadow-sm ring-1 focus:ring-0 ring-inset ring-gray-300 w-full'>
                            <select
                            className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            >
                                <option>Windows</option>
                                <option>Linux/Mac OS</option>
                            </select>
                        </div>
                    </div>
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Save</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>
        </>
		
	);
};

