import React, { useMemo, useState } from 'react';
import DataTable from 'react-data-table-component';
import HelpBox from '../HelpBox';
import Modal from '../Modal';

import AWSLogo from "../../images/aws.svg"

import { 
    CreditCardIcon,
    ArrowDownOnSquareIcon,
    ArrowUpOnSquareIcon,
    EyeIcon,
    EnvelopeIcon,
    ArrowLeftIcon,
    MagnifyingGlassIcon,
    
    } from '@heroicons/react/24/outline';
import Dropdown from '../Dropdown';
import { Menu } from '@headlessui/react';
import MultiSelectListBox from '../MultiSelectListBox';
import { Link } from 'react-router-dom';
import DropdownBorderless from '../DropdownBorderless';

export default function SslMSCA({setMasterTab}){
    const columns = [
        {
            name: 'Common Name',
            selector: row => row.CommonName,
            sortable: true,  
        },
        {
            name: 'Product Name',
            selector: row => row.ProductName,
            sortable: true,            
        },
        {
            name: 'Order Number',
            selector: row => row.OrderNumber,
            sortable: true,
        },
        {
            name: 'Valid From',
            selector: row => row.ValidFrom,
            sortable: true,
        },
        {
            name: 'Valid To',
            selector: row => row.ValidTo,
            sortable: true,
        },
        {
            name: 'Requested By',
            selector: row => row.RequestedBy,
            sortable: true,
        },
        {
            name: 'Purchase Date',
            selector: row => row.PurchaseDate,
            sortable: true,
            
        },
        {
            name: 'Status',
            selector: row => row.Status,
            sortable: true,
            
        },
        
    ];    

const data = [
    {
        CommonName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        ProductName :   "admin",
        OrderNumber : "1234567890",
        ValidFrom : "Jan 28, 2019 18:16",
        ValidTo :"Jan 28, 2019 18:16",
        RequestedBy : "JKS", 
        PurchaseDate : "May 27, 2020 11:49", 
        Status: <span className="inline-flex items-center rounded-full bg-red-50 px-2 py-1 text-xs font-medium text-red-700 ring-1 ring-inset ring-red-600/10">Expired</span>,    
    }, 
    {
        CommonName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        CertificateID :   "admin",
        CertificateType : "Jan 28, 2019 18:16",
        ValidFrom : "4096",
        ValidTo :"RSA",
        RequestedBy : "JKS", 
        PurchaseDate : "May 27, 2020 11:49", 
        Status:  <span className="inline-flex items-center rounded-full bg-yellow-50 px-2 py-1 text-xs font-medium text-yellow-800 ring-1 ring-inset ring-yellow-600/20">Pending</span>,    
    }, 
    {
        CommonName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        CertificateID :   "admin",
        CertificateType : "Jan 28, 2019 18:16",
        ValidFrom : "4096",
        ValidTo :"RSA",
        RequestedBy : "JKS", 
        PurchaseDate : "May 27, 2020 11:49", 
        Status: <span className="inline-flex items-center rounded-full bg-green-50 px-2 py-1 text-xs font-medium text-green-700 ring-1 ring-inset ring-green-600/20">Success</span>,    
    }, 
    {
        CommonName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        CertificateID :   "admin",
        CertificateType : "Jan 28, 2019 18:16",
        ValidFrom : "4096",
        ValidTo :"RSA",
        RequestedBy : "JKS", 
        PurchaseDate : "May 27, 2020 11:49", 
        Status: <span className="inline-flex items-center rounded-full bg-gray-50 px-2 py-1 text-xs font-medium text-gray-600 ring-1 ring-inset ring-gray-500/10">Other</span>,    
    }, 
  
];

const [records,setRecords] = useState(data);
const [isOpenModalDelete,setIsOpenModalDelete] = useState(false);
const [isOpenModalDelete2,setIsOpenModalDelete2] = useState(false);
const [isOpenModalImpK,setIsOpenModalImpK] = useState(false);
const [isOpenModalDcv,setIsOpenModalDcv] = useState(false);
const [isOpenModalRenew,setIsOpenModalRenew] = useState(false);
const [isOpenModalRevoke,setIsOpenModalRevoke] = useState(false);

function handleFilter(event){
    const newData = records.filter(row=>{
        return row.ResourceName.toLowerCase().includes(event.target.value.toLowerCase())
    })
    setRecords(newData)
}

	return (
        <>
            <div className="px-4 sm:px-6 lg:px-8">
            <div className='flex flex-wrap gap-1 my-4 text-sm'>
                <button onClick={()=>setIsOpenModalImpK(true)} className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                    <span>Request Certificate</span>
                </button>
                <button onClick={()=>setIsOpenModalDcv(true)} className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                    <span>Discover</span>
                </button>
                <button onClick={()=>setIsOpenModalRenew(true)} className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                    <span>Renew</span>
                </button>
                <button onClick={()=>setIsOpenModalRevoke(true)} className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                    <span>Revoke</span>
                </button>
                <button onClick={()=>setIsOpenModalDelete2(true)} className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                    <span>Delete</span>
                </button>
            </div>

            <div className='max-w-[500px] w-full mb-5 block'>
                <div className="flex">
                    <label htmlFor="search-dropdown" className="mb-2 text-sm font-medium text-gray-900 sr-only">Your Email</label>
                    <div className="relative w-full">
                        <input type="search" id="search-dropdown" className="block p-3 w-full z-20 text-sm text-gray-900 bg-gray-50 rounded-full border-s-2 border border-gray-300 focus:ring-blue-500 focus:border-blue-500" placeholder="Search..."/>
                        <button type="button" className="absolute top-0 end-0 p-3 text-sm font-medium h-full focus:ring-4 focus:outline-none focus:ring-blue-300">
                            <MagnifyingGlassIcon className='w-4 h-4'/>
                            <span className="sr-only">Search</span>
                        </button>
                    </div>
                </div>
            </div>
            
            <DataTable 
                columns={columns} 
                data={data} 
                fixedHeader
                pagination 
                selectableRows 
            />
            </div>
           
            <Modal title={'Confirmation'} isOpenModal={isOpenModalDelete2} setIsOpenModal={setIsOpenModalDelete2}>
                <div className="grid grid-cols-[100%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                        Dissociate key1 key from selected User?
                    </label>
                   
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Ok</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>
            <Modal title={'Confirmation'} isOpenModal={isOpenModalRenew} setIsOpenModal={setIsOpenModalRenew}>
                <div className="grid grid-cols-[100%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                        No private keys are available. Do you want to create one?
                    </label>
                   
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Ok</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>

            <Modal title={'Request Certificate'} isOpenModal={isOpenModalImpK} setIsOpenModal={setIsOpenModalImpK}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Values
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                            <option value="RSA">RSA</option>
                            <option value="DSA">DSA</option>
                            <option value="ECDSA">ECDSA</option>
                            <option value="ED25519">ED25519</option>
                        </select>
                    </div>
                   
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Key Name
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        SSH Key Passphrase
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Key Comment
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                   
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Add</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>
            <Modal title={'Discover Microsoft CA Certificate'} isOpenModal={isOpenModalDcv} setIsOpenModal={setIsOpenModalDcv}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Values
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                            <option value="RSA">RSA</option>
                            <option value="DSA">DSA</option>
                            <option value="ECDSA">ECDSA</option>
                            <option value="ED25519">ED25519</option>
                        </select>
                    </div>
                   
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Key Name
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        SSH Key Passphrase
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Key Comment
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                   
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Add</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>
            <Modal title={'Revoke MSCA Certificate'} isOpenModal={isOpenModalRevoke} setIsOpenModal={setIsOpenModalRevoke}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Values
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                            <option value="RSA">RSA</option>
                            <option value="DSA">DSA</option>
                            <option value="ECDSA">ECDSA</option>
                            <option value="ED25519">ED25519</option>
                        </select>
                    </div>
                   
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Key Name
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        SSH Key Passphrase
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Key Comment
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                   
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Add</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>
        </>
		
	);
};

