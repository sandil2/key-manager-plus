import React, { useMemo, useState } from 'react';
import { ChevronRightIcon } from '@heroicons/react/24/outline';
import { Menu } from '@headlessui/react';
import Dropdown from '../Dropdown';
import DataTable from 'react-data-table-component';
import { 
    ArrowDownTrayIcon,
    UserGroupIcon, 
    ArrowLeftIcon,
    } from '@heroicons/react/24/outline';
import Modal from '../Modal';
import HelpBox from '../HelpBox';
import { Link } from 'react-router-dom';

export default function SslCertificateWindowsAgents({setMasterTab}){

     const columns = [
        
        {
            name: 'Common Name',
            selector: row => row.CommonName,
            sortable: true,            
        },
        {
            name: 'DNS Name',
            selector: row => row.DNSName,
            sortable: true,
        },
        {
            name: 'Issuer',
            selector: row => row.Issuer,
            sortable: true,
        },
        {
            name: 'Valid To',
            selector: row => row.ValidTo,
            sortable: true,
        },
        {
            name: 'Key Size',
            selector: row => row.KeySize,
            sortable: true,
        },
        {
            name: 'Signature Algorithm',
            selector: row => row.SignatureAlgorithm,
            sortable: false,
        },
        
        {
            name: '',
            selector: row => row.IconKey,
            sortable: false,
            maxWidth: "30px",
            minWidth: "50px"
        },
    ];    

    const data = [
        {
            CommonName : <a href="javascript:;" className='text-theme-blue1'>	RootCA</a>  ,
            DNSName : "	RootCA",
            Issuer : "	RootCA",
            ValidTo :<span className=''>Feb 6, 2024 11:08</span>,
            KeySize: '4096',
            SignatureAlgorithm: 'SHA256',
            IconCertiHistory: <UserGroupIcon className='w-5 h-4 text-black-700 cursor-pointer'/>,
        },
        {
            CommonName : <a href="javascript:;" className='text-theme-blue1'>	RootCA</a>  ,
            DNSName : "	RootCA",
            Issuer : "	RootCA",
            ValidTo :<span className=''>Feb 6, 2024 11:08</span>,
            KeySize: '4096',
            SignatureAlgorithm: 'SHA256',
            IconCertiHistory: <UserGroupIcon className='w-5 h-4 text-black-700 cursor-pointer'/>,
        },
        {
            CommonName : <a href="javascript:;" className='text-theme-blue1'>	RootCA</a>  ,
            DNSName : "	RootCA",
            Issuer : "	RootCA",
            ValidTo :<span className=''>Feb 6, 2024 11:08</span>,
            KeySize: '4096',
            SignatureAlgorithm: 'SHA256',
            IconCertiHistory: <UserGroupIcon className='w-5 h-4 text-black-700 cursor-pointer'/>,
        },
        {
            CommonName : <a href="javascript:;" className='text-theme-blue1'>	RootCA</a>  ,
            DNSName : "	RootCA",
            Issuer : "	RootCA",
            ValidTo :<span className=''>Feb 6, 2024 11:08</span>,
            KeySize: '4096',
            SignatureAlgorithm: 'SHA256',
            IconCertiHistory: <UserGroupIcon className='w-5 h-4 text-black-700 cursor-pointer'/>,
        },
    ];

    const helpTxt = [
        "A CSR will be generated from the available values, along with a new Private Key.",
        "SHA1 certificates renewal happens using the SHA256 algorithm."
    ]
    
    const [discoverBy,setDiscoverBy] = useState("Days");
    const [CSR,setCSR] = useState("Create");
    
    const [advOptions,setAdvOptions] = useState(false);
    const [isOpenModalCRC,setIsOpenModalCRC] = useState(false);
    const [isOpenModalReqCerti,setIsOpenModalReqCerti] = useState(false);
    const [isOpenModalRenewC,setIsOpenModalRenewC] = useState(false);
    const [isOpenModalDelete,setIsOpenModalDelete] = useState(false);

	return (
        <>
            <div className="px-4 sm:px-6 lg:px-8 items-center gap-4 text">
                <h1 className='text-xl theme-gray8 my-3 flex'>
                    <span>Windows Agents</span>
                    <small className='ml-auto'>
                        <Link to="/ssl/certificate" className="hover:shadow-lg transition duration-300 ease-in-out bg-theme-gray2 rounded-full flex px-5 py-2 gap-3 items-center border border-theme-gray2 flex text-xs font-light">
                            <ArrowLeftIcon className="w-4 h-4"/> <span> Back</span>
                        </Link>
                    </small>
                </h1>
                <div className='flex flex-wrap gap-1 my-4 text-sm'>
                    <button className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                        <span>Discovery</span>
                    </button>
                    <button className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                        <span>Sign</span>
                    </button>

                    <div className='z-[10]'>
                        <Dropdown DropText={"Export"}>
                            <div className="px-1 py-1 " role="none">
                                <Menu.Item>
                                    {({ active }) => (
                                    <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                        Windows
                                    </button>
                                    )}
                                </Menu.Item>
                                <Menu.Item>
                                    {({ active }) => (
                                    <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                        Microsoft Certificate Store
                                    </button>
                                    )} 
                                </Menu.Item>
                                <Menu.Item>
                                    {({ active }) => (
                                    <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                        Internet Information Services (IIS)
                                    </button>
                                    )} 
                                </Menu.Item>
                                <Menu.Item>
                                    {({ active }) => (
                                    <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                        
                                        IIS Binding
                                    </button>
                                    )} 
                                </Menu.Item>
                            </div>
                        </Dropdown>
                    </div>
                    <button className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                        <span>Delete</span>
                    </button>
                    <button className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2 ml-auto">
                        <span><ArrowDownTrayIcon className='w-5 h-5'/></span><span>Download Windows Agent</span>
                    </button>
                </div> 
                
                <DataTable 
                    columns={columns} 
                    data={data} 
                    fixedHeader
                    pagination 
                    selectableRows 
                />
                 
            </div> 

            <Modal title={'Create Root Certificate'} isOpenModal={isOpenModalCRC} setIsOpenModal={setIsOpenModalCRC}>
                <div className="px-4 sm:px-6 lg:px-8 items-center gap-4">
                    <div className="grid grid-cols-[30%_70%] max-w-2xl mb-7 gap-y-5 items-center">
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            Common Name
                        </label>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                            <input
                                type="text"
                                autoComplete=""
                                className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                placeholder=""
                            />
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            Choose From Template
                        </label>
                        <div>
                            <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full">
                                <select className="block w-full border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                    <option value="RSA">RSA</option>
                                    <option value="DSA">DSA</option>
                                    <option value="ECDSA">ECDSA</option>
                                    <option value="ED25519">ED25519</option>
                                </select>
                            </div>
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            SAN
                        </label>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                            <input
                                type="text"
                                autoComplete=""
                                className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                placeholder=""
                            />
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            Organization Unit
                        </label>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                            <input
                                type="text"
                                autoComplete=""
                                className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                placeholder=""
                            />
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            Organization
                        </label>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                            <input
                                type="text"
                                autoComplete=""
                                className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                placeholder=""
                            />
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            Location
                        </label>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                            <input
                                type="text"
                                autoComplete=""
                                className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                placeholder=""
                            />
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            State
                        </label>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                            <input
                                type="text"
                                autoComplete=""
                                className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                placeholder=""
                            />
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            Country
                        </label>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                            <input
                                type="text"
                                autoComplete=""
                                className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                placeholder=""
                            />
                        </div>
                        
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                            Key Algorithm
                        </label>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full">
                            <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                <option value="1024">1024</option>
                                <option value="2048">2048</option>
                                <option value="4096">4096</option>
                            </select>
                        </div>

                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                            Key Size
                        </label>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full">
                            <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                <option value="1024">1024</option>
                                <option value="2048">2048</option>
                                <option value="4096">4096</option>
                            </select>
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                            Signature Algorithm
                        </label>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full">
                            <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                <option value="1024">1024</option>
                                <option value="2048">2048</option>
                                <option value="4096">4096</option>
                            </select>
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                            KeyStore Type
                        </label>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full">
                            <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                <option value="1024">1024</option>
                                <option value="2048">2048</option>
                                <option value="4096">4096</option>
                            </select>
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                            Validity Type
                        </label>
                        <div className="flex w-full">
                            <legend className="sr-only">Select Option</legend>
                            <div className="w-full flex gap-7">
                                <div className="flex items-center">
                                    <input
                                        id="Days"
                                        name="discover-by"
                                        type="radio"
                                        defaultChecked={discoverBy === 'Days'}
                                        value="Days"
                                        onClick={() => setDiscoverBy('Days')}
                                        className="accent-theme-red1 h-4 w-4 border-gray-300"
                                    />
                                    <label htmlFor="Days" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                        Days
                                    </label>
                                </div>
                                <div className="flex items-center">
                                    <input
                                        id="Hours"
                                        name="discover-by"
                                        type="radio"
                                        defaultChecked={discoverBy === 'Hours'}
                                        value="Hours"
                                        onClick={() => setDiscoverBy('Hours')}
                                        className="accent-theme-red1 h-4 w-4 border-gray-300 text-theme-red1 focus:ring-theme-red1"
                                    />
                                    <label htmlFor="Hours" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                        Hours
                                    </label>
                                </div>
                                <div className="flex items-center">
                                    <input
                                        id="Minutes"
                                        name="discover-by"
                                        type="radio"
                                        defaultChecked={discoverBy === 'Minutes'}
                                        value="Minutes"
                                        onClick={() => setDiscoverBy('Minutes')}
                                        className="accent-theme-red1 h-4 w-4 border-gray-300 text-theme-red1 focus:ring-theme-red1"
                                    />
                                    <label htmlFor="Minutes" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                        Minutes
                                    </label>
                                </div>
                            </div>
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            Validity
                        </label>
                        <div className="flex items-center rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                            <input
                                type="text"
                                autoComplete=""
                                className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                placeholder=""
                            />
                            <small className='px-3'>{discoverBy}</small>
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            Store Password
                        </label>
                        <div className="flex items-center rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                            <input
                                type="password"
                                autoComplete=""
                                className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                placeholder=""
                            />
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            Expiry Notification Email
                        </label>
                        <div className="flex items-center rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                            <input
                                type="password"
                                autoComplete=""
                                className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                placeholder=""
                            />
                        </div>
                    
                        <label htmlFor="">&nbsp;</label>
                        <div className="relative flex items-start max-w-xs w-full">
                            <div className="flex h-6 items-center">
                                <input
                                id="comments"
                                aria-describedby="comments-description"
                                name="comments"
                                type="checkbox"
                                className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                />
                            </div>
                            <div className="ml-3 text-sm leading-6">
                                <span> Generate root certificate | <a href="javascript:;" onClick={()=>setAdvOptions(!advOptions)} className={`${advOptions ? "text-theme-red1" : "underline"}`}>Advanced Options</a></span>
                            </div>
                        </div>
                        {advOptions && 
                            <>
                            <label htmlFor="">&nbsp;</label>
                        <div className="grid grid-cols-[100%] rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full p-5 gap-2">
                            <label htmlFor="" className='font-medium mb-3'>Key Usage</label>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span> Critical (will mark below selected properties as critical) </span>
                                </div>
                            </div>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span> Digital Signature</span>
                                </div>
                            </div>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span> Non Repudiation</span>
                                </div>
                            </div>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span>  Key Encipherment</span>
                                </div>
                            </div>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span>  Data Encipherment</span>
                                </div>
                            </div>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span> Key Agreement</span>
                                </div>
                            </div>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span> Certificate Sign</span>
                                </div>
                            </div>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span>  CRL Sign</span>
                                </div>
                            </div>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span> Encipher Only</span>
                                </div>
                            </div>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span> Decipher Only</span>
                                </div>
                            </div>
                            <hr className='my-5'/>
                            <label htmlFor="" className='font-medium mb-3'>Extended Key Usage</label>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span>Critical (will mark below selected properties as critical)</span>
                                </div>
                            </div>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span>Server Authentication</span>
                                </div>
                            </div>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span> Client Authentication</span>
                                </div>
                            </div>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span>Code Signing</span>
                                </div>
                            </div>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span> E-mail</span>
                                </div>
                            </div>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span>Timestamping</span>
                                </div>
                            </div>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span>OCSP Signing</span>
                                </div>
                            </div>
                        </div>
                        </>
                        }
                        
                    </div>
                    <hr />
                    <div className='w-full flex gap-5 pt-5'>
                        <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Create</button>
                        <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                    </div>
                </div> 
            </Modal>   
            <Modal title={'Request Certificate'} isOpenModal={isOpenModalReqCerti} setIsOpenModal={setIsOpenModalReqCerti}>
                <div className="px-4 sm:px-6 lg:px-8 items-center gap-4">
                    <div className="grid grid-cols-[30%_70%] max-w-2xl mb-7 gap-y-5 items-center">

                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            Sign With Certificate
                        </label>
                        <div>
                            <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full">
                                <select className="block w-full border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                    <option value="RSA">RSA</option>
                                    <option value="DSA">DSA</option>
                                    <option value="ECDSA">ECDSA</option>
                                    <option value="ED25519">ED25519</option>
                                </select>
                            </div>
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            CSR
                        </label>
                        <div>
                            <div className="flex w-full">
                                <legend className="sr-only">Select Option</legend>
                                <div className="w-full flex gap-7">
                                    <div className="flex items-center">
                                        <input
                                            id="Create"
                                            name="CSR"
                                            type="radio"
                                            defaultChecked={CSR === 'Create'}
                                            value="Create"
                                            onClick={() => setCSR('Create')}
                                            className="accent-theme-red1 h-4 w-4 border-gray-300"
                                        />
                                        <label htmlFor="Create" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                            Create
                                        </label>
                                    </div>
                                    <div className="flex items-center">
                                        <input
                                            id="Select"
                                            name="CSR"
                                            type="radio"
                                            defaultChecked={CSR === 'Select'}
                                            value="Select"
                                            onClick={() => setCSR('Select')}
                                            className="accent-theme-red1 h-4 w-4 border-gray-300 text-theme-red1 focus:ring-theme-red1"
                                        />
                                        <label htmlFor="Select" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                            Select
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {CSR === 'Create' &&
                            <>
                                <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                    Common Name
                                </label>
                                <div>
                                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                        <input
                                            type="text"
                                            autoComplete=""
                                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                            placeholder=""
                                        />
                                    </div>
                                </div>
                                <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                    SAN
                                </label>
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="text"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                                <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                    Organization Unit
                                </label>
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="text"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                                <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                    Organization
                                </label>
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="text"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                                <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                    Location
                                </label>
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="text"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                                <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                    State
                                </label>
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="text"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                                <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                    Country
                                </label>
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="text"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                                
                                <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                                    Key Algorithm
                                </label>
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full">
                                    <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                        <option value="1024">1024</option>
                                        <option value="2048">2048</option>
                                        <option value="4096">4096</option>
                                    </select>
                                </div>

                                <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                                    Key Size
                                </label>
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full">
                                    <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                        <option value="1024">1024</option>
                                        <option value="2048">2048</option>
                                        <option value="4096">4096</option>
                                    </select>
                                </div>
                                <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                                    Signature Algorithm
                                </label>
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full">
                                    <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                        <option value="1024">1024</option>
                                        <option value="2048">2048</option>
                                        <option value="4096">4096</option>
                                    </select>
                                </div>
                                <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                                    KeyStore Type
                                </label>
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full">
                                    <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                        <option value="1024">1024</option>
                                        <option value="2048">2048</option>
                                        <option value="4096">4096</option>
                                    </select>
                                </div>
                                <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                                    Validity Type
                                </label>
                                <div className="flex w-full">
                                    <legend className="sr-only">Select Option</legend>
                                    <div className="w-full flex gap-7">
                                        <div className="flex items-center">
                                            <input
                                                id="Days"
                                                name="discover-by"
                                                type="radio"
                                                defaultChecked={discoverBy === 'Days'}
                                                value="Days"
                                                onClick={() => setDiscoverBy('Days')}
                                                className="accent-theme-red1 h-4 w-4 border-gray-300"
                                            />
                                            <label htmlFor="Days" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                                Days
                                            </label>
                                        </div>
                                        <div className="flex items-center">
                                            <input
                                                id="Hours"
                                                name="discover-by"
                                                type="radio"
                                                defaultChecked={discoverBy === 'Hours'}
                                                value="Hours"
                                                onClick={() => setDiscoverBy('Hours')}
                                                className="accent-theme-red1 h-4 w-4 border-gray-300 text-theme-red1 focus:ring-theme-red1"
                                            />
                                            <label htmlFor="Hours" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                                Hours
                                            </label>
                                        </div>
                                        <div className="flex items-center">
                                            <input
                                                id="Minutes"
                                                name="discover-by"
                                                type="radio"
                                                defaultChecked={discoverBy === 'Minutes'}
                                                value="Minutes"
                                                onClick={() => setDiscoverBy('Minutes')}
                                                className="accent-theme-red1 h-4 w-4 border-gray-300 text-theme-red1 focus:ring-theme-red1"
                                            />
                                            <label htmlFor="Minutes" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                                Minutes
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                    Validity
                                </label>
                                <div className="flex items-center rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="text"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                    <small className='px-3'>{discoverBy}</small>
                                </div>
                                <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                    Store Password
                                </label>
                                <div className="flex items-center rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="password"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                                <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                    Expiry Notification Email
                                </label>
                                <div className="flex items-center rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="password"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                            </>
                        }

                        {CSR === 'Select' &&
                            <>
                                <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                                    Select CSR
                                </label>
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full">
                                    <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6" id="csrList" name="csrList" isnullable="false">
                                        <option value="-1">--Select CSR--</option>
                                        <option value="301">example.com</option>
                                        <option value="302">test.com</option>
                                        <option value="601">testcsr.com</option>
                                        <option value="901">createcsr.com</option>
                                        <option value="1501">Intermediate CA</option>
                                        <option value="1502">Intermediate CA</option>
                                        <option value="1503">Intermediate CA</option>
                                        <option value="1504">Intermediate CA</option>
                                        <option value="1505">Intermediate CA</option>
                                        <option value="1506">Intermediate CA</option>
                                        <option value="1507">Intermediate CA</option>
                                        <option value="1508">Intermediate CA</option>
                                        <option value="1509">testserver</option>
                                        <option value="1510">Intermediate CA2</option>
                                        <option value="1511">myserver</option>
                                        <option value="1512">myserver</option>
                                        <option value="1513">testServer2</option>
                                        <option value="1514">test.com</option>
                                    </select>
                                </div>
                                <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                                    Validity Type
                                </label>
                                <div className="flex w-full">
                                    <legend className="sr-only">Select Option</legend>
                                    <div className="w-full flex gap-7">
                                        <div className="flex items-center">
                                            <input
                                                id="Days"
                                                name="discover-by"
                                                type="radio"
                                                defaultChecked={discoverBy === 'Days'}
                                                value="Days"
                                                onClick={() => setDiscoverBy('Days')}
                                                className="accent-theme-red1 h-4 w-4 border-gray-300"
                                            />
                                            <label htmlFor="Days" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                                Days
                                            </label>
                                        </div>
                                        <div className="flex items-center">
                                            <input
                                                id="Hours"
                                                name="discover-by"
                                                type="radio"
                                                defaultChecked={discoverBy === 'Hours'}
                                                value="Hours"
                                                onClick={() => setDiscoverBy('Hours')}
                                                className="accent-theme-red1 h-4 w-4 border-gray-300 text-theme-red1 focus:ring-theme-red1"
                                            />
                                            <label htmlFor="Hours" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                                Hours
                                            </label>
                                        </div>
                                        <div className="flex items-center">
                                            <input
                                                id="Minutes"
                                                name="discover-by"
                                                type="radio"
                                                defaultChecked={discoverBy === 'Minutes'}
                                                value="Minutes"
                                                onClick={() => setDiscoverBy('Minutes')}
                                                className="accent-theme-red1 h-4 w-4 border-gray-300 text-theme-red1 focus:ring-theme-red1"
                                            />
                                            <label htmlFor="Minutes" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                                Minutes
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                    Validity
                                </label>
                                <div className="flex items-center rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="text"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                    <small className='px-3'>{discoverBy}</small>
                                </div>
                            </>
                        }
                        
                    
                        <label htmlFor="">&nbsp;</label>
                        <div className="relative flex items-start max-w-xs w-full">
                            <div className="flex h-6 items-center">
                                <input
                                id="comments"
                                aria-describedby="comments-description"
                                name="comments"
                                type="checkbox"
                                className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                />
                            </div>
                            <div className="ml-3 text-sm leading-6">
                                <span> Generate root certificate | <a href="javascript:;" onClick={()=>setAdvOptions(!advOptions)} className={`${advOptions ? "text-theme-red1" : "underline"}`}>Advanced Options</a></span>
                            </div>
                        </div>
                        {advOptions && 
                            <>
                            <label htmlFor="">&nbsp;</label>
                        <div className="grid grid-cols-[100%] rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full p-5 gap-2">
                            <label htmlFor="" className='font-medium mb-3'>Key Usage</label>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span> Critical (will mark below selected properties as critical) </span>
                                </div>
                            </div>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span> Digital Signature</span>
                                </div>
                            </div>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span> Non Repudiation</span>
                                </div>
                            </div>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span>  Key Encipherment</span>
                                </div>
                            </div>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span>  Data Encipherment</span>
                                </div>
                            </div>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span> Key Agreement</span>
                                </div>
                            </div>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span> Certificate Sign</span>
                                </div>
                            </div>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span>  CRL Sign</span>
                                </div>
                            </div>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span> Encipher Only</span>
                                </div>
                            </div>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span> Decipher Only</span>
                                </div>
                            </div>
                            <hr className='my-5'/>
                            <label htmlFor="" className='font-medium mb-3'>Extended Key Usage</label>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span>Critical (will mark below selected properties as critical)</span>
                                </div>
                            </div>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span>Server Authentication</span>
                                </div>
                            </div>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span> Client Authentication</span>
                                </div>
                            </div>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span>Code Signing</span>
                                </div>
                            </div>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span> E-mail</span>
                                </div>
                            </div>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span>Timestamping</span>
                                </div>
                            </div>
                            <div className="relative flex items-start w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span>OCSP Signing</span>
                                </div>
                            </div>
                        </div>
                        </>
                        }
                        
                    </div>
                    <hr />
                    <div className='w-full flex gap-5 pt-5'>
                        <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Create</button>
                        <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                    </div>
                </div> 
            </Modal>   
            <Modal title={'Certificate Renewal'} isOpenModal={isOpenModalRenewC} setIsOpenModal={setIsOpenModalRenewC}>
                <div className="px-4 sm:px-6 lg:px-8 items-center gap-4">
                    <div className="grid grid-cols-[30%_70%] max-w-2xl mb-7 gap-y-5 items-center">
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            Renewal Type
                        </label>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            Self Signed
                        </label>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            Validity
                        </label>
                        <div className="flex justify-center items-center rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                            <input
                                type="text"
                                autoComplete=""
                                className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                placeholder=""
                            />
                            <span className='px-2'>days</span>
                        </div>
                    </div>
                    <hr />
                    <div className='w-full flex gap-5 pt-5'>
                        <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Renew</button>
                        <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                    </div>
                    
                </div>        
                <HelpBox helpTxt={helpTxt} />
            </Modal>
            
            <Modal title={'Confirmation'} isOpenModal={isOpenModalDelete} setIsOpenModal={setIsOpenModalDelete}>
                <div className="grid grid-cols-[100%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                        A few selected certificates are available as the Private/Intermediate CA. Delete the selected certificates anyway?
                    </label>
                    <div className="relative flex items-start max-w-xs w-full">
                        <div className="flex h-6 items-center">
                            <input
                            id="comments"
                            aria-describedby="comments-description"
                            name="comments"
                            type="checkbox"
                            className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                            />
                        </div>
                        <div className="ml-3 text-sm leading-6">
                            <span> Add selected certificates to 'Excluded certificates'</span>
                        </div>
                    </div>
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Ok</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>   

            
        </>
		
	);
};

