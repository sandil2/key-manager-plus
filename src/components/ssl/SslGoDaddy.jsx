import React, { useMemo, useState } from 'react';
import DataTable from 'react-data-table-component';
import HelpBox from '../HelpBox';
import Modal from '../Modal';

import goDaddyLogo from "../../images/goDaddy.svg"
import { 
    CreditCardIcon,
    ArrowDownOnSquareIcon,
    ArrowUpOnSquareIcon,
    EyeIcon,
    EnvelopeIcon,
    MagnifyingGlassIcon,
    
    } from '@heroicons/react/24/outline';
import Dropdown from '../Dropdown';
import { Menu } from '@headlessui/react';
import { Link } from 'react-router-dom';

export default function SslGoDaddy({setMasterTab}){
    const columns = [
        {
            name: 'Common Name',
            selector: row => row.CommonName,
            sortable: true,  
        },
        {
            name: 'Certificate ID',
            selector: row => row.CertificateID,
            sortable: true,            
        },
        {
            name: 'Certificate Type',
            selector: row => row.CertificateType,
            sortable: true,
        },
        {
            name: 'Valid From',
            selector: row => row.ValidFrom,
            sortable: true,
        },
        {
            name: 'Valid To',
            selector: row => row.ValidTo,
            sortable: true,
        },
        {
            name: 'Requested By',
            selector: row => row.RequestedBy,
            sortable: true,
        },
        {
            name: 'Status',
            selector: row => row.Status,
            sortable: true,
            maxWidth: "230px",
            minWidth: "250px"
        },
        {
            name: '',
            selector: row => row.History,
            sortable: false,
            maxWidth: "30px",
            minWidth: "50px"
        },
    ];    

const data = [
    {
        CommonName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        CertificateID :   "admin",
        CertificateType : "Jan 28, 2019 18:16",
        ValidFrom : "4096",
        ValidTo :"RSA",
        RequestedBy : "JKS", 
        Status: <span className="inline-flex items-center rounded-full bg-blue-50 px-2 py-1 text-xs font-medium text-blue-700 ring-1 ring-inset ring-blue-700/10">CERTIFICATE ORDER CREATED</span>,
        History: <CreditCardIcon className='w-5 h-4 text-black-700'/>,
    }, 
    {
        CommonName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        CertificateID :   "admin",
        CertificateType : "Jan 28, 2019 18:16",
        ValidFrom : "4096",
        ValidTo :"RSA",
        RequestedBy : "JKS", 
        Status: <span className="inline-flex items-center rounded-full bg-green-50 px-2 py-1 text-xs font-medium text-green-700 ring-1 ring-inset ring-green-600/20">CERTIFICATE SAVED</span>,
        History: <CreditCardIcon className='w-5 h-4 text-black-700'/>,
    }, 
    {
        CommonName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        CertificateID :   "admin",
        CertificateType : "Jan 28, 2019 18:16",
        ValidFrom : "4096",
        ValidTo :"RSA",
        RequestedBy : "JKS", 
        Status: "-",
        History: <CreditCardIcon className='w-5 h-4 text-black-700'/>,
    }, 
    {
        CommonName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        CertificateID :   "admin",
        CertificateType : "Jan 28, 2019 18:16",
        ValidFrom : "4096",
        ValidTo :"RSA",
        RequestedBy : "JKS", 
        Status: "-",
        History: <CreditCardIcon className='w-5 h-4 text-black-700'/>,
    }, 
    
   
  
];

const [records,setRecords] = useState(data);

const [discoverBy, setDiscoverBy] = useState('Days');

const [isOpenModalRC, setIsOpenModalRC] = useState(false);
const [isOpenModalDeployDNS, setIsOpenModalDeployDNS] = useState(false);



function handleFilter(event){
    const newData = records.filter(row=>{
        return row.ResourceName.toLowerCase().includes(event.target.value.toLowerCase())
    })
    setRecords(newData)
}

	return (
        <>
            <div className="px-4 sm:px-6 lg:px-8 flex items-center text">
                <h1 className='text-xl theme-gray8 my-3 flex w-full gap-3 items-center'>
                   <span className='w-5 h-5 '><img src={goDaddyLogo} alt="" /></span><span>Go Daddy</span>
                </h1>
            </div> 
            <div className="px-4 sm:px-6 lg:px-8">
            <div className='flex flex-wrap gap-1 my-4 text-sm'>
                
                <Link to="/ssl/godaddy-order-certificate" className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2" >
                    <span>Order Certificate</span>
                </Link>
                
                <button className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                    <span>Renew Certificate</span>
                </button>

                <button onClick={()=>setIsOpenModalRC(true)} className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                    <span>Reissue Certificate</span>
                </button>

                <button className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                    <span>Check Issue Status</span>
                </button>

                <div className='z-[10]'>
                    <Dropdown DropText={"More"}>
                        <div className="px-1 py-1 " role="none">
                            <Menu.Item>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Import Existing Certificates
                                </button>
                                )}
                            </Menu.Item>
                            <Menu.Item>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Revoke Certificate
                                </button>
                                )} 
                            </Menu.Item>
                            <Menu.Item>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Cancel Order
                                </button>
                                )} 
                            </Menu.Item>
                            <Menu.Item>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Delete
                                </button>
                                )} 
                            </Menu.Item>
                        </div>
                    </Dropdown>
                </div>
                <button className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2 ml-auto">
                    <span>Account</span>
                </button>

                <button className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                    <span>Auto-Renewal</span>
                </button>
            </div>
            <div className='max-w-[500px] w-full mb-5 block'>
                <div className="flex">
                    <label htmlFor="search-dropdown" className="mb-2 text-sm font-medium text-gray-900 sr-only">Your Email</label>
                    <div className="relative w-full">
                        <input type="search" id="search-dropdown" className="block p-3 w-full z-20 text-sm text-gray-900 bg-gray-50 rounded-full border-s-2 border border-gray-300 focus:ring-blue-500 focus:border-blue-500" placeholder="Search..."/>
                        <button type="button" className="absolute top-0 end-0 p-3 text-sm font-medium h-full focus:ring-4 focus:outline-none focus:ring-blue-300">
                            <MagnifyingGlassIcon className='w-4 h-4'/>
                            <span className="sr-only">Search</span>
                        </button>
                    </div>
                </div>
            </div>
            
            <DataTable 
                columns={columns} 
                data={data} 
                fixedHeader
                pagination 
                selectableRows 
            />
            </div>

            <Modal title={'Reissue Certificate'} isOpenModal={isOpenModalRC} setIsOpenModal={setIsOpenModalRC}>
                <div className="px-4 sm:px-6 lg:px-8 items-center gap-4">
                    <div className="grid grid-cols-[30%_70%] max-w-2xl mb-7 gap-y-5 items-center">
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            Common Name
                        </label>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                            <input
                                type="text"
                                autoComplete=""
                                className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                placeholder=""
                            />
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            Choose From Template
                        </label>
                        <div>
                            <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full">
                                <select className="block w-full border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                    <option value="RSA">RSA</option>
                                    <option value="DSA">DSA</option>
                                    <option value="ECDSA">ECDSA</option>
                                    <option value="ED25519">ED25519</option>
                                </select>
                            </div>
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            SAN
                        </label>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                            <input
                                type="text"
                                autoComplete=""
                                className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                placeholder=""
                            />
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            Organization Unit
                        </label>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                            <input
                                type="text"
                                autoComplete=""
                                className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                placeholder=""
                            />
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            Organization
                        </label>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                            <input
                                type="text"
                                autoComplete=""
                                className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                placeholder=""
                            />
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            Location
                        </label>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                            <input
                                type="text"
                                autoComplete=""
                                className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                placeholder=""
                            />
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            State
                        </label>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                            <input
                                type="text"
                                autoComplete=""
                                className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                placeholder=""
                            />
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            Country
                        </label>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                            <input
                                type="text"
                                autoComplete=""
                                className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                placeholder=""
                            />
                        </div>
                        
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                            Key Algorithm
                        </label>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full">
                            <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                <option value="1024">1024</option>
                                <option value="2048">2048</option>
                                <option value="4096">4096</option>
                            </select>
                        </div>

                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                            Key Size
                        </label>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full">
                            <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                <option value="1024">1024</option>
                                <option value="2048">2048</option>
                                <option value="4096">4096</option>
                            </select>
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                            Signature Algorithm
                        </label>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full">
                            <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                <option value="1024">1024</option>
                                <option value="2048">2048</option>
                                <option value="4096">4096</option>
                            </select>
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                            KeyStore Type
                        </label>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full">
                            <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                <option value="1024">1024</option>
                                <option value="2048">2048</option>
                                <option value="4096">4096</option>
                            </select>
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                            Validity Type
                        </label>
                        <div className="flex w-full">
                            <legend className="sr-only">Select Option</legend>
                            <div className="w-full flex gap-7">
                                <div className="flex items-center">
                                    <input
                                        id="Days"
                                        name="discover-by"
                                        type="radio"
                                        defaultChecked={discoverBy === 'Days'}
                                        value="Days"
                                        onClick={() => setDiscoverBy('Days')}
                                        className="accent-theme-red1 h-4 w-4 border-gray-300"
                                    />
                                    <label htmlFor="Days" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                        Days
                                    </label>
                                </div>
                                <div className="flex items-center">
                                    <input
                                        id="Hours"
                                        name="discover-by"
                                        type="radio"
                                        defaultChecked={discoverBy === 'Hours'}
                                        value="Hours"
                                        onClick={() => setDiscoverBy('Hours')}
                                        className="accent-theme-red1 h-4 w-4 border-gray-300 text-theme-red1 focus:ring-theme-red1"
                                    />
                                    <label htmlFor="Hours" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                        Hours
                                    </label>
                                </div>
                                <div className="flex items-center">
                                    <input
                                        id="Minutes"
                                        name="discover-by"
                                        type="radio"
                                        defaultChecked={discoverBy === 'Minutes'}
                                        value="Minutes"
                                        onClick={() => setDiscoverBy('Minutes')}
                                        className="accent-theme-red1 h-4 w-4 border-gray-300 text-theme-red1 focus:ring-theme-red1"
                                    />
                                    <label htmlFor="Minutes" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                        Minutes
                                    </label>
                                </div>
                            </div>
                        </div>
                       
                        
                    </div>
                    <hr />
                    <div className='w-full flex gap-5 pt-5'>
                        <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Create</button>
                        <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                    </div>
                </div> 
            </Modal> 
        </>
		
	);
};

