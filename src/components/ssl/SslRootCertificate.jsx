import React, { useMemo, useState } from 'react';
import { ChevronRightIcon } from '@heroicons/react/24/outline';
import { Menu } from '@headlessui/react';
import Dropdown from '../Dropdown';
import DataTable from 'react-data-table-component';
import { 
    ArrowDownTrayIcon,
    UserGroupIcon, 
    ArrowLeftIcon,
    PresentationChartLineIcon,
    Square3Stack3DIcon} from '@heroicons/react/24/outline';
import Modal from '../Modal';
import HelpBox from '../HelpBox';
import { Link } from 'react-router-dom';

export default function SslRootCertificate({setMasterTab}){

     const columns = [
        
        {
            name: 'Common Name',
            selector: row => row.CommonName,
            sortable: true,            
        },
        {
            name: 'DNS Name',
            selector: row => row.DNSName,
            sortable: true,
        },
        {
            name: 'Issuer',
            selector: row => row.Issuer,
            sortable: true,
        },
        {
            name: 'Valid To',
            selector: row => row.ValidTo,
            sortable: true,
        },
        {
            name: 'Key Size',
            selector: row => row.KeySize,
            sortable: true,
        },
        {
            name: 'Signature Algorithm',
            selector: row => row.SignatureAlgorithm,
            sortable: false,
        },
        
        {
            name: '',
            selector: row => row.IconKey,
            sortable: false,
            maxWidth: "30px",
            minWidth: "50px"
        },
    ];    

    const data = [
        {
            CommonName : <a href="javascript:;" className='text-theme-blue1'>	RootCA</a>  ,
            DNSName : "	RootCA",
            Issuer : "	RootCA",
            ValidTo :<span className=''>Feb 6, 2024 11:08</span>,
            KeySize: '4096',
            SignatureAlgorithm: 'SHA256',
            IconCertiHistory: <UserGroupIcon className='w-5 h-4 text-black-700 cursor-pointer'/>,
        },
        {
            CommonName : <a href="javascript:;" className='text-theme-blue1'>	RootCA</a>  ,
            DNSName : "	RootCA",
            Issuer : "	RootCA",
            ValidTo :<span className=''>Feb 6, 2024 11:08</span>,
            KeySize: '4096',
            SignatureAlgorithm: 'SHA256',
            IconCertiHistory: <UserGroupIcon className='w-5 h-4 text-black-700 cursor-pointer'/>,
        },
        {
            CommonName : <a href="javascript:;" className='text-theme-blue1'>	RootCA</a>  ,
            DNSName : "	RootCA",
            Issuer : "	RootCA",
            ValidTo :<span className=''>Feb 6, 2024 11:08</span>,
            KeySize: '4096',
            SignatureAlgorithm: 'SHA256',
            IconCertiHistory: <UserGroupIcon className='w-5 h-4 text-black-700 cursor-pointer'/>,
        },
        {
            CommonName : <a href="javascript:;" className='text-theme-blue1'>	RootCA</a>  ,
            DNSName : "	RootCA",
            Issuer : "	RootCA",
            ValidTo :<span className=''>Feb 6, 2024 11:08</span>,
            KeySize: '4096',
            SignatureAlgorithm: 'SHA256',
            IconCertiHistory: <UserGroupIcon className='w-5 h-4 text-black-700 cursor-pointer'/>,
        },
    ];

    const helpTxt = [
        "Choose a root certificate and click Sign option in the top menu to sign and issue certificates to specific user groups.",
        "The sign type User Management allows you to create and issue certificates to Key Manager Plus users.",
        "The sign type Active Directory Users allows you create and issue certificates to Active Directory users within your network. Choose the domain name, provide the domain controller details, username and password. Specify the users / user groups to be imported as CSV or select the user / user groups using Groups/OU tree import type.",
        "Enabling Use root certificate details option causes the user certificate to inherit all the parameters of its root certificate."
    ]
    
    const [isOpenModalCS,setIsOpenModalCS] = useState(false);
    const [useCSA,setUseCSA] = useState(false);
    

	return (
        <>
            <div className="px-4 sm:px-6 lg:px-8 items-center gap-4 text">
                 <h1 className='text-xl theme-gray8 my-3 flex'>
                    <span>Root Certificate</span>
                    <small className='ml-auto'>
                        <Link to="/ssl/certificate" className="hover:shadow-lg transition duration-300 ease-in-out bg-theme-gray2 rounded-full flex px-5 py-2 gap-3 items-center border border-theme-gray2 flex text-xs font-light">
                            <ArrowLeftIcon className="w-4 h-4"/> <span> Back</span>
                        </Link>
                    </small>
                </h1>
                <div className='flex flex-wrap gap-1 my-4 text-sm'>
                    <button className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2" onClick={()=>setIsOpenModalCS(true)}>
                        <span>Sign</span>
                    </button>
                    <button className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                        <span>Delete</span>
                    </button>
                </div> 
                
                <DataTable 
                    columns={columns} 
                    data={data} 
                    fixedHeader
                    pagination 
                    selectableRows 
                />
                 
            </div> 

            <HelpBox helpTxt={helpTxt}/>
            <Modal title={'Certificate Signing'} isOpenModal={isOpenModalCS} setIsOpenModal={setIsOpenModalCS}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Sign Type
                    </label>
                    <div>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full max-w-sm">
                            <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                <option value="1024">1024</option>
                                <option value="2048">2048</option>
                                <option value="4096">4096</option>
                            </select>
                        </div>
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Select User
                    </label>
                    <div>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full max-w-sm">
                            <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                <option value="1024">1024</option>
                                <option value="2048">2048</option>
                                <option value="4096">4096</option>
                            </select>
                        </div>
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        &nbsp;
                    </label>
                    <div className="">
                        <div className="relative flex items-start max-w-xs w-full">
                            <div className="flex h-6 items-center">
                                <input
                                id="use-cas"
                                name="use-cas"
                                type="checkbox"
                                className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                defaultChecked={useCSA}
                                onChange={()=>setUseCSA(!useCSA)}
                                />
                            </div>
                            <div className="ml-3 text-sm leading-6">
                                <span> Use root certificate details</span>
                            </div>
                        </div>
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        SAN
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Validity
                    </label>
                    <div className="flex justify-center items-center rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                        <span className='px-2'>Days</span>
                    </div>
                </div>    
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Ok</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>   
            

            
        </>
		
	);
};

