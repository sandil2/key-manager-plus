import React, { useMemo, useState } from 'react';
import { Link } from 'react-router-dom';
import {ArrowLeftIcon} from '@heroicons/react/24/outline';

export default function SslCreateCertificate({setMasterTab}){
    
    const [discoverBy,setDiscoverBy] = useState("Days");
    
    const [advOptions,setAdvOptions] = useState(false);

	return (
        <>
            <div className="px-4 sm:px-6 lg:px-8 flex items-center gap-4 text">
                <h1 className='text-xl theme-gray8 my-3 flex w-full'>
                    <span>Create Certificate</span>
                    <small className='ml-auto'>
                        <Link to="/ssl" className="hover:shadow-lg transition duration-300 ease-in-out bg-theme-gray2 rounded-full flex px-5 py-2 gap-3 items-center border border-theme-gray2 flex text-xs font-light">
                            <ArrowLeftIcon className="w-4 h-4"/> <span> Back</span>
                        </Link>
                    </small>
                </h1>
            </div> 
            <div className="px-4 sm:px-6 lg:px-8 items-center gap-4">
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Common Name
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Choose From Template
                    </label>
                    <div>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full">
                            <select className="block w-full border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                <option value="RSA">RSA</option>
                                <option value="DSA">DSA</option>
                                <option value="ECDSA">ECDSA</option>
                                <option value="ED25519">ED25519</option>
                            </select>
                        </div>
                        <p className='text-right'>
                            <small><a href="javascript:;" className='text-theme-red1'>Manage CSR Templates</a></small>
                        </p>
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        SAN
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Organization Unit
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Organization
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Location
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        State
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Country
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                        Key Algorithm
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full">
                        <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                            <option value="1024">1024</option>
                            <option value="2048">2048</option>
                            <option value="4096">4096</option>
                        </select>
                    </div>

                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                        Key Size
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full">
                        <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                            <option value="1024">1024</option>
                            <option value="2048">2048</option>
                            <option value="4096">4096</option>
                        </select>
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                        Signature Algorithm
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full">
                        <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                            <option value="1024">1024</option>
                            <option value="2048">2048</option>
                            <option value="4096">4096</option>
                        </select>
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                        KeyStore Type
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full">
                        <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                            <option value="1024">1024</option>
                            <option value="2048">2048</option>
                            <option value="4096">4096</option>
                        </select>
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                        Validity Type
                    </label>
                    <div className="flex w-full">
                        <legend className="sr-only">Select Option</legend>
                        <div className="w-full flex gap-7">
                            <div className="flex items-center">
                                <input
                                    id="Days"
                                    name="discover-by"
                                    type="radio"
                                    defaultChecked={discoverBy === 'Days'}
                                    value="Days"
                                    onClick={() => setDiscoverBy('Days')}
                                    className="accent-theme-red1 h-4 w-4 border-gray-300"
                                />
                                <label htmlFor="Days" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                    Days
                                </label>
                            </div>
                            <div className="flex items-center">
                                <input
                                    id="Hours"
                                    name="discover-by"
                                    type="radio"
                                    defaultChecked={discoverBy === 'Hours'}
                                    value="Hours"
                                    onClick={() => setDiscoverBy('Hours')}
                                    className="accent-theme-red1 h-4 w-4 border-gray-300 text-theme-red1 focus:ring-theme-red1"
                                />
                                <label htmlFor="Hours" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                    Hours
                                </label>
                            </div>
                            <div className="flex items-center">
                                <input
                                    id="Minutes"
                                    name="discover-by"
                                    type="radio"
                                    defaultChecked={discoverBy === 'Minutes'}
                                    value="Minutes"
                                    onClick={() => setDiscoverBy('Minutes')}
                                    className="accent-theme-red1 h-4 w-4 border-gray-300 text-theme-red1 focus:ring-theme-red1"
                                />
                                <label htmlFor="Minutes" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                    Minutes
                                </label>
                            </div>
                        </div>
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Validity
                    </label>
                    <div className="flex items-center rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                        <small className='px-3'>{discoverBy}</small>
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Store Password
                    </label>
                    <div className="flex items-center rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="password"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Expiry Notification Email
                    </label>
                    <div className="flex items-center rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="password"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                   
                    <label htmlFor="">&nbsp;</label>
                    <div className="relative flex items-start max-w-xs w-full">
                        <div className="flex h-6 items-center">
                            <input
                            id="comments"
                            aria-describedby="comments-description"
                            name="comments"
                            type="checkbox"
                            className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                            />
                        </div>
                        <div className="ml-3 text-sm leading-6">
                            <span> Generate root certificate | <a href="javascript:;" onClick={()=>setAdvOptions(!advOptions)} className={`${advOptions ? "text-theme-red1" : "underline"}`}>Advanced Options</a></span>
                        </div>
                    </div>
                    {advOptions && 
                        <>
                        <label htmlFor="">&nbsp;</label>
                    <div className="grid grid-cols-[100%] rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full p-5 gap-2">
                        <label htmlFor="" className='font-medium mb-3'>Key Usage</label>
                        <div className="relative flex items-start w-full">
                            <div className="flex h-6 items-center">
                                <input
                                id="comments"
                                aria-describedby="comments-description"
                                name="comments"
                                type="checkbox"
                                className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                />
                            </div>
                            <div className="ml-3 text-sm leading-6">
                                <span> Critical (will mark below selected properties as critical) </span>
                            </div>
                        </div>
                        <div className="relative flex items-start w-full">
                            <div className="flex h-6 items-center">
                                <input
                                id="comments"
                                aria-describedby="comments-description"
                                name="comments"
                                type="checkbox"
                                className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                />
                            </div>
                            <div className="ml-3 text-sm leading-6">
                                <span> Digital Signature</span>
                            </div>
                        </div>
                        <div className="relative flex items-start w-full">
                            <div className="flex h-6 items-center">
                                <input
                                id="comments"
                                aria-describedby="comments-description"
                                name="comments"
                                type="checkbox"
                                className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                />
                            </div>
                            <div className="ml-3 text-sm leading-6">
                                <span> Non Repudiation</span>
                            </div>
                        </div>
                        <div className="relative flex items-start w-full">
                            <div className="flex h-6 items-center">
                                <input
                                id="comments"
                                aria-describedby="comments-description"
                                name="comments"
                                type="checkbox"
                                className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                />
                            </div>
                            <div className="ml-3 text-sm leading-6">
                                <span>  Key Encipherment</span>
                            </div>
                        </div>
                        <div className="relative flex items-start w-full">
                            <div className="flex h-6 items-center">
                                <input
                                id="comments"
                                aria-describedby="comments-description"
                                name="comments"
                                type="checkbox"
                                className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                />
                            </div>
                            <div className="ml-3 text-sm leading-6">
                                <span>  Data Encipherment</span>
                            </div>
                        </div>
                        <div className="relative flex items-start w-full">
                            <div className="flex h-6 items-center">
                                <input
                                id="comments"
                                aria-describedby="comments-description"
                                name="comments"
                                type="checkbox"
                                className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                />
                            </div>
                            <div className="ml-3 text-sm leading-6">
                                <span> Key Agreement</span>
                            </div>
                        </div>
                        <div className="relative flex items-start w-full">
                            <div className="flex h-6 items-center">
                                <input
                                id="comments"
                                aria-describedby="comments-description"
                                name="comments"
                                type="checkbox"
                                className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                />
                            </div>
                            <div className="ml-3 text-sm leading-6">
                                <span> Certificate Sign</span>
                            </div>
                        </div>
                        <div className="relative flex items-start w-full">
                            <div className="flex h-6 items-center">
                                <input
                                id="comments"
                                aria-describedby="comments-description"
                                name="comments"
                                type="checkbox"
                                className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                />
                            </div>
                            <div className="ml-3 text-sm leading-6">
                                <span>  CRL Sign</span>
                            </div>
                        </div>
                        <div className="relative flex items-start w-full">
                            <div className="flex h-6 items-center">
                                <input
                                id="comments"
                                aria-describedby="comments-description"
                                name="comments"
                                type="checkbox"
                                className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                />
                            </div>
                            <div className="ml-3 text-sm leading-6">
                                <span> Encipher Only</span>
                            </div>
                        </div>
                        <div className="relative flex items-start w-full">
                            <div className="flex h-6 items-center">
                                <input
                                id="comments"
                                aria-describedby="comments-description"
                                name="comments"
                                type="checkbox"
                                className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                />
                            </div>
                            <div className="ml-3 text-sm leading-6">
                                <span> Decipher Only</span>
                            </div>
                        </div>
                        <hr className='my-5'/>
                        <label htmlFor="" className='font-medium mb-3'>Extended Key Usage</label>
                        <div className="relative flex items-start w-full">
                            <div className="flex h-6 items-center">
                                <input
                                id="comments"
                                aria-describedby="comments-description"
                                name="comments"
                                type="checkbox"
                                className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                />
                            </div>
                            <div className="ml-3 text-sm leading-6">
                                <span>Critical (will mark below selected properties as critical)</span>
                            </div>
                        </div>
                        <div className="relative flex items-start w-full">
                            <div className="flex h-6 items-center">
                                <input
                                id="comments"
                                aria-describedby="comments-description"
                                name="comments"
                                type="checkbox"
                                className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                />
                            </div>
                            <div className="ml-3 text-sm leading-6">
                                <span>Server Authentication</span>
                            </div>
                        </div>
                        <div className="relative flex items-start w-full">
                            <div className="flex h-6 items-center">
                                <input
                                id="comments"
                                aria-describedby="comments-description"
                                name="comments"
                                type="checkbox"
                                className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                />
                            </div>
                            <div className="ml-3 text-sm leading-6">
                                <span> Client Authentication</span>
                            </div>
                        </div>
                        <div className="relative flex items-start w-full">
                            <div className="flex h-6 items-center">
                                <input
                                id="comments"
                                aria-describedby="comments-description"
                                name="comments"
                                type="checkbox"
                                className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                />
                            </div>
                            <div className="ml-3 text-sm leading-6">
                                <span>Code Signing</span>
                            </div>
                        </div>
                        <div className="relative flex items-start w-full">
                            <div className="flex h-6 items-center">
                                <input
                                id="comments"
                                aria-describedby="comments-description"
                                name="comments"
                                type="checkbox"
                                className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                />
                            </div>
                            <div className="ml-3 text-sm leading-6">
                                <span> E-mail</span>
                            </div>
                        </div>
                        <div className="relative flex items-start w-full">
                            <div className="flex h-6 items-center">
                                <input
                                id="comments"
                                aria-describedby="comments-description"
                                name="comments"
                                type="checkbox"
                                className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                />
                            </div>
                            <div className="ml-3 text-sm leading-6">
                                <span>Timestamping</span>
                            </div>
                        </div>
                        <div className="relative flex items-start w-full">
                            <div className="flex h-6 items-center">
                                <input
                                id="comments"
                                aria-describedby="comments-description"
                                name="comments"
                                type="checkbox"
                                className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                />
                            </div>
                            <div className="ml-3 text-sm leading-6">
                                <span>OCSP Signing</span>
                            </div>
                        </div>
                    </div>
                    </>
                    }
                    
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Create</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>
            </div> 
        </>
		
	);
};

