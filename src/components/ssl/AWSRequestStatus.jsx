import React, { useMemo, useState } from 'react';
import DataTable from 'react-data-table-component';
import HelpBox from '../HelpBox';
import Modal from '../Modal';

import AWSLogo from "../../images/aws.svg"

import { 
    CreditCardIcon,
    ArrowDownOnSquareIcon,
    ArrowUpOnSquareIcon,
    EyeIcon,
    EnvelopeIcon,
    ArrowLeftIcon,
    MagnifyingGlassIcon,
    
    } from '@heroicons/react/24/outline';
import Dropdown from '../Dropdown';
import { Menu } from '@headlessui/react';
import MultiSelectListBox from '../MultiSelectListBox';
import { Link } from 'react-router-dom';
import DropdownBorderless from '../DropdownBorderless';

export default function AWSRequestStatus({setMasterTab}){
    const columns = [
        {
            name: 'Common Name',
            selector: row => row.CommonName,
            sortable: true,  
        },
        {
            name: 'Product Name',
            selector: row => row.ProductName,
            sortable: true,            
        },
        {
            name: 'Order Number',
            selector: row => row.OrderNumber,
            sortable: true,
        },
        {
            name: 'Valid From',
            selector: row => row.ValidFrom,
            sortable: true,
        },
        {
            name: 'Valid To',
            selector: row => row.ValidTo,
            sortable: true,
        },
        {
            name: 'Requested By',
            selector: row => row.RequestedBy,
            sortable: true,
        },
        {
            name: 'Purchase Date',
            selector: row => row.PurchaseDate,
            sortable: true,
            
        },
        {
            name: 'Status',
            selector: row => row.Status,
            sortable: true,
            
        },
        
    ];    

const data = [
    {
        CommonName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        ProductName :   "admin",
        OrderNumber : "1234567890",
        ValidFrom : "Jan 28, 2019 18:16",
        ValidTo :"Jan 28, 2019 18:16",
        RequestedBy : "JKS", 
        PurchaseDate : "May 27, 2020 11:49", 
        Status: <span className="inline-flex items-center rounded-full bg-red-50 px-2 py-1 text-xs font-medium text-red-700 ring-1 ring-inset ring-red-600/10">Expired</span>,    
    }, 
    {
        CommonName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        CertificateID :   "admin",
        CertificateType : "Jan 28, 2019 18:16",
        ValidFrom : "4096",
        ValidTo :"RSA",
        RequestedBy : "JKS", 
        PurchaseDate : "May 27, 2020 11:49", 
        Status:  <span className="inline-flex items-center rounded-full bg-yellow-50 px-2 py-1 text-xs font-medium text-yellow-800 ring-1 ring-inset ring-yellow-600/20">Pending</span>,    
    }, 
    {
        CommonName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        CertificateID :   "admin",
        CertificateType : "Jan 28, 2019 18:16",
        ValidFrom : "4096",
        ValidTo :"RSA",
        RequestedBy : "JKS", 
        PurchaseDate : "May 27, 2020 11:49", 
        Status: <span className="inline-flex items-center rounded-full bg-green-50 px-2 py-1 text-xs font-medium text-green-700 ring-1 ring-inset ring-green-600/20">Success</span>,    
    }, 
    {
        CommonName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        CertificateID :   "admin",
        CertificateType : "Jan 28, 2019 18:16",
        ValidFrom : "4096",
        ValidTo :"RSA",
        RequestedBy : "JKS", 
        PurchaseDate : "May 27, 2020 11:49", 
        Status: <span className="inline-flex items-center rounded-full bg-gray-50 px-2 py-1 text-xs font-medium text-gray-600 ring-1 ring-inset ring-gray-500/10">Other</span>,    
    }, 
  
];

const [records,setRecords] = useState(data);

function handleFilter(event){
    const newData = records.filter(row=>{
        return row.ResourceName.toLowerCase().includes(event.target.value.toLowerCase())
    })
    setRecords(newData)
}

	return (
        <>
            <div className="px-4 sm:px-6 lg:px-8 flex items-center text">
                <div className='text-xl theme-gray8 my-3 flex w-full items-center'>
                    <h1 className='text-xl theme-gray8 my-3 flex w-full gap-3 items-center'>
                        <span className='w-5 h-5 '><img src={AWSLogo} alt="" /></span><span>AWS Request Status</span>
                    </h1>
                    <small className='ml-auto'>
                        <Link to="/ssl/aws" className="hover:shadow-lg transition duration-300 ease-in-out bg-theme-gray2 rounded-full flex px-5 py-2 gap-3 items-center border border-theme-gray2 flex text-xs font-light">
                            <ArrowLeftIcon className="w-4 h-4"/> <span> Back</span>
                        </Link>
                    </small>
                </div>
            </div> 
            <div className="px-4 sm:px-6 lg:px-8">
            
            <div className='max-w-[500px] w-full mb-5 block'>
                <div className="flex">
                    <label htmlFor="search-dropdown" className="mb-2 text-sm font-medium text-gray-900 sr-only">Your Email</label>
                    <div className="relative w-full">
                        <input type="search" id="search-dropdown" className="block p-3 w-full z-20 text-sm text-gray-900 bg-gray-50 rounded-full border-s-2 border border-gray-300 focus:ring-blue-500 focus:border-blue-500" placeholder="Search..."/>
                        <button type="button" className="absolute top-0 end-0 p-3 text-sm font-medium h-full focus:ring-4 focus:outline-none focus:ring-blue-300">
                            <MagnifyingGlassIcon className='w-4 h-4'/>
                            <span className="sr-only">Search</span>
                        </button>
                    </div>
                </div>
            </div>
            
            <DataTable 
                columns={columns} 
                data={data} 
                fixedHeader
                pagination 
                selectableRows 
            />
            </div>

        </>
		
	);
};

