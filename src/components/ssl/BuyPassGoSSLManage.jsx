import React, { useMemo, useState } from 'react';
import DataTable from 'react-data-table-component';
import HelpBox from '../HelpBox';
import Modal from '../Modal';
import { 
    CreditCardIcon,
    ArrowDownOnSquareIcon,
    ArrowUpOnSquareIcon,
    EyeIcon,
    EnvelopeIcon,
    ArrowLeftIcon,
    MagnifyingGlassIcon,
    InformationCircleIcon,
    } from '@heroicons/react/24/outline';
import byPassLogo from "../../images/buypass.svg" 
import letsEncryptLogo from "../../images/lets-encrypt-logo.svg" 
import Dropdown from '../Dropdown';
import { Menu } from '@headlessui/react';
import { Link, NavLink } from 'react-router-dom';

export default function BuyPassGoSSLManage({setMasterTab}){
    const columns = [
        {
            name: 'Domain Names',
            selector: row => row.DomainName,
            sortable: true,  
        },
        {
            name: 'Serial Number',
            selector: row => row.SerialNumber,
            sortable: true,            
        },
        {
            name: 'Valid From',
            selector: row => row.ValidFrom,
            sortable: true,
        },
        {
            name: 'Valid To',
            selector: row => row.ValidTo,
            sortable: true,
        },
        {
            name: 'Requested By',
            selector: row => row.RequestedBy,
            sortable: true,
        },
        {
            name: 'Authorized',
            selector: row => row.Authorized,
            sortable: true,
        },
        {
            name: 'Processed By',
            selector: row => row.ProcessedBy,
            sortable: true,
        },
        {
            name: 'Requsted Status',
            selector: row => row.RequstedStatus,
            sortable: true,
        },
        {
            name: 'Certificate Status',
            selector: row => row.CertificateStatus,
            sortable: true,
        },
        {
            name: '',
            selector: row => row.CertificateHistory,
            sortable: false,
            maxWidth: "30px",
            minWidth: "50px"
        },
        {
            name: '',
            selector: row => row.PassPhrase,
            sortable: false,
            maxWidth: "30px",
            minWidth: "50px"
        },
    ];    

const data = [
    {
        DomainName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        SerialNumber :   "-",
        ValidFrom : "Jan 28, 2019 18:16",
        ValidTo : "Jan 28, 2019 18:16",
        RequestedBy : "demouser",
        Authorized : "demouser",
        ProcessedBy : "-",
        RequstedStatus : <a href="javascript:;">Pending</a>,
        CertificateStatus : <span className='min-w-12 bg-theme-gray2 p-2 block text-center rounded'>NA</span>,
        CertificateHistory: <CreditCardIcon className='w-5 h-5' />,
        PassPhrase:<EyeIcon className='w-5 h-5' />,
    }, 
    {
        DomainName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        SerialNumber :   "-",
        ValidFrom : "Jan 28, 2019 18:16",
        ValidTo : "Jan 28, 2019 18:16",
        RequestedBy : "demouser",
        Authorized : "demouser",
        ProcessedBy : "-",
        RequstedStatus : <a href="javascript:;">Pending</a>,
        CertificateStatus : <span className='min-w-12 bg-theme-gray2 p-2 block text-center rounded'>NA</span>,
        CertificateHistory: <CreditCardIcon className='w-5 h-5' />,
        PassPhrase:<EyeIcon className='w-5 h-5' />,
    }, 
    {
        DomainName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        SerialNumber :   "-",
        ValidFrom : "Jan 28, 2019 18:16",
        ValidTo : "Jan 28, 2019 18:16",
        RequestedBy : "demouser",
        Authorized : "demouser",
        ProcessedBy : "-",
        RequstedStatus : <a href="javascript:;">Pending</a>,
        CertificateStatus : <span className='min-w-12 bg-theme-gray2 p-2 block text-center rounded'>Expired</span>,
        CertificateHistory: <CreditCardIcon className='w-5 h-5' />,
        PassPhrase:<EyeIcon className='w-5 h-5' />,
    }, 
    {
        DomainName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        SerialNumber :   "-",
        ValidFrom : "Jan 28, 2019 18:16",
        ValidTo : "Jan 28, 2019 18:16",
        RequestedBy : "demouser",
        Authorized : "demouser",
        ProcessedBy : "-",
        RequstedStatus : <a href="javascript:;">Pending</a>,
        CertificateStatus : <span className='min-w-12 bg-theme-gray2 p-2 block text-center rounded'>Expired</span>,
        CertificateHistory: <CreditCardIcon className='w-5 h-5' />,
        PassPhrase:<EyeIcon className='w-5 h-5' />,
    }, 
    {
        DomainName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        SerialNumber :   "-",
        ValidFrom : "Jan 28, 2019 18:16",
        ValidTo : "Jan 28, 2019 18:16",
        RequestedBy : "demouser",
        Authorized : "demouser",
        ProcessedBy : "-",
        RequstedStatus : <a href="javascript:;">Pending</a>,
        CertificateStatus : <span className='min-w-12 bg-theme-gray2 p-2 block text-center rounded'>Expired</span>,
        CertificateHistory: <CreditCardIcon className='w-5 h-5' />,
        PassPhrase:<EyeIcon className='w-5 h-5' />,
    }, 
   
];

const [records,setRecords] = useState(data);

const [isOpenModal,setIsOpenModal] = useState(false);
const [isOpenModalDelete2,setIsOpenModalDelete2] = useState(false);
const [isOpenModalDNS,setIsOpenModalDNS] = useState(false);


function handleFilter(event){
    const newData = records.filter(row=>{
        return row.ResourceName.toLowerCase().includes(event.target.value.toLowerCase())
    })
    setRecords(newData)
}

const navigation = [
  { name: 'Account', href: 'javascript:;', status:""},
  { name: 'DNS', href: 'javascript:;', status:"active"},
  { name: 'Deploy', href: 'javascript:;', status:""},
  { name: 'Windows Agents', href: 'javascript:;', status:""}
]

	return (
        <>
            <div className="px-4 sm:px-6 lg:px-8 flex items-center text">
                <div className='text-xl theme-gray8 my-3 flex w-full items-center'>
                    <h1 className='text-xl theme-gray8 my-3 flex w-full gap-3 items-center'>
                        <span className='w-5 h-5 '><img src={byPassLogo} alt="" /></span><span>Manage</span>
                    </h1>
                    <small className='ml-auto'>
                        <Link to="/ssl/acme-buy-pass-go-ssl" className="hover:shadow-lg transition duration-300 ease-in-out bg-theme-gray2 rounded-full flex px-5 py-2 gap-3 items-center border border-theme-gray2 flex text-xs font-light">
                            <ArrowLeftIcon className="w-4 h-4"/> <span> Back</span>
                        </Link>
                    </small>
                </div>
            </div> 
            <div className="px-4 sm:px-6 lg:px-8">
            <div className='flex flex-wrap gap-1 mb-4 text-sm'>
                <ul className="flex items-center">
                    {navigation.map((item) => (
                        <li key={item.name}>
                            <a
                                href={item.href}
                                className={`${item.status == "active" ? "border-b-2 border-theme-red1": "border-b"} text-gray-600 hover:text-black hover:bg-theme-gray1 group flex items-center px-3 py-2 text-sm leading-6`}
                                >
                                <span>{item.name}</span>
                            </a>
                        </li>    
                    ))}
                </ul>       
            </div>
            <div className='max-w-[500px] w-full mb-5 block'>
                <div className="flex">
                    <label htmlFor="search-dropdown" className="mb-2 text-sm font-medium text-gray-900 sr-only">Your Email</label>
                    <div className="relative w-full">
                        <input type="search" id="search-dropdown" className="block p-3 w-full z-20 text-sm text-gray-900 bg-gray-50 rounded-full border-s-2 border border-gray-300 focus:ring-blue-500 focus:border-blue-500" placeholder="Search..."/>
                        <button type="button" className="absolute top-0 end-0 p-3 text-sm font-medium h-full focus:ring-4 focus:outline-none focus:ring-blue-300">
                            <MagnifyingGlassIcon className='w-4 h-4'/>
                            <span className="sr-only">Search</span>
                        </button>
                    </div>
                </div>
            </div>
            
            <DataTable 
                columns={columns} 
                data={data} 
                fixedHeader
                pagination 
                selectableRows 
            />
            </div>
        </>
		
	);
};

