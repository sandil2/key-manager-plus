import React, { useMemo, useState } from 'react';
import { ChevronRightIcon } from '@heroicons/react/24/outline';
import { Menu } from '@headlessui/react';
import Dropdown from '../Dropdown';
import DataTable from 'react-data-table-component';
import { 
    ArrowDownTrayIcon,
    UserGroupIcon, 
    ArrowLeftIcon,
    PresentationChartLineIcon,
    Square3Stack3DIcon} from '@heroicons/react/24/outline';
import Modal from '../Modal';
import HelpBox from '../HelpBox';
import { Link } from 'react-router-dom';

export default function SslCertificateGroup({setMasterTab}){

     const columns = [
        
        {
            name: 'Group Name',
            selector: row => row.GroupName,
            sortable: true,            
        },
        {
            name: 'Description',
            selector: row => row.Description,
            sortable: true,
        },
        {
            name: 'Group Email',
            selector: row => row.GroupEmail,
            sortable: true,
        },
        {
            name: 'Create By',
            selector: row => row.CreateBy,
            sortable: true,
        },
        {
            name: 'Key Size',
            selector: row => row.LastModiBy,
            sortable: true,
        },
        {
            name: '',
            selector: row => row.IconKey,
            sortable: false,
            maxWidth: "30px",
            minWidth: "50px"
        },
    ];    

    const data = [
        {
            GroupName : <a href="javascript:;" className='text-theme-blue1'>	RootCA</a>  ,
            Description : "	RootCA",
            GroupEmail : "RootCA@adas.com",
            CreateBy : "admin",
            LastModiBy : "admin",
            IconKey: <UserGroupIcon className='w-5 h-4 text-black-700 cursor-pointer'/>,
        },
        {
            GroupName : <a href="javascript:;" className='text-theme-blue1'>	RootCA</a>  ,
            Description : "	RootCA",
            GroupEmail : "RootCA@adas.com",
            CreateBy : "admin",
            LastModiBy : "admin",
            IconKey: <UserGroupIcon className='w-5 h-4 text-black-700 cursor-pointer'/>,
        },
        {
            GroupName : <a href="javascript:;" className='text-theme-blue1'>	RootCA</a>  ,
            Description : "	RootCA",
            GroupEmail : "RootCA@adas.com",
            CreateBy : "admin",
            LastModiBy : "admin",
            IconKey: <UserGroupIcon className='w-5 h-4 text-black-700 cursor-pointer'/>,
        },
        {
            GroupName : <a href="javascript:;" className='text-theme-blue1'>	RootCA</a>  ,
            Description : "	RootCA",
            GroupEmail : "RootCA@adas.com",
            CreateBy : "admin",
            LastModiBy : "admin",
            IconKey: <UserGroupIcon className='w-5 h-4 text-black-700 cursor-pointer'/>,
        },
        
    ];

    const helpTxt = [
        "Choose a root certificate and click Sign option in the top menu to sign and issue certificates to specific user groups.",
        "The sign type User Management allows you to create and issue certificates to Key Manager Plus users.",
        "The sign type Active Directory Users allows you create and issue certificates to Active Directory users within your network. Choose the domain name, provide the domain controller details, username and password. Specify the users / user groups to be imported as CSV or select the user / user groups using Groups/OU tree import type.",
        "Enabling Use root certificate details option causes the user certificate to inherit all the parameters of its root certificate."
    ]
    
    

	return (
        <>
            <div className="px-4 sm:px-6 lg:px-8 items-center gap-4 text">
                <h1 className='text-xl theme-gray8 my-3 flex'>
                    <span>Certificate Group</span>
                    <small className='ml-auto'>
                        <Link to="/ssl/certificate" className="hover:shadow-lg transition duration-300 ease-in-out bg-theme-gray2 rounded-full flex px-5 py-2 gap-3 items-center border border-theme-gray2 flex text-xs font-light">
                            <ArrowLeftIcon className="w-4 h-4"/> <span> Back</span>
                        </Link>
                    </small>
                </h1>
                <div className='flex flex-wrap gap-1 my-4 text-sm'>
                    <Link to="/ssl/add-certificate-group" className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                        <span>Add Group</span>
                    </Link>
                    <button className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                        <span>Delete</span>
                    </button>
                    <button className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                        <span>Update Additional Fields</span>
                    </button>
                </div> 
                
                <DataTable 
                    columns={columns} 
                    data={data} 
                    fixedHeader
                    pagination 
                    selectableRows 
                />
                 
            </div> 
        </>
		
	);
};

