import React, { useMemo, useState } from 'react';
import DataTable from 'react-data-table-component';
import { ArrowLeftIcon } from '@heroicons/react/24/outline';
import { Link } from 'react-router-dom';

export default function ZeroSSLCR({setMasterTab}){
    const [discoverBy,setDiscoverBy] = useState("hostname");
	return (
        <>
            <div className="px-4 sm:px-6 lg:px-8">    
                <h1 className='text-xl theme-gray8 my-3 flex'>
                    <span>Certificate Request</span>
                    <small className='ml-auto'>
                        <Link to="/ssl/acme-zero-ssl" className="hover:shadow-lg transition duration-300 ease-in-out bg-theme-gray2 rounded-full flex px-5 py-2 gap-3 items-center border border-theme-gray2 flex text-xs font-light">
                            <ArrowLeftIcon className="w-4 h-4"/> <span> Back</span>
                        </Link>
                    </small>
                </h1>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-2">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Select Option
                    </label>
                    <div className="flex w-full">
                        <legend className="sr-only">Certificate Store</legend>
                        <div className="flex w-full gap-7">
                            <div className="flex items-center">
                                <input
                                    id="hostname"
                                    name="discover-by"
                                    type="radio"
                                    defaultChecked={discoverBy === 'hostname'}
                                    value="hostname"
                                    onClick={() => setDiscoverBy('hostname')}
                                    className="accent-theme-red1 h-4 w-4 border-gray-300"
                                />
                                <label htmlFor="password" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                    Create CSR
                                </label>
                            </div>
                            <div className="flex items-center">
                                <input
                                    id="ip-name"
                                    name="discover-by"
                                    type="radio"
                                    defaultChecked={discoverBy === 'ip-name'}
                                    value="ip-name"
                                    onClick={() => setDiscoverBy('ip-name')}
                                    className="accent-theme-red1 h-4 w-4 border-gray-300 text-theme-red1 focus:ring-theme-red1"
                                />
                                <label htmlFor="ip-name" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                    Create CSR From KeyStore
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-2">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Common Name
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                </div>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-2">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Choose From Template
                    </label>
                    <div>
                        <div className='flex justify-between rounded-md shadow-sm ring-1 focus:ring-0 ring-inset ring-gray-300 w-full'>
                            <select
                            className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            >
                                <option>Windows</option>
                                <option>Linux/Mac OS</option>
                            </select>
                        </div>
                        <p className='text-right'>
                            <small>
                                <a href="javascript:;" className='text-theme-red1'>Manage CSR Templates</a>
                            </small>
                        </p>
                    </div>
                </div>
            </div>
            
            <div className="px-4 sm:px-6 lg:px-8 py-3">
                <div className='flex'>
                    <button className='hover:shadow-lg transition duration-300 ease-in-out bg-black text-white hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center'>
                        <span>Create</span>
                    </button>
                    <Link to="/ssh/keys" className='hover:shadow-lg transition duration-300 ease-in-out hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center'>
                        <span>Cancel</span>
                    </Link>
                </div>
                
            </div>
        </>
		
	);
};

