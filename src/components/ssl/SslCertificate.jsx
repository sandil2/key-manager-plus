import React, { useMemo, useState } from 'react';
import DataTable from 'react-data-table-component';
import HelpBox from '../HelpBox';
import Modal from '../Modal';
import { 
    ClipboardDocumentListIcon,
    ServerStackIcon,
    KeyIcon,
    CreditCardIcon,
    ComputerDesktopIcon,
    DocumentChartBarIcon,
    TrashIcon,
    ArrowDownOnSquareIcon,
    EyeIcon,
    BookOpenIcon,
    MagnifyingGlassIcon,
    PencilSquareIcon,
    CommandLineIcon,
    RectangleStackIcon,
    UserGroupIcon, 
    PresentationChartLineIcon,
    Square3Stack3DIcon} from '@heroicons/react/24/outline';
import Dropdown from '../Dropdown';
import { Menu } from '@headlessui/react';
import { Link } from 'react-router-dom';

export default function SslCertificate({setMasterTab}){
    const columns = [
        {
            name: '',
            selector: row => row.Scan,
            sortable: true,  
            maxWidth: "30px",
            minWidth: "50px"          
        },
        {
            name: 'Common Name',
            selector: row => row.CommonName,
            sortable: true,            
        },
        {
            name: 'DNS Name',
            selector: row => row.DNSName,
            sortable: true,
        },
        {
            name: 'Issuer',
            selector: row => row.Issuer,
            sortable: true,
        },
        {
            name: 'Valid To',
            selector: row => row.ValidTo,
            sortable: true,
        },
        {
            name: 'Days',
            selector: row => row.Days,
            sortable: true,
        },
        {
            name: 'Key Size',
            selector: row => row.KeySize,
            sortable: true,
        },
        {
            name: 'Signature Algorithm',
            selector: row => row.SignatureAlgorithm,
            sortable: false,
        },
        {
            name: 'Domain Expiration',
            selector: row => row.DomainExpiration,
            sortable: false,
        },
        {
            name: 'SAN',
            selector: row => row.SAN,
            sortable: false,
        },
        {
            name: '',
            selector: row => row.IconKey,
            sortable: false,
            maxWidth: "30px",
            minWidth: "50px"
        },
        {
            name: '',
            selector: row => row.IconSingleServer,
            sortable: false,
            maxWidth: "30px",
            minWidth: "50px"
        },
        {
            name: '',
            selector: row => row.IconCertiHistory,
            sortable: false,
            maxWidth: "30px",
            minWidth: "50px"
        },
    ];    

const data = [
    {
        Scan : <PresentationChartLineIcon className="w-4 h-4 text-black-700"/>,
        CommonName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>  ,
        DNSName : "NA",
        Issuer : "NA",
        ValidTo :<span className='text-red-500'>Feb 6, 2024 11:08</span>,
        Days : "Feb 9, 2017 15:52", 
        KeySize: "-",
        SignatureAlgorithm: "-",
        DomainExpiration: "-",
        SAN: "-",
        IconKey: <ServerStackIcon className='w-5 h-4 text-black-700'/>,
        IconSingleServer: <ClipboardDocumentListIcon className='w-5 h-4 text-black-700 cursor-pointer' onClick={()=>setIsOpenModalCreateDeploy(true)}/>,
        IconCertiHistory: <UserGroupIcon className='w-5 h-4 text-black-700 cursor-pointer'/>,
    }, 
    {
        Scan : <PresentationChartLineIcon className="w-4 h-4 text-black-700"/>,
        CommonName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>  ,
        DNSName : "NA",
        Issuer : "NA",
        ValidTo :<span className='text-yellow-500'>March 6, 2024 11:08</span>,
        Days : "Feb 9, 2017 15:52", 
        KeySize: "-",
        SignatureAlgorithm: "-",
        DomainExpiration: "-",
        SAN: "-",
        IconKey: <ServerStackIcon className='w-5 h-4 text-black-700'/>,
        IconSingleServer: <ClipboardDocumentListIcon className='w-5 h-4 text-black-700 cursor-pointer' onClick={()=>setIsOpenModalCreateDeploy(true)}/>,
        IconCertiHistory: <UserGroupIcon className='w-5 h-4 text-black-700 cursor-pointer'/>,
    }, 
    {
        Scan : <PresentationChartLineIcon className="w-4 h-4 text-black-700"/>,
        CommonName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>  ,
        DNSName : "NA",
        Issuer : "NA",
        ValidTo :<span className='text-red-500'>March 6, 2024 11:08</span>,
        Days : "Feb 9, 2017 15:52", 
        KeySize: "-",
        SignatureAlgorithm: "-",
        DomainExpiration: "-",
        SAN: "-",
        IconKey: <ServerStackIcon className='w-5 h-4 text-black-700'/>,
        IconSingleServer: <ClipboardDocumentListIcon className='w-5 h-4 text-black-700 cursor-pointer' onClick={()=>setIsOpenModalCreateDeploy(true)}/>,
        IconCertiHistory: <UserGroupIcon className='w-5 h-4 text-black-700 cursor-pointer'/>,
    }, 
    {
        Scan : <PresentationChartLineIcon className="w-4 h-4 text-black-700"/>,
        CommonName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>  ,
        DNSName : "NA",
        Issuer : "NA",
        ValidTo :<span className='text-green-500'>March 6, 2024 11:08</span>,
        Days : "Feb 9, 2017 15:52", 
        KeySize: "-",
        SignatureAlgorithm: "-",
        DomainExpiration: "-",
        SAN: "-",
        IconKey: <ServerStackIcon className='w-5 h-4 text-black-700'/>,
        IconSingleServer: <ClipboardDocumentListIcon className='w-5 h-4 text-black-700 cursor-pointer' onClick={()=>setIsOpenModalCreateDeploy(true)}/>,
        IconCertiHistory: <UserGroupIcon className='w-5 h-4 text-black-700 cursor-pointer'/>,
    }, 
    {
        Scan : <PresentationChartLineIcon className="w-4 h-4 text-black-700"/>,
        CommonName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>  ,
        DNSName : "NA",
        Issuer : "NA",
        ValidTo :"192.168.15.25",
        Days : "Feb 9, 2017 15:52", 
        KeySize: "-",
        SignatureAlgorithm: "-",
        DomainExpiration: "-",
        SAN: "-",
        IconKey: <ServerStackIcon className='w-5 h-4 text-black-700'/>,
        IconSingleServer: <ClipboardDocumentListIcon className='w-5 h-4 text-black-700 cursor-pointer' onClick={()=>setIsOpenModalCreateDeploy(true)}/>,
        IconCertiHistory: <UserGroupIcon className='w-5 h-4 text-black-700 cursor-pointer'/>,
    }, 
    {
        Scan : <PresentationChartLineIcon className="w-4 h-4 text-black-700"/>,
        CommonName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>  ,
        DNSName : "NA",
        Issuer : "NA",
        ValidTo :"192.168.15.25",
        Days : "Feb 9, 2017 15:52", 
        KeySize: "-",
        SignatureAlgorithm: "-",
        DomainExpiration: "-",
        SAN: "-",
        IconKey: <ServerStackIcon className='w-5 h-4 text-black-700'/>,
        IconSingleServer: <ClipboardDocumentListIcon className='w-5 h-4 text-black-700 cursor-pointer' onClick={()=>setIsOpenModalCreateDeploy(true)}/>,
        IconCertiHistory: <UserGroupIcon className='w-5 h-4 text-black-700 cursor-pointer'/>,
    }, 
  
];

const [records,setRecords] = useState(data);

const [isOpenModal,setIsOpenModal] = useState(false);
const [isOpenModalDelete,setIsOpenModalDelete] = useState(false);
const [isOpenModalALS,setIsOpenModalALS] = useState(false);
const [isOpenModalEnumerate,setIsOpenModalEnumerate] = useState(false);
const [isOpenModalCreateDeploy,setIsOpenModalCreateDeploy] = useState(false);

const [isOpenModalWindows,setIsOpenModalWindows] = useState(false);
const [isOpenModalMCS,setIsOpenModalMCS] = useState(false);
const [isOpenModalLinux,setIsOpenModalLinux] = useState(false);
const [isOpenModalMDM,setIsOpenModalMDM] = useState(false);
const [isOpenModalAWS_ACM,setIsOpenModalAWS_ACM] = useState(false);
const [isOpenModalLBD,setIsOpenModalLBD] = useState(false);
const [isOpenModalEditCerti,setIsOpenModalEditCerti] = useState(false);
const [isOpenModalADS,setIsOpenModalADS] = useState(false);
const [isOpenModalImportKey,setIsOpenModalImportKey] = useState(false);
const [isOpenModalImportIssuer,setIsOpenModalImportIssuer] = useState(false);
const [isOpenModalCreateGrp,setIsOpenModalCreateGrp] = useState(false);
const [isOpenModalDWD,setIsOpenModalDWD] = useState(false);
const [isOpenModalsyncCMDB,setIsOpenModalsyncCMDB] = useState(false);

const [devType,setDevType] = useState("Single");
const [credType,setCredType] = useState("Password");

const [discoverBy,setDiscoverBy] = useState("dmz");
const [useCSA,setUseCSA] = useState(false);



function handleFilter(event){
    const newData = records.filter(row=>{
        return row.ResourceName.toLowerCase().includes(event.target.value.toLowerCase())
    })
    setRecords(newData)
}

	return (
        <>
            <div className="px-4 sm:px-6 lg:px-8">
            <div className='flex flex-wrap gap-1 my-4 text-sm'>
                <button className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2" onClick={()=>setIsOpenModal(true)}>
                    <span>Add</span>
                </button>
                <Link to="/ssl/create-certificate" className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                    <span>Create</span>
                </Link>
                <div className='z-[10]'>
                    <Dropdown DropText={"Deploy"}>
                        <div className="px-1 py-1 " role="none">
                            <Menu.Item onClick={()=>setIsOpenModalWindows(true)}>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Windows
                                </button>
                                )}
                            </Menu.Item>
                            <Menu.Item onClick={()=>setIsOpenModalMCS(true)}>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Microsoft Certificate Store
                                </button>
                                )} 
                            </Menu.Item>
                            <Menu.Item>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Internet Information Services (IIS)
                                </button>
                                )} 
                            </Menu.Item>
                            <Menu.Item>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    IIS Binding
                                </button>
                                )} 
                            </Menu.Item>
                            <Menu.Item onClick={()=>setIsOpenModalLinux(true)}>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Linus
                                </button>
                                )} 
                            </Menu.Item>
                            <Menu.Item>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Browser
                                </button>
                                )} 
                            </Menu.Item>
                            <Menu.Item onClick={()=>setIsOpenModalMDM(true)}>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    ManageEngine MDM
                                </button>
                                )} 
                            </Menu.Item>
                            <Menu.Item onClick={()=>setIsOpenModalAWS_ACM(true)}>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    AWS-ACM
                                </button>
                                )} 
                            </Menu.Item>
                            <Menu.Item onClick={()=>setIsOpenModalLBD(true)}>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    LoadBalancer
                                </button>
                                )} 
                            </Menu.Item>
                        </div>
                    </Dropdown>
                </div>
                <button className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2" onClick={()=>setIsOpenModal(true)}>
                    <span>Renew</span>
                </button>
                <div className='z-[10]'>
                    <Dropdown DropText={"More"}>
                        <div className="px-1 py-1 " role="none">
                            <Menu.Item onClick={()=>setIsOpenModalEditCerti(true)}>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Edit
                                </button>
                                )}
                            </Menu.Item>
                            <Menu.Item onClick={()=>setIsOpenModalADS(true)}>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Add Deployed Server
                                </button>
                                )} 
                            </Menu.Item>
                            <Menu.Item>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Re Discover
                                </button>
                                )} 
                            </Menu.Item>
                            <Menu.Item onClick={()=>setIsOpenModalImportKey(true)}>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Import Key
                                </button>
                                )} 
                            </Menu.Item>
                            <Menu.Item onClick={()=>setIsOpenModalImportIssuer(true)}>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Import Issuer
                                </button>
                                )} 
                            </Menu.Item>
                            <Menu.Item>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Mark as Root
                                </button>
                                )} 
                            </Menu.Item>
                            <Menu.Item onClick={()=>setIsOpenModalCreateGrp(true)}>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Create Group
                                </button>
                                )} 
                            </Menu.Item>
                            <Menu.Item>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Scan Vulnerabilities
                                </button>
                                )} 
                            </Menu.Item>
                            <Menu.Item onClick={()=>setIsOpenModalDWD(true)}>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Check Domain Expiry
                                </button>
                                )} 
                            </Menu.Item>
                            <Menu.Item onClick={()=>setIsOpenModalsyncCMDB(true)}>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Sync with CMDB
                                </button>
                                )} 
                            </Menu.Item>
                            <Menu.Item>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Create CSR
                                </button>
                                )} 
                            </Menu.Item>
                            <Menu.Item onClick={()=>setIsOpenModalDelete(true)}>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Delete
                                </button>
                                )} 
                            </Menu.Item>
                        </div>
                    </Dropdown>
                </div>
                
                
                <Link to="/ssl/private-ca" className="hover:shadow-lg transition text-xs text-red-600 duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-3 py-3 gap-2 items-center border border-theme-gray2 ml-auto">
                    <CreditCardIcon className='w-4 h-4'/> <span>Private CA</span>  
                </Link>
                <Link to="/ssl/windows-agents" className="hover:shadow-lg transition text-xs text-red-600 duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-3 py-3 gap-2 items-center border border-theme-gray2" >
                    <ComputerDesktopIcon className='w-4 h-4'/> <span>Windows Agents</span>
                </Link>
                <Link to="/ssl/root-certificate" className="hover:shadow-lg transition text-xs text-red-600 duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-3 py-3 gap-2 items-center border border-theme-gray2">
                    <DocumentChartBarIcon className='w-4 h-4'/> <span>Root Certificate</span>
                </Link>
                <Link to="/ssl/certificate-group" className="hover:shadow-lg transition text-xs text-red-600 duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-3 py-3 gap-2 items-center border border-theme-gray2">
                    <Square3Stack3DIcon className='w-4 h-4'/> <span>Certificate Group </span>
                </Link>

            </div>
            <div className='max-w-[500px] w-full mb-5 block'>
                <div className="flex">
                    <label htmlFor="search-dropdown" className="mb-2 text-sm font-medium text-gray-900 sr-only">Your Email</label>
                    <div className="relative w-full">
                        <input type="search" id="search-dropdown" className="block p-3 w-full z-20 text-sm text-gray-900 bg-gray-50 rounded-full border-s-2 border border-gray-300 focus:ring-blue-500 focus:border-blue-500" placeholder="Search..."/>
                        <button type="button" className="absolute top-0 end-0 p-3 text-sm font-medium h-full focus:ring-4 focus:outline-none focus:ring-blue-300">
                            <MagnifyingGlassIcon className='w-4 h-4'/>
                            <span className="sr-only">Search</span>
                        </button>
                    </div>
                </div>
            </div>
            
            <DataTable 
                columns={columns} 
                data={data} 
                fixedHeader
                pagination 
                selectableRows 
            />
            </div>

            <Modal title={'Add Credentials'} isOpenModal={isOpenModal} setIsOpenModal={setIsOpenModal}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Select Option
                    </label>
                    <div className="flex w-full my-5">
                        <legend className="sr-only">Select Option</legend>
                        <div className="w-full gap-7">
                            <div className="flex items-center">
                                <input
                                    id="dmz"
                                    name="discover-by"
                                    type="radio"
                                    defaultChecked={discoverBy === 'dmz'}
                                    value="dmz"
                                    onClick={() => setDiscoverBy('dmz')}
                                    className="accent-theme-red1 h-4 w-4 border-gray-300"
                                />
                                <label htmlFor="dmz" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                    DMZ
                                </label>
                            </div>
                            <div className="flex items-center">
                                <input
                                    id="cs"
                                    name="discover-by"
                                    type="radio"
                                    defaultChecked={discoverBy === 'cs'}
                                    value="cs"
                                    onClick={() => setDiscoverBy('cs')}
                                    className="accent-theme-red1 h-4 w-4 border-gray-300 text-theme-red1 focus:ring-theme-red1"
                                />
                                <label htmlFor="cs" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                    Certificate Store
                                </label>
                            </div>
                            <div className="flex items-center">
                                <input
                                    id="mca"
                                    name="discover-by"
                                    type="radio"
                                    defaultChecked={discoverBy === 'mca'}
                                    value="mca"
                                    onClick={() => setDiscoverBy('mca')}
                                    className="accent-theme-red1 h-4 w-4 border-gray-300 text-theme-red1 focus:ring-theme-red1"
                                />
                                <label htmlFor="mca" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                    Microsoft Certificate Authority 
                                </label>
                            </div>
                            <div className="flex items-center">
                                <input
                                    id="dir"
                                    name="discover-by"
                                    type="radio"
                                    defaultChecked={discoverBy === 'dir'}
                                    value="dir"
                                    onClick={() => setDiscoverBy('dir')}
                                    className="accent-theme-red1 h-4 w-4 border-gray-300 text-theme-red1 focus:ring-theme-red1"
                                />
                                <label htmlFor="dir" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                    Directory
                                </label>
                            </div>
                        </div>
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        File Location
                    </label>
                    <div className="">
                        <div className='flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full max-w-sm'>
                            <input
                            id="location"
                            name="location"
                            type="file"
                            className="accent-theme-red1 w-full rounded border-gray-300"
                            />
                        </div>
                        <p>
                            <small>(File format should be .cer / .crt / .pem / .der / .p7b)</small>
                        </p>
                    </div>
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Add</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full' onClick={()=>setIsOpenModal(false)}>Cancel</button>
                </div> 
            </Modal>
            

            <Modal title={'Landing Server Association'} isOpenModal={isOpenModalALS} setIsOpenModal={setIsOpenModalALS}>
                <div className="grid grid-cols-[100%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                        Landing Server
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full max-w-sm">
                        <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                            <option value="RSA">RSA</option>
                            <option value="DSA">DSA</option>
                            <option value="ECDSA">ECDSA</option>
                            <option value="ED25519">ED25519</option>
                        </select>
                    </div>
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Associate</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>
            
            <Modal title={'Confirmation'} isOpenModal={isOpenModalDelete} setIsOpenModal={setIsOpenModalDelete}>
                <div className="grid grid-cols-[100%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                        Delete selected resource(s)?
                    </label>
                    <div className="relative flex items-start max-w-xs w-full">
                        <div className="flex h-6 items-center">
                            <input
                            id="comments"
                            aria-describedby="comments-description"
                            name="comments"
                            type="checkbox"
                            className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                            />
                        </div>
                        <div className="ml-3 text-sm leading-6">
                            <span> Delete without disassociating the key </span>
                        </div>
                    </div>
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Ok</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>
            <Modal title={'Available User Credentials'} isOpenModal={isOpenModalEnumerate} setIsOpenModal={setIsOpenModalEnumerate}>
                <div className="grid grid-cols-[100%] max-w-xl mb-7 gap-y-5 items-center">
                    
                    <div className="relative flex items-start w-full items-center">
                        <div className="flex rounded-md overflow-hidden ring-1 ring-inset ring-gray-300 w-full max-w-sm border">
                            <div id="dropdownSearch" className="z-10 bg-white w-full">
                                <ul className="h-48 px-3 pb-3 overflow-y-auto text-sm text-gray-700" aria-labelledby="dropdownSearchButton">
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-1" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-1" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">User Name</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-2" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-2" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Test</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-3" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-3" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (Milan) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-4" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-4" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">AWS GovCloud (US-East)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-5" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-5" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Canada (Central)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-6" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-6" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (Frankfurt) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-7" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-7" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US ISO West</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-8" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-8" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US West (N. California) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-9" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-9" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US West (Oregon) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-10" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-10" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Africa (Cape Town) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-11" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-11" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (Paris) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-12" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-12" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (Stockholm) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-13" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-13" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (London)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-14" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-14" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (Ireland)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-15" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-15" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Osaka) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-16" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-16" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Seoul)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-17" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-17" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Tokyo)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-18" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-18" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Middle East (Bahrain)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-19" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-19" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">South America (Sao Paulo)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-20" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-20" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Hong Kong)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-21" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-21" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">China (Beijing)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-22" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-22" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">AWS GovCloud (US)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-23" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-23" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Singapore) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-24" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-24" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Sydney)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-25" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-25" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US ISO East </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-26" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-26" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US East (N. Virginia)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-27" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-27" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US East (Ohio)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-28" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-28" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">China (Ningxia)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-29" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-29" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US ISOB East (Ohio) </label>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                        </div>
                        <div className="ml-3 text-sm leading-6 w-60">
                            &nbsp;
                        </div>
                    </div>
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Enumerate</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div> 
                
            </Modal>
            
            <Modal title={'Create and Deploy'} isOpenModal={isOpenModalCreateDeploy} setIsOpenModal={setIsOpenModalCreateDeploy}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Key Comment
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Key Type
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full max-w-sm">
                        <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                            <option value="RSA">RSA</option>
                            <option value="DSA">DSA</option>
                            <option value="ECDSA">ECDSA</option>
                            <option value="ED25519">ED25519</option>
                        </select>
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Key Length
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full max-w-sm">
                        <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                            <option value="1024">1024</option>
                            <option value="2048">2048</option>
                            <option value="4096">4096</option>
                        </select>
                    </div>
                    
                   
                    <label htmlFor="">&nbsp;</label>
                    <div className="relative flex items-start max-w-xs w-full">
                        <div className="flex h-6 items-center">
                            <input
                            id="comments"
                            aria-describedby="comments-description"
                            name="comments"
                            type="checkbox"
                            className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                            />
                        </div>
                        <div className="ml-3 text-sm leading-6">
                            <span> Root / Administrator</span>
                        </div>
                    </div>
                    <label htmlFor="">&nbsp;</label>
                    <div className="relative flex items-start max-w-xs w-full">
                        <small>(Use this option if the direct root user login is disabled. Enabling this option elevates a user login from a non-root user to a root user.)</small>
                    </div>
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Deploy</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>
            

            <Modal title={'Certificate Deployment - Windows'} isOpenModal={isOpenModalWindows} setIsOpenModal={setIsOpenModalWindows}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Deployment Type
                    </label>
                    <div className="flex w-full my-5">
                        <legend className="sr-only">Deployment Type</legend>
                        <div className="w-full flex gap-7">
                            <div className="flex items-center">
                                <input
                                    id="Single"
                                    name="dev-type"
                                    type="radio"
                                    defaultChecked={devType === 'Single'}
                                    value="Single"
                                    onClick={() => setDevType('Single')}
                                    className="accent-theme-red1 h-4 w-4 border-gray-300"
                                />
                                <label htmlFor="Single" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                    Single
                                </label>
                            </div>
                            <div className="flex items-center">
                                <input
                                    id="Multiple"
                                    name="dev-type"
                                    type="radio"
                                    defaultChecked={devType === 'Multiple'}
                                    value="Multiple"
                                    onClick={() => setDevType('Multiple')}
                                    className="accent-theme-red1 h-4 w-4 border-gray-300 text-theme-red1 focus:ring-theme-red1"
                                />
                                <label htmlFor="Multiple" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                    Multiple (Servers)
                                </label>
                            </div>
                            <div className="flex items-center">
                                <input
                                    id="Agent"
                                    name="dev-type"
                                    type="radio"
                                    defaultChecked={devType === 'Agent'}
                                    value="Agent"
                                    onClick={() => setDevType('Agent')}
                                    className="accent-theme-red1 h-4 w-4 border-gray-300 text-theme-red1 focus:ring-theme-red1"
                                />
                                <label htmlFor="Agent" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                    Agent
                                </label>
                            </div>
                        </div>
                    </div>

                    {devType === 'Single' &&
                        <>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            Server Name
                        </label>
                        <div className="">
                            <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                <input
                                    type="text"
                                    autoComplete=""
                                    className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                    placeholder=""
                                />
                            </div>
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            &nbsp;
                        </label>
                        <div className="">
                            <div className="relative flex items-start max-w-xs w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="use-cas"
                                    name="use-cas"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    defaultChecked={useCSA}
                                    onChange={()=>setUseCSA(!useCSA)}
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span> Use Cryptobind service account credentials for authentication </span>
                                </div>
                            </div>
                        </div>
                        {useCSA &&
                            <>
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                User Name
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="text"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                            </div>
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Password
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="text"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                            </div>
                            </>
                        }
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Path
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="text"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                            </div>
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                File Type
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full max-w-sm">
                                    <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                        <option value="RSA">RSA</option>
                                        <option value="DSA">DSA</option>
                                        <option value="ECDSA">ECDSA</option>
                                        <option value="ED25519">ED25519</option>
                                    </select>
                                </div>
                            </div>
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Certificate File Name 
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="text"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                            </div>
                         
                        </>
                    }
                    {devType === 'Multiple' &&
                        <>
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                File Location 
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="file"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                            </div>
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                File Type
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                        <option value="CER">CER</option>
                                        <option value="CRT">CRT</option>
                                        <option value="DER">DER</option>
                                        <option value="P7B">P7B</option>
                                    </select>
                                </div>
                            </div>
                        </>
                    }

                    {devType === 'Agent' &&
                        <>
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Select Agent
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                        <option value="CER">CER</option>
                                        <option value="CRT">CRT</option>
                                        <option value="DER">DER</option>
                                    </select>
                                </div>
                                <p className='text-right'>
                                    <small>
                                        <a href="javascript:;" className='text-theme-red1'>Manage</a> 
                                    </small>
                                </p>
                            </div>
                            
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Path
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="text"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                            </div>
                            
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                File Type
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                        <option value="CER">CER</option>
                                        <option value="CRT">CRT</option>
                                        <option value="DER">DER</option>
                                    </select>
                                </div>
                            </div>

                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Certificate File Name 
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="text"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                            </div>
                        </>
                    }
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Deploy</button>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Save</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full' onClick={()=>setIsOpenModal(false)}>Cancel</button>
                </div> 
            </Modal>

            <Modal title={'Certificate Deployment - MS Store'} isOpenModal={isOpenModalMCS} setIsOpenModal={setIsOpenModalMCS}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Deployment Type
                    </label>
                    <div className="flex w-full my-5">
                        <legend className="sr-only">Deployment Type</legend>
                        <div className="w-full flex gap-7">
                            <div className="flex items-center">
                                <input
                                    id="Single"
                                    name="dev-type"
                                    type="radio"
                                    defaultChecked={devType === 'Single'}
                                    value="Single"
                                    onClick={() => setDevType('Single')}
                                    className="accent-theme-red1 h-4 w-4 border-gray-300"
                                />
                                <label htmlFor="Single" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                    Single
                                </label>
                            </div>
                            <div className="flex items-center">
                                <input
                                    id="Multiple"
                                    name="dev-type"
                                    type="radio"
                                    defaultChecked={devType === 'Multiple'}
                                    value="Multiple"
                                    onClick={() => setDevType('Multiple')}
                                    className="accent-theme-red1 h-4 w-4 border-gray-300 text-theme-red1 focus:ring-theme-red1"
                                />
                                <label htmlFor="Multiple" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                    Multiple (Servers)
                                </label>
                            </div>
                            <div className="flex items-center">
                                <input
                                    id="Agent"
                                    name="dev-type"
                                    type="radio"
                                    defaultChecked={devType === 'Agent'}
                                    value="Agent"
                                    onClick={() => setDevType('Agent')}
                                    className="accent-theme-red1 h-4 w-4 border-gray-300 text-theme-red1 focus:ring-theme-red1"
                                />
                                <label htmlFor="Agent" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                    Agent
                                </label>
                            </div>
                        </div>
                    </div>

                    {devType === 'Single' &&
                        <>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            Server Name
                        </label>
                        <div className="">
                            <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                <input
                                    type="text"
                                    autoComplete=""
                                    className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                    placeholder=""
                                />
                            </div>
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            &nbsp;
                        </label>
                        <div className="">
                            <div className="relative flex items-start max-w-xs w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="use-cas"
                                    name="use-cas"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    defaultChecked={useCSA}
                                    onChange={()=>setUseCSA(!useCSA)}
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span> Use Cryptobind service account credentials for authentication </span>
                                </div>
                            </div>
                        </div>
                        {useCSA &&
                            <>
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                User Name
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="text"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                            </div>
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Password
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="text"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                            </div>
                            </>
                        }
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Path
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="text"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                            </div>
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Select Account
                            </label>
                            <div className="">
                                <div className="relative flex items-start max-w-xs w-full">
                                    <div className="flex h-6 items-center">
                                        <input
                                        id="comments"
                                        aria-describedby="comments-description"
                                        name="comments"
                                        type="checkbox"
                                        className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                        />
                                    </div>
                                    <div className="ml-3 text-sm leading-6">
                                        <span> Computer </span>
                                    </div>
                                </div>
                                <div className="relative flex items-start max-w-xs w-full">
                                    <div className="flex h-6 items-center">
                                        <input
                                        id="comments"
                                        aria-describedby="comments-description"
                                        name="comments"
                                        type="checkbox"
                                        className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                        />
                                    </div>
                                    <div className="ml-3 text-sm leading-6">
                                        <span> User </span>
                                    </div>
                                </div>
                            </div>
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Store Name
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="text"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                                <p className='text-right'>
                                    <small>
                                        <a href="javascript:;" className='text-theme-red1'>Get Stores</a>
                                    </small>
                                </p>
                            </div>
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                &nbsp;
                            </label>
                            <div className="">
                                <div className="relative flex items-start max-w-xs w-full">
                                    <div className="flex h-6 items-center">
                                        <input
                                        id="use-cas"
                                        name="use-cas"
                                        type="checkbox"
                                        className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                        />
                                    </div>
                                    <div className="ml-3 text-sm leading-6">
                                        <span>  Enable PrivateKey Export from MS Certificate Store after deployment. </span>
                                    </div>
                                </div>
                            </div>      
                         
                        </>
                    }
                    {devType === 'Multiple' &&
                        <>
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                File Location 
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="file"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                            </div>
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Select Account
                            </label>
                            <div className="">
                                <div className="relative flex items-start max-w-xs w-full">
                                    <div className="flex h-6 items-center">
                                        <input
                                        id="comments"
                                        aria-describedby="comments-description"
                                        name="comments"
                                        type="checkbox"
                                        className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                        />
                                    </div>
                                    <div className="ml-3 text-sm leading-6">
                                        <span> Computer </span>
                                    </div>
                                </div>
                                <div className="relative flex items-start max-w-xs w-full">
                                    <div className="flex h-6 items-center">
                                        <input
                                        id="comments"
                                        aria-describedby="comments-description"
                                        name="comments"
                                        type="checkbox"
                                        className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                        />
                                    </div>
                                    <div className="ml-3 text-sm leading-6">
                                        <span> User </span>
                                    </div>
                                </div>
                            </div>
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Select Account
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                        <option value="CER">CER</option>
                                        <option value="CRT">CRT</option>
                                        <option value="DER">DER</option>
                                        <option value="P7B">P7B</option>
                                    </select>
                                </div>
                            </div>
                        </>
                    }

                    {devType === 'Agent' &&
                        <>
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Select Agent
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                        <option value="CER">CER</option>
                                        <option value="CRT">CRT</option>
                                        <option value="DER">DER</option>
                                    </select>
                                </div>
                                <p className='text-right'>
                                    <small>
                                        <a href="javascript:;" className='text-theme-red1'>Manage</a> 
                                    </small>
                                </p>
                            </div>
                            
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Path
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="text"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                            </div>
                            
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                File Type
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                        <option value="CER">CER</option>
                                        <option value="CRT">CRT</option>
                                        <option value="DER">DER</option>
                                    </select>
                                </div>
                            </div>

                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Certificate File Name 
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="text"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                            </div>
                        </>
                    }
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Deploy</button>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Save</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full' onClick={()=>setIsOpenModal(false)}>Cancel</button>
                </div> 
            </Modal>

            <Modal title={'Certificate Deployment - Linux'} isOpenModal={isOpenModalLinux} setIsOpenModal={setIsOpenModalLinux}>
                <div className="grid grid-cols-[100%] max-w-xl items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900" style={{width:"max-content"}}>
                        Certificate Deployment - Linux
                    </label>
                    <div className="flex w-full my-5">
                        <legend className="sr-only">Certificate Deployment - Linux</legend>
                        <div className="w-full flex gap-7">
                            <div className="flex items-center">
                                <input
                                    id="Single"
                                    name="dev-type"
                                    type="radio"
                                    defaultChecked={devType === 'Single'}
                                    value="Single"
                                    onClick={() => setDevType('Single')}
                                    className="accent-theme-red1 h-4 w-4 border-gray-300"
                                />
                                <label htmlFor="Single" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                    Single
                                </label>
                            </div>
                            <div className="flex items-center">
                                <input
                                    id="Multiple"
                                    name="dev-type"
                                    type="radio"
                                    defaultChecked={devType === 'Multiple'}
                                    value="Multiple"
                                    onClick={() => setDevType('Multiple')}
                                    className="accent-theme-red1 h-4 w-4 border-gray-300 text-theme-red1 focus:ring-theme-red1"
                                />
                                <label htmlFor="Multiple" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                    Multiple (Servers)
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    {devType === 'Single' &&
                        <>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            Server Name
                        </label>
                        <div className="">
                            <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                <input
                                    type="text"
                                    autoComplete=""
                                    className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                    placeholder=""
                                />
                            </div>
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            Port
                        </label>
                        <div className="">
                            <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                <input
                                    type="text"
                                    autoComplete=""
                                    className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                    placeholder=""
                                />
                            </div>
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            User Name
                        </label>
                        <div className="">
                            <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                <input
                                    type="text"
                                    autoComplete=""
                                    className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                    placeholder=""
                                />
                            </div>
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            Credential Type
                        </label>
                        <div className="flex w-full my-5">
                        <legend className="sr-only">Credential Type</legend>
                        <div className="w-full gap-7 flex">
                            <div className="flex items-center">
                                <input
                                    id="Password"
                                    name="credType"
                                    type="radio"
                                    defaultChecked={credType === 'Password'}
                                    value="Password"
                                    onClick={() => setCredType('Password')}
                                    className="accent-theme-red1 h-4 w-4 border-gray-300"
                                />
                                <label htmlFor="Password" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                    Password
                                </label>
                            </div>
                            <div className="flex items-center">
                                <input
                                    id="ImportKey"
                                    name="credType"
                                    type="radio"
                                    defaultChecked={credType === 'Import Key'}
                                    value="Import Key"
                                    onClick={() => setCredType('Import Key')}
                                    className="accent-theme-red1 h-4 w-4 border-gray-300 text-theme-red1 focus:ring-theme-red1"
                                />
                                <label htmlFor="ImportKey" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                    Import Key
                                </label>
                            </div>
                        </div>
                    </div>
                        
                        {credType == "Password" &&
                            <>
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Password
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="text"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                            </div>
                           </>
                        }    
                        {credType != "Import Key" &&
                            <>
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                User PrivateKey
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="file"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                            </div>
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Passphrase
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="text"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                            </div>
                           </>
                        }    
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Path
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="text"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                            </div>
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                File Type
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                        <option value="CER">CER</option>
                                        <option value="CRT">CRT</option>
                                        <option value="DER">DER</option>
                                        <option value="P7B">P7B</option>
                                    </select>
                                </div>
                            </div>
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Certificate File Name
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="text"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                            </div>
                        </>
                    }
                    {devType === 'Multiple' &&
                        <>
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                File Location 
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="file"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                            </div>
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                File Type
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                        <option value="CER">CER</option>
                                        <option value="CRT">CRT</option>
                                        <option value="DER">DER</option>
                                        <option value="P7B">P7B</option>
                                    </select>
                                </div>
                            </div>
                        </>
                    }

                    {devType === 'Agent' &&
                        <>
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Select Agent
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                        <option value="CER">CER</option>
                                        <option value="CRT">CRT</option>
                                        <option value="DER">DER</option>
                                    </select>
                                </div>
                                <p className='text-right'>
                                    <small>
                                        <a href="javascript:;" className='text-theme-red1'>Manage</a> 
                                    </small>
                                </p>
                            </div>
                            
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Path
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="text"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                            </div>
                            
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                File Type
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                        <option value="CER">CER</option>
                                        <option value="CRT">CRT</option>
                                        <option value="DER">DER</option>
                                    </select>
                                </div>
                            </div>

                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Certificate File Name 
                            </label>
                            <div className="">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="text"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                            </div>
                        </>
                    }
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Deploy</button>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Save</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full' onClick={()=>setIsOpenModal(false)}>Cancel</button>
                </div> 
            </Modal>

            <Modal title={'Certificate Deployment - ManageEngine MDM'} isOpenModal={isOpenModalMDM} setIsOpenModal={setIsOpenModalMDM}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Device Platform
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full max-w-sm">
                        <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                            <option value="1024">1024</option>
                            <option value="2048">2048</option>
                            <option value="4096">4096</option>
                        </select>
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Key Type
                    </label>
                    <div className="relative flex items-start w-full items-center">
                        <div className="flex rounded-md overflow-hidden ring-1 ring-inset ring-gray-300 w-full border">
                            <div id="dropdownSearch" className="z-10 bg-white w-full">
                                <ul className="h-48 px-3 pb-3 overflow-y-auto text-sm text-gray-700" aria-labelledby="dropdownSearchButton">
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-1" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-1" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">User Name</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-2" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-2" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Test</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-3" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-3" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (Milan) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-4" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-4" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">AWS GovCloud (US-East)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-5" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-5" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Canada (Central)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-6" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-6" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (Frankfurt) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-7" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-7" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US ISO West</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-8" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-8" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US West (N. California) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-9" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-9" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US West (Oregon) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-10" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-10" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Africa (Cape Town) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-11" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-11" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (Paris) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-12" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-12" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (Stockholm) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-13" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-13" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (London)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-14" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-14" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (Ireland)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-15" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-15" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Osaka) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-16" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-16" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Seoul)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-17" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-17" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Tokyo)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-18" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-18" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Middle East (Bahrain)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-19" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-19" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">South America (Sao Paulo)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-20" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-20" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Hong Kong)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-21" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-21" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">China (Beijing)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-22" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-22" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">AWS GovCloud (US)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-23" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-23" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Singapore) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-24" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-24" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Sydney)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-25" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-25" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US ISO East </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-26" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-26" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US East (N. Virginia)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-27" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-27" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US East (Ohio)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-28" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-28" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">China (Ningxia)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-29" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-29" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US ISOB East (Ohio) </label>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Deploy</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>        
            <Modal title={'Certificate Deployment - AWS'} isOpenModal={isOpenModalAWS_ACM} setIsOpenModal={setIsOpenModalAWS_ACM}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        AWS Credential
                    </label>
                    <div>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full max-w-sm">
                        <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                            <option value="1024">1024</option>
                            <option value="2048">2048</option>
                            <option value="4096">4096</option>
                        </select>
                    </div>
                    <p className='text-right'>
                        <small> 
                            <a href="javascript:;" className='text-theme-red1'>Manage AWS Credential</a>
                        </small>
                    </p>
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Region
                    </label>
                    <div className="relative flex items-start w-full items-center">
                        <div className="flex rounded-md overflow-hidden ring-1 ring-inset ring-gray-300 w-full border">
                            <div id="dropdownSearch" className="z-10 bg-white w-full">
                                <ul className="h-48 px-3 pb-3 overflow-y-auto text-sm text-gray-700" aria-labelledby="dropdownSearchButton">
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-10" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-10" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Africa (Cape Town) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-11" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-11" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (Paris) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-12" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-12" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (Stockholm) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-13" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-13" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (London)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-14" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-14" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (Ireland)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-15" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-15" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Osaka) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-16" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-16" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Seoul)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-17" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-17" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Tokyo)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-23" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-23" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Singapore) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-24" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-24" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Sydney)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-25" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-25" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US ISO East </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-26" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-26" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US East (N. Virginia)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-27" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-27" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US East (Ohio)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-28" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-28" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">China (Ningxia)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-29" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-29" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US ISOB East (Ohio) </label>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        &nbsp;
                    </label>
                    <div className="">
                        <div className="relative flex items-start max-w-xs w-full">
                            <div className="flex h-6 items-center">
                                <input
                                id="use-cas"
                                name="use-cas"
                                type="checkbox"
                                className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                defaultChecked={useCSA}
                                onChange={()=>setUseCSA(!useCSA)}
                                />
                            </div>
                            <div className="ml-3 text-sm leading-6">
                                <span>  Deploy and replace if the same certificate is found in ACM</span>
                            </div>
                        </div>
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        &nbsp;
                    </label>
                    <div className="">
                        <div className="relative flex items-start max-w-xs w-full">
                            <div className="flex h-6 items-center">
                                <input
                                id="use-cas"
                                name="use-cas"
                                type="checkbox"
                                className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                defaultChecked={useCSA}
                                onChange={()=>setUseCSA(!useCSA)}
                                />
                            </div>
                            <div className="ml-3 text-sm leading-6">
                                <span>Automatically re-deploy the certificate to ACM upon renewal.</span>
                            </div>
                        </div>
                    </div>
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Deploy</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>        
            <Modal title={'Load Balancer Deployment'} isOpenModal={isOpenModalLBD} setIsOpenModal={setIsOpenModalLBD}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Load Balancer Type
                    </label>
                    
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full max-w-sm">
                        <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                            <option value="1024">1024</option>
                            <option value="2048">2048</option>
                            <option value="4096">4096</option>
                        </select>
                    </div>
                    
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Citrix Credentials List
                    </label>
                    <div>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full max-w-sm">
                            <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                <option value="1024">1024</option>
                                <option value="2048">2048</option>
                                <option value="4096">4096</option>
                            </select>
                        </div>
                        <p className='text-right'>
                            <small> 
                                <a href="javascript:;" className='text-theme-red1'>Manage Credentials</a>
                            </small>
                        </p>
                    </div>
                    
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Passphrase
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="password"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        &nbsp;
                    </label>
                    <div className="">
                        <div className="relative flex items-start max-w-xs w-full">
                            <div className="flex h-6 items-center">
                                <input
                                id="use-cas"
                                name="use-cas"
                                type="checkbox"
                                className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                defaultChecked={useCSA}
                                onChange={()=>setUseCSA(!useCSA)}
                                />
                            </div>
                            <div className="ml-3 text-sm leading-6">
                                <span>  Bypass Proxy Settings</span>
                            </div>
                        </div>
                    </div>
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Deploy</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>        
            <Modal title={'Edit Certificate'} isOpenModal={isOpenModalEditCerti} setIsOpenModal={setIsOpenModalEditCerti}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        DNS Name
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Description
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <textarea name="" className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"  id="" rows="4"></textarea>
                    </div>
                    
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Expiry Notification Email
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>

                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Type
                    </label>
                    <div>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full max-w-sm">
                            <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                <option value="1024">1024</option>
                                <option value="2048">2048</option>
                                <option value="4096">4096</option>
                            </select>
                        </div>
                    </div>

                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        &nbsp;
                    </label>
                    <div className="">
                        <div className="relative flex items-start max-w-xs w-full">
                            <div className="flex h-6 items-center">
                                <input
                                id="use-cas"
                                name="use-cas"
                                type="checkbox"
                                className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                defaultChecked={useCSA}
                                onChange={()=>setUseCSA(!useCSA)}
                                />
                            </div>
                            <div className="ml-3 text-sm leading-6">
                                <span>  Bypass Proxy Settings</span>
                            </div>
                        </div>
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Address1
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Address2
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Phone no
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Date1
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="date"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Date2
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="date"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Email
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="email"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Sample Values
                    </label>
                    <div>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full max-w-sm">
                            <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                <option value="1024">1024</option>
                                <option value="2048">2048</option>
                                <option value="4096">4096</option>
                            </select>
                        </div>
                    </div>
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Save</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>        
            <Modal title={'Add Deployed Server'} isOpenModal={isOpenModalADS} setIsOpenModal={setIsOpenModalADS}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        DNS Name
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        IP Address
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Port
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Sync Check Timeout
                    </label>
                    <div className="flex justify-center items-center rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                        <small className='px-2'>sec</small>
                    </div>

                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        &nbsp;
                    </label>
                    <div className="">
                        <div className="relative flex items-start max-w-xs w-full">
                            <div className="flex h-6 items-center">
                                <input
                                id="use-cas"
                                name="use-cas"
                                type="checkbox"
                                className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                defaultChecked={useCSA}
                                onChange={()=>setUseCSA(!useCSA)}
                                />
                            </div>
                            <div className="ml-3 text-sm leading-6">
                                <span>  Bypass Proxy Settings</span>
                            </div>
                        </div>
                    </div>
                    {useCSA &&
                    <>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            Select Agent
                        </label>
                        <div>
                            <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full max-w-sm">
                                <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                    <option value="1024">1024</option>
                                    <option value="2048">2048</option>
                                    <option value="4096">4096</option>
                                </select>
                            </div>
                        </div>
                    </>
                        
                    }
                    
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Save</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>        
            <Modal title={'Import Key'} isOpenModal={isOpenModalImportKey} setIsOpenModal={setIsOpenModalImportKey}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        File Location
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="file"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Keystore Password
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="password"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    

                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        &nbsp;
                    </label>
                    <div className="">
                        <div className="relative flex items-start max-w-xs w-full">
                            <div className="flex h-6 items-center">
                                <input
                                id="use-cas"
                                name="use-cas"
                                type="checkbox"
                                className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                defaultChecked={useCSA}
                                onChange={()=>setUseCSA(!useCSA)}
                                />
                            </div>
                            <div className="ml-3 text-sm leading-6">
                                <span>Validate with existing key</span>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Import</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>        
            <Modal title={'Import Issuer Certificate(s)'} isOpenModal={isOpenModalImportIssuer} setIsOpenModal={setIsOpenModalImportIssuer}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        File Location
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="file"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                   <label htmlFor="">
                    &nbsp;
                   </label>
                   <div>
                    <small>(File format should be .cer / .crt / .pem / .der / .p7b)</small>
                   </div>
                    
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Import</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>   
            <Modal title={'Create Group'} isOpenModal={isOpenModalCreateGrp} setIsOpenModal={setIsOpenModalCreateGrp}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Group Name
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Description
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div> 
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Create</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>   
            <Modal title={'Domain WhoIs Data'} isOpenModal={isOpenModalDWD} setIsOpenModal={setIsOpenModalDWD}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Domain Name
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        Admin
                    </div>
                    
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Create</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>   
            <Modal title={'Confirmation'} isOpenModal={isOpenModalsyncCMDB} setIsOpenModal={setIsOpenModalsyncCMDB}>
                <div className="grid grid-cols-[100%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                        Enable ServiceDesk Plus CMDB sync ?
                    </label>
                    
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Ok</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>   

        </>
		
	);
};

