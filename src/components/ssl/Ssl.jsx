import { useOutletContext } from 'react-router-dom'
import Header from '../../components/Header'
import HelpBox from '../HelpBox';
import { useState } from 'react';
import DropdownBorderless from '../DropdownBorderless';
import { Menu } from '@headlessui/react';

import acmeLogo from "../../images/acme-logo.svg" 
import goDaddyLogo from "../../images/goDaddy.svg" 
import sslStore from "../../images/ssl-store.svg" 
import sectigoLogo from "../../images/sectigo.svg" 
import symantecLogo from "../../images/symantec.svg" 
import digiCertLogo from "../../images/digicert_dynamic_blade.svg"
import letsEncryptLogo from "../../images/lets-encrypt-logo.svg" 
import byPassLogo from "../../images/buypass.svg" 
import zeroSslLogo from "../../images/zero-ssl.svg" 
import globalSignLogo from "../../images/globalsign-logo.svg"
import MDMLogo from "../../images/me-mdm.svg"
import AWSLogo from "../../images/aws.svg"
import MSCALogo from "../../images/MicrosoftCertificateAuthority.svg"
import AzureLogo from "../../images/azure.svg"
import kubernetesLogo from "../../images/kubernetes.svg"
import entrustLogo from "../../images/entrust.svg"


import SslCertificate from './SslCertificate';
import SslCreateCertificate from './SslCreateCertificate';
import SslCreateCertificatePrivateCA from './SslCertificatePrivateCA';
import SslCreateCertificateWindowsAgents from './SslCertificateWindowsAgents';
import SslRootCertificate from './SslRootCertificate';
import SslCertificateGroup from './SslCertificateGroup';
import AddCertificateGroup from './AddCertificateGroup';
import SslCSR from './SslCSR';
import SslGoDaddy from './SslGoDaddy';
import SslStore from './SslStore';

export default function Ssl(){
    const [masterTab, setMasterTab, sidebarOpen, setSidebarOpen, setSidebarShrink, sidebarShrink] = useOutletContext();

    return (
    <div>
        <div className={`${sidebarShrink ? "lg:pl-72" : "lg:pl-24" } bg-theme-gray2`}>
            <div className='bg-theme-gray2'>
                <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
            </div>
            <div className='bg-white'>
                <main className="py-10">
                    <div className="pl-4 sm:pl-6 lg:pl-8 flex justify-between items-center pr-12 border border-b border-t-0 border-l-0 border-r-0">
                        {/* <ul className='flex gap-y-3 text-xs h-7 gap-x-3 w-full'>
                            <li className={`
                            ${masterTab == "ssl-certificate" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}
                            ${masterTab == "ssl-create-certificate" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}
                            ${masterTab == "ssl-create-certificate-private-ca" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}
                            ${masterTab == "ssl-create-certificate-windows-agents" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}
                            ${masterTab == "ssl-root-certificate" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}
                            ${masterTab == "ssl-certificate-group" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}
                            ${masterTab == "ssl-add-certificate-group" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}
                            `}>
                                <a href="javascript:;" onClick={()=>setMasterTab('ssl-certificate')}>
                                    <div className='flex items-center'>
                                        <div className='w-1 h-5 flex items-center pr-1'>&nbsp;</div> <span>Certificates</span>
                                    </div>
                                </a>
                            </li>
                            <li className={`
                            ${masterTab == "ssl-csr" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}
                            `}>
                                <a href="javascript:;" onClick={()=> setMasterTab("ssl-csr")}>
                                    <div className='flex items-center'>
                                        <div className='w-1 h-5 flex items-center pr-1'>&nbsp;</div> <span>CSR</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <DropdownBorderless DropText={"ACME"} DropIcon={<img src={acmeLogo} alt="acme"/>}>
                                    <div className="px-1 py-1 " role="none">
                                        <Menu.Item>
                                            {({ active }) => (
                                            <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm items-center`}>
                                                <div className='w-4 h-4 mr-1'><img src={letsEncryptLogo} alt="" /></div> Let's Encrypt
                                            </button>
                                            )}
                                        </Menu.Item>
                                        <Menu.Item>
                                            {({ active }) => (
                                            <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm items-center`}>
                                                <div className='w-4 h-4 mr-1'><img src={byPassLogo} alt="" /></div> Buypass Go SSL
                                            </button>
                                            )} 
                                        </Menu.Item>
                                        <Menu.Item>
                                            {({ active }) => (
                                            <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm items-center`}>
                                                <div className='w-4 h-4 mr-1'><img src={zeroSslLogo} alt="" /></div> ZeroSSL
                                            </button>
                                            )} 
                                        </Menu.Item>
                                    </div>
                                </DropdownBorderless>
                            </li>
                            <li className={`
                                ${masterTab == "ssl-godaddy" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}
                            `}>
                                <a href="javascript:;" onClick={()=> setMasterTab("ssl-godaddy")}>
                                    <div className='flex items-center'>
                                        <div className='w-5 h-5 flex items-center pr-1'><img src={goDaddyLogo} alt="" /></div> <span>GoDaddy</span>
                                    </div>
                                </a>
                            </li>
                            <li  className={`${masterTab == "ssl-store" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}`}>
                                <DropdownBorderless DropText={"The SSL Store"} DropIcon={<img src={sslStore} alt="acme"/>}>
                                    <div className="px-1 py-1 " role="none">
                                        <Menu.Item onClick={()=>setMasterTab("ssl-store")}>
                                            {({ active }) => (
                                            <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                                 <span>All SSL Stores</span>
                                            </button>
                                            )}
                                        </Menu.Item>
                                        <Menu.Item>
                                            {({ active }) => (
                                            <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                                <div className='w-4 h-4 mr-1'><img src={sectigoLogo} alt="" /></div> <span>Sectigo (formerly Comodo)</span>
                                            </button>
                                            )}
                                        </Menu.Item>
                                        <Menu.Item>
                                            {({ active }) => (
                                            <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                                <div className='w-4 h-4 mr-1'><img src={symantecLogo} alt="" /></div> <span>Symantec</span>
                                            </button>
                                            )} 
                                        </Menu.Item>
                                        <Menu.Item>
                                            {({ active }) => (
                                            <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                                <div className='w-4 h-4 mr-1'><img src={digiCertLogo} alt="" /></div> <span>DigiCert</span>
                                            </button>
                                            )} 
                                        </Menu.Item>
                                    </div>
                                </DropdownBorderless>
                            </li>
                            <li>
                                <div className='flex items-center'>
                                    <div className='w-5 h-5 flex items-center pr-1'><img src={digiCertLogo} alt="" /></div> <span>DigiCert</span>
                                </div>
                            </li>
                            <li>
                                <div className='flex items-center'>
                                    <div className='w-5 h-5 flex items-center pr-1'><img src={globalSignLogo} alt="" /></div> <span>GlobalSign</span>
                                </div>
                            </li>
                            <li>
                                <div className='flex items-center'>
                                    <div className='w-5 h-5 flex items-center pr-1'><img src={MDMLogo} alt="" /></div> <span>MDM</span>
                                </div>
                            </li>
                            <li>
                                <div className='flex items-center'>
                                    <div className='w-5 h-5 flex items-center pr-1'><img src={AWSLogo} alt="" /></div> <span>AWS</span>
                                </div>
                            </li>
                            <li>
                                <div className='flex items-center'>
                                    <div className='w-5 h-5 flex items-center pr-1'><img src={MSCALogo} alt="" /></div> <span>MSCA</span>
                                </div>
                            </li>
                            <li>
                                <DropdownBorderless DropText={"ACME"} DropIcon={<img src={AzureLogo} alt="azure"/>}>
                                    <div className="px-1 py-1 " role="none">
                                        <Menu.Item>
                                            {({ active }) => (
                                            <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                                <div className='w-4 h-4 mr-1'><img src={AzureLogo} alt="" /></div> <span>Azure Certificates</span>
                                            </button>
                                            )}
                                        </Menu.Item>
                                        <Menu.Item>
                                            {({ active }) => (
                                            <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                                <div className='w-4 h-4 mr-1'><img src={AzureLogo} alt="" /></div> <span>Azure Secrets</span>
                                            </button>
                                            )} 
                                        </Menu.Item>
                                    </div>
                                </DropdownBorderless>
                            </li>
                            <li>
                                <div className='flex items-center'>
                                    <div className='w-4 h-5 flex items-center pr-1'><img src={sectigoLogo} alt="" /></div> <span>Sectigo</span>
                                </div>
                            </li>
                            <li>
                                <div className='flex items-center'>
                                    <div className='w-6 h-5 flex items-center pr-1'><img src={kubernetesLogo} alt="" /></div> <span>Kubernetes</span>
                                </div>
                            </li>
                            <li>
                                <div className='flex items-center'>
                                    <div className='w-6 h-5 flex items-center pr-1'><img src={entrustLogo} alt="" /></div> <span>Entrust</span>
                                </div>
                            </li>
                        </ul> */}
                    </div>

                    {/* {masterTab == "ssl-certificate" && 
                        <SslCertificate setMasterTab={setMasterTab}/>
                    }
                    
                    {masterTab == "ssl-create-certificate" && 
                        <SslCreateCertificate setMasterTab={setMasterTab}/>
                    }

                    {masterTab == "ssl-create-certificate-private-ca" && 
                        <SslCreateCertificatePrivateCA  setMasterTab={setMasterTab}/>
                    }

                    {masterTab == "ssl-create-certificate-windows-agents" && 
                        <SslCreateCertificateWindowsAgents setMasterTab={setMasterTab}/>
                    }
                    {masterTab == "ssl-root-certificate" && 
                        <SslRootCertificate setMasterTab={setMasterTab}/>
                    }
                    {masterTab == "ssl-certificate-group" && 
                        <SslCertificateGroup setMasterTab={setMasterTab}/>
                    }

                    {masterTab == "ssl-add-certificate-group" && 
                        <AddCertificateGroup setMasterTab={setMasterTab}/>
                    }

                    {masterTab == "ssl-csr" && 
                        <SslCSR setMasterTab={setMasterTab}/>
                    }

                    {masterTab == "ssl-godaddy" && 
                        <SslGoDaddy setMasterTab={setMasterTab}/>
                    }

                    {masterTab == "ssl-store" && 
                        <SslStore setMasterTab={setMasterTab}/>
                    } */}


                    
                </main>
            </div>
        </div>
    </div>                
    )
}