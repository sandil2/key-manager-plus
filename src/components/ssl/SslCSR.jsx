import React, { useMemo, useState } from 'react';
import DataTable from 'react-data-table-component';
import HelpBox from '../HelpBox';
import Modal from '../Modal';
import { 
    CreditCardIcon,
    ArrowDownOnSquareIcon,
    ArrowUpOnSquareIcon,
    EyeIcon,
    EnvelopeIcon,
    MagnifyingGlassIcon,
    InformationCircleIcon,
    } from '@heroicons/react/24/outline';
import Dropdown from '../Dropdown';
import { Menu } from '@headlessui/react';
import { Link } from 'react-router-dom';

export default function SslCSR({setMasterTab}){
    const columns = [
        {
            name: 'Domain Name',
            selector: row => row.DomainName,
            sortable: true,  
        },
        {
            name: 'Created By',
            selector: row => row.CreatedBy,
            sortable: true,            
        },
        {
            name: 'Created Time',
            selector: row => row.CreatedTime,
            sortable: true,
        },
        {
            name: 'Key Size',
            selector: row => row.KeySize,
            sortable: true,
        },
        {
            name: 'Key Algorithm',
            selector: row => row.KeyAlgorithm,
            sortable: true,
        },
        {
            name: 'KeyStore Type',
            selector: row => row.KeyStoreType,
            sortable: true,
        },
        {
            name: 'Expiry Notification Email',
            selector: row => row.ENEmail,
            sortable: true,
        },
        {
            name: '',
            selector: row => row.View,
            sortable: false,
            maxWidth: "30px",
            minWidth: "50px"
        },
        {
            name: '',
            selector: row => row.Import,
            sortable: false,
            maxWidth: "30px",
            minWidth: "50px"
        },
        {
            name: '',
            selector: row => row.Export,
            sortable: false,
            maxWidth: "30px",
            minWidth: "50px"
        },
        {
            name: '',
            selector: row => row.Mail,
            sortable: false,
            maxWidth: "30px",
            minWidth: "50px"
        },
    ];    

const data = [
    {
        DomainName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        CreatedBy :   "admin",
        CreatedTime : "Jan 28, 2019 18:16",
        KeySize : "4096",
        KeyAlgorithm :"RSA",
        KeyStoreType : "JKS", 
        ENEmail: "-",
        View: <EyeIcon className='w-5 h-4 text-black-700'/>,
        Import: <ArrowDownOnSquareIcon className='w-5 h-4 text-black-700 cursor-pointer' />,
        Export: <ArrowUpOnSquareIcon className='w-5 h-4 text-black-700 cursor-pointer'/>,
        Mail: <EnvelopeIcon className='w-5 h-4 text-black-700 cursor-pointer'/>,
    }, 
    {
        DomainName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        CreatedBy :   "admin",
        CreatedTime : "Jan 28, 2019 18:16",
        KeySize : "4096",
        KeyAlgorithm :"RSA",
        KeyStoreType : "JKS", 
        ENEmail: "-",
        View: <EyeIcon className='w-5 h-4 text-black-700'/>,
        Import: <ArrowDownOnSquareIcon className='w-5 h-4 text-black-700 cursor-pointer' />,
        Export: <ArrowUpOnSquareIcon className='w-5 h-4 text-black-700 cursor-pointer'/>,
        Mail: <EnvelopeIcon className='w-5 h-4 text-black-700 cursor-pointer'/>,
    }, 
    {
        DomainName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        CreatedBy :   "admin",
        CreatedTime : "Jan 28, 2019 18:16",
        KeySize : "4096",
        KeyAlgorithm :"RSA",
        KeyStoreType : "JKS", 
        ENEmail: "-",
        View: <EyeIcon className='w-5 h-4 text-black-700'/>,
        Import: <ArrowDownOnSquareIcon className='w-5 h-4 text-black-700 cursor-pointer' />,
        Export: <ArrowUpOnSquareIcon className='w-5 h-4 text-black-700 cursor-pointer'/>,
        Mail: <EnvelopeIcon className='w-5 h-4 text-black-700 cursor-pointer'/>,
    }, 
    {
        DomainName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        CreatedBy :   "admin",
        CreatedTime : "Jan 28, 2019 18:16",
        KeySize : "4096",
        KeyAlgorithm :"RSA",
        KeyStoreType : "JKS", 
        ENEmail: "-",
        View: <EyeIcon className='w-5 h-4 text-black-700'/>,
        Import: <ArrowDownOnSquareIcon className='w-5 h-4 text-black-700 cursor-pointer' />,
        Export: <ArrowUpOnSquareIcon className='w-5 h-4 text-black-700 cursor-pointer'/>,
        Mail: <EnvelopeIcon className='w-5 h-4 text-black-700 cursor-pointer'/>,
    }, 
    {
        DomainName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        CreatedBy :   "admin",
        CreatedTime : "Jan 28, 2019 18:16",
        KeySize : "4096",
        KeyAlgorithm :"RSA",
        KeyStoreType : "JKS", 
        ENEmail: "-",
        View: <EyeIcon className='w-5 h-4 text-black-700'/>,
        Import: <ArrowDownOnSquareIcon className='w-5 h-4 text-black-700 cursor-pointer' />,
        Export: <ArrowUpOnSquareIcon className='w-5 h-4 text-black-700 cursor-pointer'/>,
        Mail: <EnvelopeIcon className='w-5 h-4 text-black-700 cursor-pointer'/>,
    }, 
    {
        DomainName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        CreatedBy :   "admin",
        CreatedTime : "Jan 28, 2019 18:16",
        KeySize : "4096",
        KeyAlgorithm :"RSA",
        KeyStoreType : "JKS", 
        ENEmail: "-",
        View: <EyeIcon className='w-5 h-4 text-black-700'/>,
        Import: <ArrowDownOnSquareIcon className='w-5 h-4 text-black-700 cursor-pointer' />,
        Export: <ArrowUpOnSquareIcon className='w-5 h-4 text-black-700 cursor-pointer'/>,
        Mail: <EnvelopeIcon className='w-5 h-4 text-black-700 cursor-pointer'/>,
    }, 
   
  
];

const [records,setRecords] = useState(data);

const [isOpenModal,setIsOpenModal] = useState(false);
const [isOpenModalImpCSR,setIsOpenModalImpCSR] = useState(false);
const [isOpenModalDelete2,setIsOpenModalDelete2] = useState(false);


function handleFilter(event){
    const newData = records.filter(row=>{
        return row.ResourceName.toLowerCase().includes(event.target.value.toLowerCase())
    })
    setRecords(newData)
}

	return (
        <>
            <div className="px-4 sm:px-6 lg:px-8">
            <div className='flex flex-wrap gap-1 my-4 text-sm'>
                <Link to="/ssl/create-csr" className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                    <span>Create</span>
                </Link>
                
                <a href="javascript:;" onClick={()=>setIsOpenModal(true)} className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                    <span>Sign</span>
                </a>

                <a href="javascript:;" onClick={()=>setIsOpenModalImpCSR(true)} className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                    <span>Import</span>
                </a>

                <a href="javascript:;" onClick={()=>setIsOpenModalDelete2(true)} className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                    <span>Delete</span>
                </a>

                <Link to="/ssl/csr-template" className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                    <span>CSR Template</span>
                </Link>                
                
                <Link to="/ssl/certificate-request" className="hover:shadow-lg transition text-xs text-red-600 duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-3 py-3 gap-2 items-center border border-theme-gray2 ml-auto">
                    <CreditCardIcon className='w-4 h-4'/> <span>Certificate Register Re</span>  
                </Link>
            </div>
            <div className='max-w-[500px] w-full mb-5 block'>
                <div className="flex">
                    <label htmlFor="search-dropdown" className="mb-2 text-sm font-medium text-gray-900 sr-only">Your Email</label>
                    <div className="relative w-full">
                        <input type="search" id="search-dropdown" className="block p-3 w-full z-20 text-sm text-gray-900 bg-gray-50 rounded-full border-s-2 border border-gray-300 focus:ring-blue-500 focus:border-blue-500" placeholder="Search..."/>
                        <button type="button" className="absolute top-0 end-0 p-3 text-sm font-medium h-full focus:ring-4 focus:outline-none focus:ring-blue-300">
                            <MagnifyingGlassIcon className='w-4 h-4'/>
                            <span className="sr-only">Search</span>
                        </button>
                    </div>
                </div>
            </div>
            
            <DataTable 
                columns={columns} 
                data={data} 
                fixedHeader
                pagination 
                selectableRows 
            />
            </div>

            <Modal title={'Certificate Signing'} isOpenModal={isOpenModal} setIsOpenModal={setIsOpenModal}>
                <label htmlFor="ip-address-range" className="block text-sm font-medium leading-6 text-gray-900 mb-7 sr-only">
                        Certificate Signing
                </label>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-2">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Sign Type
                    </label>
                    <div>
                        <div className='flex justify-between rounded-md shadow-sm ring-1 focus:ring-0 ring-inset ring-gray-300 w-full'>
                            <select
                            className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            >
                                <option>Windows</option>
                                <option>Linux/Mac OS</option>
                            </select>
                        </div>
                        <p className='text-right'>
                            <small>
                                <a href="javascript:;" className='text-theme-red1'>Manage CSR Templates</a>
                            </small>
                        </p>
                    </div>
                </div>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-2">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Server Name
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                </div>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-2">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Certificate Authority
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                </div>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-2">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Template Name / OID
                    </label>
                    <div>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                            <input
                                type="text"
                                autoComplete=""
                                className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                placeholder=""
                            />
                        </div>
                        <p className='text-right'>
                            <small>
                                <a href="javascript:;">Get Templates</a>
                            </small>
                        </p>
                    </div>
                    
                </div>
                
                <hr />
                <div className='w-full flex gap-5 py-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Sign</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <InformationCircleIcon className="w-4 h-4" /><small>If you choose the "Elevate to root user" option, ensure the resource has both "root" and non-root user credentials.</small>
                </div>  
                
            </Modal>
            <Modal title={'Import CSR'} isOpenModal={isOpenModalImpCSR} setIsOpenModal={setIsOpenModalImpCSR}>
                <label htmlFor="ip-address-range" className="block text-sm font-medium leading-6 text-gray-900 mb-7 sr-only">
                        Import CSR
                </label>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-2">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Select a CSR file
                    </label>
                    <div>
                        <div className='flex justify-between rounded-md shadow-sm ring-1 focus:ring-0 ring-inset ring-gray-300 w-full'>
                            <input
                                type="file"
                                autoComplete=""
                                className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                placeholder=""
                            />
                        </div>
                        <p className=''>
                            <small>
                                (File format should be .csr)
                            </small>
                        </p>
                    </div>
                </div>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-2">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Select a key file
                    </label>
                    <div>
                        <div className='flex justify-between rounded-md shadow-sm ring-1 focus:ring-0 ring-inset ring-gray-300 w-full'>
                            <input
                                type="file"
                                autoComplete=""
                                className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                placeholder=""
                            />
                        </div>
                        <p className=''>
                            <small>
                                (File format should be .pfx / .p12 / .pkcs12 / .jks / .keystore)
                            </small>
                        </p>
                    </div>
                </div>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-2">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Private Key Password
                    </label>
                    
                    <div className='flex justify-between rounded-md shadow-sm ring-1 focus:ring-0 ring-inset ring-gray-300 w-full'>
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                </div>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-2">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Expiry Notification Email
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                </div>
                
                
                <hr />
                <div className='w-full flex gap-5 py-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Sign</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
                
            </Modal>
            <Modal title={'Confirmation'} isOpenModal={isOpenModalDelete2} setIsOpenModal={setIsOpenModalDelete2}>
                <div className="grid grid-cols-[100%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                        Delete selected CSRs?
                    </label>
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Ok</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>
        </>
		
	);
};

