import React, { useMemo, useState } from 'react';
import DataTable from 'react-data-table-component';
import HelpBox from '../HelpBox';
import Modal from '../Modal';

import { 
    ArrowLeftIcon,
    MagnifyingGlassIcon,
    InformationCircleIcon,
    ClockIcon,
    } from '@heroicons/react/24/outline';
import Dropdown from '../Dropdown';
import { Menu } from '@headlessui/react';
import MultiSelectListBox from '../MultiSelectListBox';
import { Link } from 'react-router-dom';
import DropdownBorderless from '../DropdownBorderless';

export default function MSCACertificatesReport({setMasterTab}){
    const columns = [
        {
            name: 'Common Name',
            selector: row => row.CommonName,
            sortable: true,  
        },
        {
            name: 'Product Name',
            selector: row => row.ProductName,
            sortable: true,            
        },
        {
            name: 'Order Number',
            selector: row => row.OrderNumber,
            sortable: true,
        },
        {
            name: 'Valid From',
            selector: row => row.ValidFrom,
            sortable: true,
        },
        {
            name: 'Valid To',
            selector: row => row.ValidTo,
            sortable: true,
        },
        {
            name: 'Requested By',
            selector: row => row.RequestedBy,
            sortable: true,
        },
        {
            name: 'Purchase Date',
            selector: row => row.PurchaseDate,
            sortable: true,
            
        },
        {
            name: 'Status',
            selector: row => row.Status,
            sortable: true,
            
        },
        
    ];    

const data = [
    {
        CommonName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        ProductName :   "admin",
        OrderNumber : "1234567890",
        ValidFrom : "Jan 28, 2019 18:16",
        ValidTo :"Jan 28, 2019 18:16",
        RequestedBy : "JKS", 
        PurchaseDate : "May 27, 2020 11:49", 
        Status: <span className="inline-flex items-center rounded-full bg-red-50 px-2 py-1 text-xs font-medium text-red-700 ring-1 ring-inset ring-red-600/10">Expired</span>,    
    }, 
    {
        CommonName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        CertificateID :   "admin",
        CertificateType : "Jan 28, 2019 18:16",
        ValidFrom : "4096",
        ValidTo :"RSA",
        RequestedBy : "JKS", 
        PurchaseDate : "May 27, 2020 11:49", 
        Status:  <span className="inline-flex items-center rounded-full bg-yellow-50 px-2 py-1 text-xs font-medium text-yellow-800 ring-1 ring-inset ring-yellow-600/20">Pending</span>,    
    }, 
    {
        CommonName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        CertificateID :   "admin",
        CertificateType : "Jan 28, 2019 18:16",
        ValidFrom : "4096",
        ValidTo :"RSA",
        RequestedBy : "JKS", 
        PurchaseDate : "May 27, 2020 11:49", 
        Status: <span className="inline-flex items-center rounded-full bg-green-50 px-2 py-1 text-xs font-medium text-green-700 ring-1 ring-inset ring-green-600/20">Success</span>,    
    }, 
    {
        CommonName : <a href="javascript:;" className='text-theme-blue1'>demo.c7CILdRH4KE2.com</a>,
        CertificateID :   "admin",
        CertificateType : "Jan 28, 2019 18:16",
        ValidFrom : "4096",
        ValidTo :"RSA",
        RequestedBy : "JKS", 
        PurchaseDate : "May 27, 2020 11:49", 
        Status: <span className="inline-flex items-center rounded-full bg-gray-50 px-2 py-1 text-xs font-medium text-gray-600 ring-1 ring-inset ring-gray-500/10">Other</span>,    
    }, 
  
];

const [records,setRecords] = useState(data);
const [isOpenModal,setIsOpenModal] = useState(false);
const [isOpenModalDelete2,setIsOpenModalDelete2] = useState(false);

function handleFilter(event){
    const newData = records.filter(row=>{
        return row.ResourceName.toLowerCase().includes(event.target.value.toLowerCase())
    })
    setRecords(newData)
}

const helpTxt = [
    "The MDM integration needs a valid API key with the necessary permissions. To generate the API server key, follow the below steps in the MDM portal: <br/> Go to Admin and click Generate Key under API Key Generation. <br/> In the pop-up, choose Third-party App as the Application, enter a Service Name, and under Permissions, select the following: CertMgmt (read and write), Devices (read and write), Profiles (read and write). <br/> Click Generate Key. Once the new API key is generated, use the copy icon to copy it.",
    "To complete the integration, specify the URL of the MDM server and paste the newly generated API Key.",
    "Click Import From MDM to start importing SSL certificates from the MDM server.",
    "Click Discovery Audit to view the status of the import.",
    "Note: integration works with MDM release version 10.1.2011.02 and above only.",
];


	return (
        <>
            <div className="px-4 sm:px-6 lg:px-8">
                <div className="flex">
                    <h1 className="text-xl theme-gray8 my-b flex">MSCA Certificates Report</h1>
                    <small className='ml-auto flex gap-2 items-center' style={{width:'max-content'}}>
                        <Link className=''>
                            <ClockIcon className="w-4 h-4" />
                        </Link>
                        
                    <div className='z-[10] ml-auto'>
                        <Dropdown DropText={"All Groups"}>
                            <div className="px-1 py-1 " role="none">
                                <Menu.Item>
                                    {({ active }) => (
                                    <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-1 text-sm`}>
                                        PDF
                                    </button>
                                    )}
                                </Menu.Item>
                                <Menu.Item>
                                    {({ active }) => (
                                    <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-1 text-sm`}>
                                        Email
                                    </button>
                                    )} 
                                </Menu.Item>
                            </div>
                        </Dropdown>
                    </div>
                    <div className='z-[10] ml-auto'>
                        <Dropdown DropText={"Export"}>
                            <div className="px-1 py-1 " role="none">
                                <Menu.Item>
                                    {({ active }) => (
                                    <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-1 text-sm`}>
                                        PDF
                                    </button>
                                    )}
                                </Menu.Item>
                                <Menu.Item>
                                    {({ active }) => (
                                    <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-1 text-sm`}>
                                        CSV
                                    </button>
                                    )} 
                                </Menu.Item>
                                <Menu.Item>
                                    {({ active }) => (
                                    <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-1 text-sm`}>
                                        EMAIL
                                    </button>
                                    )} 
                                </Menu.Item>
                            </div>
                        </Dropdown>
                    </div>
                    <Link to="/reports" className="hover:shadow-lg transition duration-300 ease-in-out bg-theme-gray2 rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2 flex text-xs font-light">
                        <ArrowLeftIcon className='w-4 h-4' />
                        <span> Back</span>
                    </Link>
                    </small>
                </div>
           
            <div className='max-w-[500px] w-full mb-5 block'>
                <div className="flex">
                    <label htmlFor="search-dropdown" className="mb-2 text-sm font-medium text-gray-900 sr-only">Your Email</label>
                    <div className="relative w-full">
                        <input type="search" id="search-dropdown" className="block p-3 w-full z-20 text-sm text-gray-900 bg-gray-50 rounded-full border-s-2 border border-gray-300 focus:ring-blue-500 focus:border-blue-500" placeholder="Search..."/>
                        <button type="button" className="absolute top-0 end-0 p-3 text-sm font-medium h-full focus:ring-4 focus:outline-none focus:ring-blue-300">
                            <MagnifyingGlassIcon className='w-4 h-4'/>
                            <span className="sr-only">Search</span>
                        </button>
                    </div>
                </div>
            </div>
            
            <DataTable 
                columns={columns} 
                data={data} 
                fixedHeader
                pagination 
                selectableRows 
            />

            </div>

            {/* <HelpBox helpTxt={helpTxt}/> */}

            <Modal title={'CSR Template'} isOpenModal={isOpenModal} setIsOpenModal={setIsOpenModal}>
                <label htmlFor="ip-address-range" className="block text-sm font-medium leading-6 text-gray-900 mb-7 sr-only">
                    CSR Template
                </label>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-2">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Sign Type
                    </label>
                    <div>
                        <div className='flex justify-between rounded-md shadow-sm ring-1 focus:ring-0 ring-inset ring-gray-300 w-full'>
                            <select
                            className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            >
                                <option>Windows</option>
                                <option>Linux/Mac OS</option>
                            </select>
                        </div>
                        <p className='text-right'>
                            <small>
                                <a href="javascript:;" className='text-theme-red1'>Manage CSR Templates</a>
                            </small>
                        </p>
                    </div>
                </div>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-2">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Server Name
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                </div>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-2">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Certificate Authority
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                </div>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-2">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Template Name / OID
                    </label>
                    <div>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                            <input
                                type="text"
                                autoComplete=""
                                className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                placeholder=""
                            />
                        </div>
                        <p className='text-right'>
                            <small>
                                <a href="javascript:;">Get Templates</a>
                            </small>
                        </p>
                    </div>
                    
                </div>
                
                <hr />
                <div className='w-full flex gap-5 py-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Sign</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <InformationCircleIcon className="w-4 h-4" /><small>If you choose the "Elevate to root user" option, ensure the resource has both "root" and non-root user credentials.</small>
                </div>  
                
            </Modal>
            <Modal title={'Confirmation'} isOpenModal={isOpenModalDelete2} setIsOpenModal={setIsOpenModalDelete2}>
                <div className="grid grid-cols-[100%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                        Delete selected Template(s)?
                    </label>
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Ok</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>
        </>
		
	);
};

