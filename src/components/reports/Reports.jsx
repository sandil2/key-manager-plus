import React, { useMemo, useState } from 'react';

import Icons1 from "../../images/reports-icons/ssh.png"
import Icons2 from "../../images/reports-icons/database-report.png"
import Icons3 from "../../images/reports-icons/report.png"
import Icons4 from "../../images/reports-icons/key.png"
import Icons5 from "../../images/reports-icons/data.png"
import Icons6 from "../../images/reports-icons/report-2.png"
import Icons7 from "../../images/reports-icons/keyword.png"

import Icons8 from "../../images/reports-icons/ssl-certificate.png"
import Icons9 from "../../images/reports-icons/report-3.png"
import Icons10 from "../../images/reports-icons/time.png"
import Icons11 from "../../images/reports-icons/ssl.png"
import Icons12 from "../../images/reports-icons/server.png"
import Icons13 from "../../images/reports-icons/sync.png"
import Icons14 from "../../images/reports-icons/experience.png"
import Icons15 from "../../images/reports-icons/certificate.png"
import Icons16 from "../../images/reports-icons/policy.png"
import Icons17 from "../../images/reports-icons/cloud.png"
import Icons18 from "../../images/reports-icons/data-2.png"
import Icons19 from "../../images/reports-icons/renewable.png"
import Icons20 from "../../images/reports-icons/azure.png"

import { Link } from 'react-router-dom';
import Modal from '../Modal';

import { 
    ArrowLeftIcon,
    MagnifyingGlassIcon,
    InformationCircleIcon,
    ClockIcon,
    } from '@heroicons/react/24/outline';
export default function Reports({setMasterTab}){

const [records,setRecords] = useState();
const [isOpenModal,setIsOpenModal] = useState(false);

	return (
        <>
            <div className="px-4 sm:px-6 lg:px-8 flex items-center gap-5">
                <h1 className="text-xl">SSH Reports</h1>
            </div>
            <div className="px-4 sm:px-6 lg:px-8 items-center">
                <div className='grid grid-cols-5 gap-5 py-5'>
                    <Link to="/reports/ssh-resource-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons1} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>SSH Resource Report</h3>
                    </Link>
                    <Link to="/reports/landing-servers-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons2} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>Landing Servers Report</h3>
                    </Link>
                    <Link to="/reports/private-key-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons3} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>Private Key Report</h3>
                    </Link>
                    <Link to="/reports/key-rotation-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons4} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>Key Rotation Report</h3>
                    </Link>
                    <Link to="/reports/key-deployment-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons7} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>Key Deployment Report</h3>
                    </Link>
                    <Link to="/reports/server-access-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons5} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>Server Access Report</h3>
                    </Link>
                    <Link to="javascript:;" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons6} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>SSH Users Report</h3>
                    </Link>
                </div>      
            </div>
            <hr className='mb-5 mt-4'/>
            <div className="px-4 sm:px-6 lg:px-8 flex items-center gap-5">
                <h1 className="text-xl">SSL Reports</h1>
            </div>
            <div className="px-4 sm:px-6 lg:px-8 items-center">
                <div className='grid grid-cols-5 gap-5 py-5'>
                    <Link to="/reports/ssl-certificates-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons8} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>SSL Certificates Report</h3>
                    </Link>
                    <Link to="/reports/ssl-request-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons9} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>SSL Request Report</h3>
                    </Link>
                    <Link to="javascript:;" onClick={()=>setIsOpenModal(true)} className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons10} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>SSL Expiry Report</h3>
                    </Link>
                    <Link to="/reports/wildcard-ssl-certificates-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons11} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>Wildcard SSL Certificates Report</h3>
                    </Link>
                    <Link to="/reports/deployed-servers" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons12} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>Deployed Servers</h3>
                    </Link>
                    <Link to="/reports/certificates-sync-status-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons13} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>Certificates Sync Status Report</h3>
                    </Link>
                    <Link to="/reports/ad-user-certificates-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons14} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>AD User Certificates Report</h3>
                    </Link>
                    <Link to="/reports/certificate-sign-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons15} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>Certificate Sign Report</h3>
                    </Link>
                    <Link to="/reports/sha1-certificate-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons16} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>SHA1 Certificate Report</h3>
                    </Link>
                    <Link to="/reports/deployment-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons17} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>Deployment Report</h3>
                    </Link>
                    <Link to="/reports/ssl-vulnerability-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons18} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>SSL Vulnerability Report</h3>
                    </Link>
                    <Link to="/reports/certificate-renewal-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons19} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>Certificate Renewal Report</h3>
                    </Link>
                    <Link to="/reports/aws-certificate-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons20} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>AWS Certificate Report</h3>
                    </Link>
                    <Link to="/reports/aws-certificate-request-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons8} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>AWS Certificate Request Report</h3>
                    </Link>
                    <Link to="/reports/azure-certificates-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons9} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>Azure Certificates Report</h3>
                    </Link>
                    <Link to="/reports/azure-certificate-requests-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons10} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>Azure Certificate Requests Report</h3>
                    </Link>
                    <Link to="/reports/kubernetes-tls-secrets-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons11} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>Kubernetes TLS Secrets Report</h3>
                    </Link>
                    <Link to="/reports/loadbalancer-deployment-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons12} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>LoadBalancer Deployment Report</h3>
                    </Link>
                    <Link to="/reports/azure-tls-secrets-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons13} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>Azure TLS Secrets Report</h3>
                    </Link>
                    <Link to="/reports/mdm-certificates-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons14} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>MDM Certificates Report</h3>
                    </Link>
                    <Link to="/reports/msca-revoke-and-delete-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons15} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>MSCA Revoke and Delete Report</h3>
                    </Link>
                    <Link to="/reports/msca-certificates-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons16} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>MSCA Certificates Report</h3>
                    </Link>
                    <Link to="/reports/jenkins-access-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons17} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>Jenkins Access Report</h3>
                    </Link>
                    <Link to="/reports/private-ca-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons18} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>Private CA Report</h3>
                    </Link>
                </div>      
            </div>
            <hr className='mb-5 mt-4'/>
            <div className="px-4 sm:px-6 lg:px-8 flex items-center gap-5">
                <h1 className="text-xl">Public CA Reports</h1>
            </div>
            <div className="px-4 sm:px-6 lg:px-8 items-center">
                <div className='grid grid-cols-5 gap-5 py-5'>
                    <Link to="/reports/lets-encrypt-requests-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons1} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>Let's Encrypt Requests Report</h3>
                    </Link>
                    <Link to="/reports/lets-encrypt-certificates-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons2} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>Let's Encrypt Certificates Report</h3>
                    </Link>
                    <Link to="/reports/buypass-go-ssl-requests-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons3} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>Buypass Go SSL Requests Report</h3>
                    </Link>
                    <Link to="/reports/buypass-go-ssl-certificates-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons4} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>Buypass Go SSL Certificates Report</h3>
                    </Link>
                    <Link to="/reports/zerossl-requests-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons5} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>ZeroSSL Requests Report</h3>
                    </Link>
                    <Link to="/reports/zerossl-certificates-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons6} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>ZeroSSL Certificates Report</h3>
                    </Link>
                    <Link to="/reports/acme-requests-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons7} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>ACME Requests Report</h3>
                    </Link>
                    <Link to="/reports/acme-certificates-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons8} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>ACME Certificates Report</h3>
                    </Link>
                    <Link to="/reports/godaddy-orders-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons9} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>GoDaddy Orders Report</h3>
                    </Link>
                    <Link to="/reports/the-ssl-store-orders-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons9} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>The SSL Store Orders Report</h3>
                    </Link>
                    <Link to="/reports/digiCert-orders-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons9} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>DigiCert Orders Report</h3>
                    </Link>
                    <Link to="/reports/globalsign-orders-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons9} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>GlobalSign Orders Report</h3>
                    </Link>
                    <Link to="/reports/sectigo-orders-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons9} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>Sectigo Orders Report</h3>
                    </Link>
                    <Link to="/reports/entrust-orders-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons9} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>Entrust Orders Report</h3>
                    </Link>
                </div>
            </div>    
            <hr className='mb-5 mt-4'/>
            <div className="px-4 sm:px-6 lg:px-8 flex items-center gap-5">
                <h1 className="text-xl">Common Reports</h1>
            </div>   
            <div className="px-4 sm:px-6 lg:px-8 items-center">
                <div className='grid grid-cols-5 gap-5 py-5'>
                    <Link to="/reports/all-keys-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons1} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>All Keys Report</h3>
                    </Link>
                    <Link to="/reports/audit-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons2} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>Audit Report</h3>
                    </Link>
                    <Link to="/reports/key-vault-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons3} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>Key Vault Report</h3>
                    </Link>
                    <Link to="/reports/pgp-keys-report" className='shadow-inner transition hover:bg-theme-gray3 hover:text-white flex justify-brtween items-center gap-2 p-3 rounded ring-1 ring-theme-gray3 align-center'>
                        <div className='bg-white p-2 rounded-full shadow-xl'>
                            <img src={Icons4} className='size-7' alt="" />    
                        </div>
                        <h3 className='font-light text-xs'>PGP Keys Report</h3>
                    </Link>
                </div>
            </div>         
             <Modal title={'Expiry Date Filter'} isOpenModal={isOpenModal} setIsOpenModal={setIsOpenModal}>
                <label htmlFor="ip-address-range" className="block text-sm font-medium leading-6 text-gray-900 mb-7 sr-only">
                    Expiry Date Filter
                </label>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-2">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Sign Type
                    </label>
                    <div>
                        <div className='flex justify-between rounded-md shadow-sm ring-1 focus:ring-0 ring-inset ring-gray-300 w-full'>
                            <select
                            className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            >
                                <option>Windows</option>
                                <option>Linux/Mac OS</option>
                            </select>
                        </div>
                        <p className='text-right'>
                            <small>
                                <a href="javascript:;" className='text-theme-red1'>Manage CSR Templates</a>
                            </small>
                        </p>
                    </div>
                </div>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-2">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Server Name
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                </div>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-2">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Certificate Authority
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                </div>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-2">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Template Name / OID
                    </label>
                    <div>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                            <input
                                type="text"
                                autoComplete=""
                                className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                placeholder=""
                            />
                        </div>
                        <p className='text-right'>
                            <small>
                                <a href="javascript:;">Get Templates</a>
                            </small>
                        </p>
                    </div>
                    
                </div>
                
                <hr />
                <div className='w-full flex gap-5 py-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Sign</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <InformationCircleIcon className="w-4 h-4" /><small>If you choose the "Elevate to root user" option, ensure the resource has both "root" and non-root user credentials.</small>
                </div>  
                
            </Modal>
        </>
		
	);
};

