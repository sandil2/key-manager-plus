import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';

export default function BarChart({options}){
  
    return(
        <HighchartsReact highcharts={Highcharts} options={options} />
    )
}