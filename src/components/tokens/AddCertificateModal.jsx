import { Fragment, useRef, useState } from 'react'
import { Dialog, Transition } from '@headlessui/react'
import { CheckIcon } from '@heroicons/react/24/outline'

export default function AddCertificateModal({modalTrigger2, setModalTrigger2}) {
//   const [open, setOpen] = useState(modalTrigger)
    console.log(modalTrigger2);
  const cancelButtonRef = useRef(null);
  const [certiCatag,setCertiCatag] = useState("certi");

  return (
    <Transition.Root show={modalTrigger2} as={Fragment}>
      <Dialog as="div" className="relative z-30" initialFocus={cancelButtonRef} onClose={setModalTrigger2}>
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
        </Transition.Child>

        <div className="fixed inset-0 z-30 w-screen overflow-y-auto">
          <div className="flex min-h-full items-end justify-center p-4 text-center sm:items-center sm:p-0">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <Dialog.Panel className="relative transform overflow-hidden rounded-lg bg-white px-4 pb-4 pt-5 text-left shadow-xl transition-all sm:my-8 sm:w-full sm:max-w-2xl sm:p-6">
                <div>
                  {/* <div className="mx-auto flex h-12 w-12 items-center justify-center rounded-full bg-green-100">
                    <CheckIcon className="h-6 w-6 text-green-600" aria-hidden="true" />
                  </div> */}
                  <div className="mt-3 text-center sm:mt-5">
                    <Dialog.Title as="h3" className="text-base font-semibold leading-6 text-gray-900">
                      Add Certificate
                    </Dialog.Title>
                    <div className="mt-2 text-left">
                       <label className='pb-3 block'><small>Select Option</small></label> 
                      <div className="w-full mb-7">
                        
                        <div className="flex w-full">
                            <div className="space-y-4 sm:flex sm:items-center sm:space-x-10 sm:space-y-0 w-full">
                                <div className="flex items-center">
                                    <input
                                        type="radio"
                                        name="add-certi"
                                        className="accent-theme-red1 h-4 w-4 border-gray-300"
                                        id="certi"
                                        defaultChecked={certiCatag === 'certi'}
                                        value="certi"
                                        onClick={() => setCertiCatag('certi')}
                                    />
                                    <label htmlFor="certi" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                        Certificates
                                    </label>
                                </div>
                                <div className="flex items-center">
                                    <input
                                        name="add-certi"
                                        type="radio"
                                        id="certi-content"
                                        className="accent-theme-red1 h-4 w-4 border-gray-300"
                                        defaultChecked={certiCatag === 'certi-content'}
                                        value="certi-content"
                                        onClick={() => setCertiCatag('certi-content')}
                                    />
                                    <label htmlFor="certi-content" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                        Certificate Content
                                    </label>
                                </div>
                                <div className="flex items-center">
                                    <input
                                        id="keystore"
                                        name="add-certi"
                                        type="radio"
                                        className="accent-theme-red1 h-4 w-4 border-gray-300"
                                        defaultChecked={certiCatag === 'keystore'}
                                        value="keystore"
                                        onClick={() => setCertiCatag('keystore')}
                                    />
                                    <label htmlFor="keystore" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                        Keystore
                                    </label>
                                </div>
                                <div className="flex items-center">
                                    <input
                                        name="add-certi"
                                        type="radio"
                                        className="accent-theme-red1 h-4 w-4 border-gray-300"
                                        id="certi-detail"
                                        defaultChecked={certiCatag === 'certi-detail'}
                                        value="certi-detail"
                                        onClick={() => setCertiCatag('certi-detail')}
                                    />
                                    <label htmlFor="certi-detail" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                        Certificate Details
                                    </label>
                                </div>
                                
                            </div>
                        </div>

                         {certiCatag == "certi" && 
                          <>
                          <div className="grid grid-cols-[23%_70%] w-full items-center my-5 gap-7">
                              <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                  File Location
                              </label>
                              <div className="">
                                <input
                                    type="file"
                                    name="ip-address-range"
                                    id="ip-address-range"
                                    autoComplete=""
                                    className="block w-full border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6 rounded-md shadow-sm ring-1 ring-inset ring-gray-300"
                                    placeholder=""
                                />
                                <small className='py-3'>(File format should be .cer / .crt / .pem / .der / .p7b)</small>
                              </div>
                              
                          </div>
                            
                          </>
                        }
                         {certiCatag == "certi-content" && 
                          <>
                          <div className="grid grid-cols-[23%_70%] w-full items-center my-5 gap-7">
                              <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                  Paste The Content
                              </label>
                              <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                

                                <textarea
                                    type="file"
                                    name="ip-address-range"
                                    id="ip-address-range"
                                    autoComplete=""
                                    className="block w-full flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                    placeholder="Place"
                                    rows={3}
                                ></textarea>
                                
                              </div>
                          </div>
                            
                          </>
                        }
                         {certiCatag == "keystore" && 
                          <>
                          <div className="grid grid-cols-[33%_70%] w-full gap-y-7 items-center my-5">
                              <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                  File Location
                              </label>
                              <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                <div>

                                <input
                                    type="file"
                                    name="ip-address-range"
                                    id="ip-address-range"
                                    autoComplete=""
                                    className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                    placeholder=""
                                />
                                
                                </div>
                              </div>
                              <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                  File Location
                              </label>
                              <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                <div>

                                <input
                                    type="file"
                                    name="ip-address-range"
                                    id="ip-address-range"
                                    autoComplete=""
                                    className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                    placeholder=""
                                />
                                
                                </div>
                              </div>
                          </div>
                            
                          </>
                        }
                         {certiCatag == "certi-detail" && 
                          <>
                          <div className="grid grid-cols-[33%_70%] w-full gap-y-7 items-center my-5">
                              <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                  File Location
                              </label>
                              <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                <div>

                                <input
                                    type="file"
                                    name="ip-address-range"
                                    id="ip-address-range"
                                    autoComplete=""
                                    className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                    placeholder=""
                                />
                                
                                </div>
                              </div>
                          </div>
                            
                          </>
                        }

                    </div>
                    </div>
                  </div>
                </div>
                <div className="mt-5 sm:mt-6 sm:grid sm:grid-flow-row-dense sm:grid-cols-2 sm:gap-3">
                  <button
                    type="button"
                    className="inline-flex w-full justify-center rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600 sm:col-start-2"
                    onClick={() => setModalTrigger2(false)}
                  >
                    Add / Register
                  </button>
                  <button
                    type="button"
                    className="mt-3 inline-flex w-full justify-center rounded-md bg-white px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50 sm:col-start-1 sm:mt-0"
                    onClick={() => setModalTrigger2(false)}
                    ref={cancelButtonRef}
                  >
                    Cancel
                  </button>
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  )
}
