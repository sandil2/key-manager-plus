import { useLayoutEffect, useRef, useState } from 'react'
import { TrashIcon,PlusIcon,ArrowPathRoundedSquareIcon,ArrowUpOnSquareIcon } from '@heroicons/react/24/outline'

const people = [
  {
    TokenSerial: '1231231',
    IPAddress: '12.12.1.21.2',
    Status: 'active',
    Make: 'Make demo',
    Model: 'Model demo',
    Version: 'Version demo',
    CertificateDetails: 'CD demo',
    Expiry: 'Expiry demo',
    Algorithm: 'Algon',
    DPCertificate: 'DPC',
  },
  {
    TokenSerial: '1231231',
    IPAddress: '12.12.1.21.2',
    Status: 'not active',
    Make: 'Make demo',
    Model: 'Model demo',
    Version: 'Version demo',
    CertificateDetails: 'CD demo',
    Expiry: 'Expiry demo',
    Algorithm: 'Algon',
    DPCertificate: 'DPC',
  },
  {
    TokenSerial: '1231231',
    IPAddress: '12.12.1.21.2',
    Status: 'active',
    Make: 'Make demo',
    Model: 'Model demo',
    Version: 'Version demo',
    CertificateDetails: 'CD demo',
    Expiry: 'Expiry demo',
    Algorithm: 'Algon',
    DPCertificate: 'DPC',
  },
  {
    TokenSerial: '1231231',
    IPAddress: '12.12.1.21.2',
    Status: 'active',
    Make: 'Make demo',
    Model: 'Model demo',
    Version: 'Version demo',
    CertificateDetails: 'CD demo',
    Expiry: 'Expiry demo',
    Algorithm: 'Algon',
    DPCertificate: 'DPC',
  },
  {
    TokenSerial: '1231231',
    IPAddress: '12.12.1.21.2',
    Status: 'active',
    Make: 'Make demo',
    Model: 'Model demo',
    Version: 'Version demo',
    CertificateDetails: 'CD demo',
    Expiry: 'Expiry demo',
    Algorithm: 'Algon',
    DPCertificate: 'DPC',
  },
  {
    TokenSerial: '1231231',
    IPAddress: '12.12.1.21.2',
    Status: 'active',
    Make: 'Make demo',
    Model: 'Model demo',
    Version: 'Version demo',
    CertificateDetails: 'CD demo',
    Expiry: 'Expiry demo',
    Algorithm: 'Algon',
    DPCertificate: 'DPC',
  },
  // More people...
]

function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}

export default function SelectedTokensTable({modalTrigger2, setModalTrigger2}) {
  const checkbox = useRef()
  const [checked, setChecked] = useState(false)
  const [indeterminate, setIndeterminate] = useState(false)
  const [selectedPeople, setSelectedPeople] = useState([])

  useLayoutEffect(() => {
    const isIndeterminate = selectedPeople.length > 0 && selectedPeople.length < people.length
    setChecked(selectedPeople.length === people.length)
    setIndeterminate(isIndeterminate)
    checkbox.current.indeterminate = isIndeterminate
  }, [selectedPeople])

  function toggleAll() {
    setSelectedPeople(checked || indeterminate ? [] : people)
    setChecked(!checked && !indeterminate)
    setIndeterminate(false)
  }

  return (
    <>
      <div className="sm:flex sm:items-center">
        <div className="sm:flex-auto">
          <h1 className="text-base font-medium leading-6 text-gray-900">List of Certificates</h1>
          {/* <p className="mt-2 text-sm text-gray-700">
            Need text for the this and above title
          </p> */}
        </div>
        <div className="mt-4 sm:ml-16 sm:mt-0 sm:flex-none">
          {/* <button
            type="button"
            className="bg-theme-red1 text-white hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2"
          >
            Add user
          </button> */}

          {/* <button className='bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2'>

          </button> */}
        </div>
      </div>
      <div className="mt-8 flow-root">
        <div className="-mx-4 -my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div className="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
            <div className="relative">
              {selectedPeople.length > 0 && (
                <div className="absolute left-14 top-0 flex h-12 items-center space-x-3 bg-white sm:left-12">
                  <button
                    type="button"
                    className="inline-flex items-center rounded bg-white px-2 py-1 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50 disabled:cursor-not-allowed disabled:opacity-30 disabled:hover:bg-white"
                  >
                    Bulk edit
                  </button>
                  <button
                    type="button"
                    className="inline-flex items-center rounded bg-white px-2 py-1 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50 disabled:cursor-not-allowed disabled:opacity-30 disabled:hover:bg-white"
                  >
                    Delete all
                  </button>
                </div>
              )}
              <table className="min-w-full table-fixed divide-y divide-gray-300">
                <thead>
                  <tr>
                    <th scope="col" className="relative px-7 sm:w-12 sm:px-6">
                      <input
                        type="checkbox"
                        className="absolute left-4 top-1/2 -mt-2 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                        ref={checkbox}
                        checked={checked}
                        onChange={toggleAll}
                      />
                    </th>
                    <th scope="col" className="px-3 py-3.5 text-left text-sm font-medium text-gray-900">
                      Certificate Detail
                    </th>
                    <th scope="col" className="px-3 py-3.5 text-left text-sm font-medium text-gray-900">
                      Expire
                    </th>
                    <th scope="col" className="px-3 py-3.5 text-left text-sm font-medium text-gray-900">
                      Algorithm
                    </th>
                    <th scope="col" className="min-w-[110px] px-3 py-3.5 text-left text-sm font-medium text-gray-900">
                        Download <br/>
                      <small> Public Certificate</small>
                    </th>
                    <th scope="col" className="relative py-3.5 pl-3 pr-4 font-medium sm:pr-3">
                      <span className="sr-only">Action</span>
                        Action
                    </th>
                  </tr>
                </thead>
                <tbody className="divide-y divide-gray-200 bg-white">
                  {people.map((person) => (
                    <tr key={person.TokenSerial} className={selectedPeople.includes(person) ? 'bg-gray-50' : undefined}>
                      <td className="relative px-7 sm:w-12 sm:px-6">
                        {selectedPeople.includes(person) && (
                          <div className="absolute inset-y-0 left-0 w-0.5 bg-indigo-600" />
                        )}
                        <input
                          type="checkbox"
                          className="absolute left-4 top-1/2 -mt-2 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                          value={person.TokenSerial}
                          checked={selectedPeople.includes(person)}
                          onChange={(e) =>
                            setSelectedPeople(
                              e.target.checked
                                ? [...selectedPeople, person]
                                : selectedPeople.filter((p) => p !== person)
                            )
                          }
                        />
                      </td>
                        <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{person.CertificateDetails}</td>
                        <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{person.Expiry}</td>
                        <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{person.Algorithm}</td>
                        <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{person.DPCertificate}</td>
                        <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">
                        <div className='flex w-full justify-center gap-2'>
                            <a href="javascript:;" className="transition duration-300 ease-in-out text-indigo-600 hover:text-indigo-900 w-8 rounded-full text-red-700 hover:bg-red-700 hover:text-white p-2" title="Delete">
                                <ArrowPathRoundedSquareIcon /><span className="sr-only">,Rotate {person.name}</span>
                            </a>
                            {/* <a href="javascript:;" onClick={()=>setModalTrigger2(true)} className="transition duration-300 ease-in-out text-indigo-600 hover:text-indigo-900 w-8 rounded-full text-green-700 hover:bg-green-700 hover:text-white p-2" title="Add">
                                <PlusIcon /><span className="sr-only">,Add {person.name}</span>
                            </a> */}
                            <a href="javascript:;" className="transition duration-300 ease-in-out text-indigo-600 hover:text-indigo-900 w-8 rounded-full text-green-700 hover:bg-green-700 hover:text-white p-2" title="Add">
                                <ArrowUpOnSquareIcon /><span className="sr-only">,Add {person.name}</span>
                            </a>
                        </div>
                            
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
