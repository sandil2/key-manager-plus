

import { ArrowUpIcon } from "@heroicons/react/24/outline"
const people = [
  { name: 'User Name 1', time: 'Mar 23, 2024 08:37', operation: 'ACME Certificate Renewal Schedule'},
  { name: 'User Name 2', time: 'Mar 23, 2024 08:37', operation: 'ACME Certificate Renewal Schedule'},
  { name: 'User Name 3', time: 'Mar 23, 2024 08:37', operation: 'ACME Certificate Renewal Schedule'},
  { name: 'User Name 4', time: 'Mar 23, 2024 08:37', operation: 'ACME Certificate Renewal Schedule'},
  { name: 'User Name 5', time: 'Mar 23, 2024 08:37', operation: 'ACME Certificate Renewal Schedule'},
  { name: 'User Name 6', time: 'Mar 23, 2024 08:37', operation: 'ACME Certificate Renewal Schedule'}
]

export default function OperationAuditTable() {
  return (
      <div className="py-4 overflow-x-auto">
            <table className="divide-y divide-gray-300 w-full border-collapse border border-slate-400">
              <thead className="text-left bg-theme-gray2">
                <tr>
                  <th scope="col" style={{textAlign:'left'}} className="text-theme-gray3 border border-slate-300 px-3 py-4 text-left text-sm font-medium bg-theme-gray5">
                    <p className="flex items-center gap-3"><span>Name</span> <span><ArrowUpIcon className="w-3 h-3"/></span></p>
                  </th>
                  <th scope="col" style={{textAlign:'left'}} className="text-theme-gray3 border border-slate-300 px-3 py-4 text-left text-sm font-medium bg-theme-gray5">
                    <p className="flex items-center gap-3"><span>Time</span> <span><ArrowUpIcon className="w-3 h-3"/></span></p>
                  </th>
                  <th scope="col" style={{textAlign:'left'}} className="text-theme-gray3 border border-slate-300 px-3 py-4 text-left text-sm font-medium bg-theme-gray5">
                    <p className="flex items-center gap-3"><span>Operation</span> <span><ArrowUpIcon className="w-3 h-3"/></span></p>
                  </th>
                </tr>
              </thead>
              <tbody className="divide-y divide-gray-200">
                {people.map((person) => (
                  <tr key={person.name}>
                    <td className="border border-slate-300 whitespace-nowrap py-4 px-3 text-sm font-medium text-theme-gray6">
                      {person.name}
                    </td>
                    <td className="border border-slate-300 whitespace-nowrap px-3 py-4 text-sm text-theme-gray6">{person.time}</td>
                    <td className="border border-slate-300 whitespace-nowrap px-3 py-4 text-sm text-theme-gray6">{person.operation}</td>
                  </tr>
                ))}
              </tbody>
            </table>
      </div>
  )
}
