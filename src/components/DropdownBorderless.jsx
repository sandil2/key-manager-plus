import { Menu, Transition } from '@headlessui/react'
import { Fragment, useEffect, useRef, useState } from 'react'
import { ChevronDownIcon } from '@heroicons/react/20/solid'

export default function DropdownBorderless({DropIcon, DropText, children}) {
  return (
    
      <Menu as="div" className="relative inline-block text-left z-[10]">
        <div>
          <Menu.Button className="inline-flex w-full justify-centers items-center border-none">
            <div className='w-5 h-5 flex items-center pr-1'>{DropIcon && DropIcon}</div><span style={{width:"max-content"}}>{DropText && DropText}</span>
            <ChevronDownIcon
              className="h-5 w-5 text-violet-200 hover:text-violet-100"
              aria-hidden="true"
            />
          </Menu.Button>
        </div>
        <Transition
          as={Fragment}
          enter="transition ease-out duration-100"
          enterFrom="transform opacity-0 scale-95"
          enterTo="transform opacity-100 scale-100"
          leave="transition ease-in duration-75"
          leaveFrom="transform opacity-100 scale-100"
          leaveTo="transform opacity-0 scale-95"
        >
          <Menu.Items className="absolute right-0 mt-2 w-56 origin-top-right divide-y divide-gray-100 rounded-md bg-white shadow-lg ring-1 ring-black/5 focus:outline-none">
            {children}
          </Menu.Items>
        </Transition>
      </Menu>
    
  )
}

