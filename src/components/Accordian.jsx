import { Disclosure } from '@headlessui/react'
import { ChevronUpIcon } from '@heroicons/react/20/solid'

export default function Accordian({title,children}) {
  return (
        <>
        <Disclosure>
        {({ open }) => (
            <>
            <Disclosure.Button className="flex w-full justify-start rounded-lg bg-theme-gray2 px-4 py-2 text-left text-sm font-medium hover:bg-theme-gray3 hover:text-white focus:outline-none">
                
                <ChevronUpIcon
                className={`${
                    open ? 'rotate-180 transform' : ''
                } h-5 w-5 text-theme-gray3`}
                />
                <span>{title}</span>
            </Disclosure.Button>
            <Disclosure.Panel className="px-4 pb-2 pt-4 text-sm text-gray-500">
                {children}
            </Disclosure.Panel>
            </>
        )}
        </Disclosure>
        </>
  )
}
