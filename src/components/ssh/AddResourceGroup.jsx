import React, { useMemo, useState } from 'react';

import { KeyIcon, TrashIcon,ArrowDownOnSquareIcon,EyeIcon,UserGroupIcon, RectangleGroupIcon, MagnifyingGlassIcon, ArrowPathRoundedSquareIcon, PencilSquareIcon
 } from '@heroicons/react/24/outline';

export default function AddResourceGroup({setMasterTab}){
    const [selType,setSelType] = useState("dmz");
	return (
        <>
            <div className="px-4 sm:px-6 lg:px-8">    
                <h1 className='text-xl theme-gray8 mb-10 mt-3'>Add Resource Group</h1>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Resource Group Name
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Description
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <textarea
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                            rows={3}
                        />
                    </div>
                    
                    <label htmlFor="ip-address-range" className="text-xs">
                        Select Resources
                    </label>
                    <div className="flex w-full my-5">
                        <legend className="sr-only">Agents</legend>
                        <div className="flex w-full gap-7">
                            <div className="flex items-center">
                                <input
                                    id="dmz"
                                    name="discover-by"
                                    type="radio"
                                    defaultChecked={selType === 'dmz'}
                                    value="dmz"
                                    onClick={() => setSelType('dmz')}
                                    className="accent-theme-red1 h-4 w-4 border-gray-300"
                                />
                                <label htmlFor="dmz" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                    By Specific Resource
                                </label>
                            </div>
                            <div className="flex items-center">
                                <input
                                    id="cs"
                                    name="discover-by"
                                    type="radio"
                                    defaultChecked={selType === 'cs'}
                                    value="cs"
                                    onClick={() => setSelType('cs')}
                                    className="accent-theme-red1 h-4 w-4 border-gray-300 text-theme-red1 focus:ring-theme-red1"
                                />
                                <label htmlFor="cs" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                    By Criteria
                                </label>
                            </div>
                        </div>
                    </div>

                    <label htmlFor="ip-address-range" className="text-xs">
                        &nbsp;
                    </label>

                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full max-w-sm">
                        <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                            <option value="RSA">RSA</option>
                            <option value="DSA">DSA</option>
                            <option value="ECDSA">ECDSA</option>
                            <option value="ED25519">ED25519</option>
                        </select>
                    </div>

                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Save</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full' onClick={()=>setMasterTab("ssh-server")}>Cancel</button>
                </div>  
            </div>
        </>	
	);
};

