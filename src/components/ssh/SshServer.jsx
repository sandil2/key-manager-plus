import React, { useMemo, useState } from 'react';
import DataTable from 'react-data-table-component';
import HelpBox from '../HelpBox';
import Modal from '../Modal';
import { 
    ClipboardDocumentListIcon,
    ServerStackIcon,
    KeyIcon,
    TrashIcon,
    ArrowDownOnSquareIcon,
    EyeIcon,
    BookOpenIcon,
    MagnifyingGlassIcon,
    PencilSquareIcon,
    CommandLineIcon,
    RectangleStackIcon,
    UserGroupIcon } from '@heroicons/react/24/outline';
import { Link } from 'react-router-dom';

export default function SshServer({setMasterTab}){
    const columns = [
        {
            name: 'Keys',
            selector: row => row.Key,
            sortable: false,
            maxWidth: "30px"
        },
        {
            name: 'Resource Name',
            selector: row => row.ResourceName,
            sortable: true,
        },
        {
            name: 'Description',
            selector: row => row.Description,
            sortable: true,
        },
        {
            name: 'IP Address',
            selector: row => row.IPAddress,
            sortable: true,
        },
        {
            name: 'Created Time',
            selector: row => row.CreatedTime,
            sortable: true,
        },
        {
            name: 'Landing Server',
            selector: row => row.LandingServer,
            sortable: true,
        },
        {
            name: '',
            selector: row => row.LandingServerIcon,
            sortable: false,
            maxWidth: "30px",
            minWidth: "50px"
        },
        {
            name: '',
            selector: row => row.CreateDeployIcon,
            sortable: false,
            maxWidth: "30px",
            minWidth: "50px"
        },
        {
            name: '',
            selector: row => row.SSHUsers,
            sortable: false,
            maxWidth: "30px",
            minWidth: "50px"
        },
        {
            name: '',
            selector: row => row.EditSSHRsr,
            sortable: false,
            maxWidth: "30px",
            minWidth: "50px"
        },
    ];    
    
    const userColumn = [
        {
            name: 'User Name',
            selector: row => row.UserName,
            sortable: true,
        },
        {
            name: '',
            selector: row => row.Key,
            sortable: true,
            maxWidth: "30px",
            minWidth: "50px"
        },
        {
            name: '',
            selector: row => row.OpenConnetion,
            sortable: true,
            maxWidth: "30px",
            minWidth: "50px"
        },
    ];

    const userData = [
        {
            UserName : "test_server101",
            Key : <KeyIcon className="w-4 h-4 text-black-700"/>,
            OpenConnetion : <CommandLineIcon className='w-4 h4 text-black-700'/>,
        },
        {
            UserName : "test_server102",
            Key : <KeyIcon className="w-4 h-4 text-black-700"/>,
            OpenConnetion : <CommandLineIcon className='w-4 h4 text-black-700'/>,
        },
        {
            UserName : "test_server103",
            Key : <KeyIcon className="w-4 h-4 text-black-700"/>,
            OpenConnetion : <CommandLineIcon className='w-4 h4 text-black-700'/>,
        },
        {
            UserName : "test_server103",
            Key : <KeyIcon className="w-4 h-4 text-black-700"/>,
            OpenConnetion : <CommandLineIcon className='w-4 h4 text-black-700'/>,
        },
        {
            UserName : "test_server103",
            Key : <KeyIcon className="w-4 h-4 text-black-700"/>,
            OpenConnetion : <CommandLineIcon className='w-4 h4 text-black-700'/>,
        },
        {
            UserName : "test_server103",
            Key : <KeyIcon className="w-4 h-4 text-black-700"/>,
            OpenConnetion : <CommandLineIcon className='w-4 h4 text-black-700'/>,
        },
        {
            UserName : "test_server103",
            Key : <KeyIcon className="w-4 h-4 text-black-700"/>,
            OpenConnetion : <CommandLineIcon className='w-4 h4 text-black-700'/>,
        },
        
    ]



const data = [
    {
        ResourceName : "test_server101",
        Key : <KeyIcon className="w-4 h-4 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-",
        LandingServerIcon: <ServerStackIcon className='w-5 h-4 text-black-700'/>,
        CreateDeployIcon: <ClipboardDocumentListIcon className='w-5 h-4 text-black-700 cursor-pointer' onClick={()=>setIsOpenModalCreateDeploy(true)}/>,
        SSHUsers: <UserGroupIcon className='w-5 h-4 text-black-700 cursor-pointer' onClick={()=>setIsOpenModalSSHUser(true)}/>,
        EditSSHRsr: <PencilSquareIcon className='w-5 h-4 text-black-700 cursor-pointer' onClick={()=>setIsOpenModalSSHRsr(true)}/>,
    },
    {
        ResourceName : "test_server102",
        Key : <KeyIcon className="w-4 h-4 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-",
        LandingServerIcon: <ServerStackIcon className='w-5 h-4'/>,
        CreateDeployIcon: <ClipboardDocumentListIcon className='w-5 h-4'/>,
        SSHUsers: <UserGroupIcon className='w-5 h-4'/>,
        EditSSHRsr: <PencilSquareIcon className='w-5 h-4'/>,
    },
    {
        ResourceName : "test_server103",
        Key : <KeyIcon className="w-4 h-4 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-",
        LandingServerIcon: <ServerStackIcon className='w-5 h-4'/>,
        CreateDeployIcon: <ClipboardDocumentListIcon className='w-5 h-4'/>,
        SSHUsers: <UserGroupIcon className='w-5 h-4'/>,
        EditSSHRsr: <PencilSquareIcon className='w-5 h-4'/>,
    },
    {
        ResourceName : "test_server104",
        Key : <KeyIcon className="w-4 h-4 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-",
        LandingServerIcon: <ServerStackIcon className='w-5 h-4'/>,
        CreateDeployIcon: <ClipboardDocumentListIcon className='w-5 h-4'/>,
        SSHUsers: <UserGroupIcon className='w-5 h-4'/>,
        EditSSHRsr: <PencilSquareIcon className='w-5 h-4'/>,
    },
    {
        ResourceName : "test_server105",
        Key : <KeyIcon className="w-4 h-4 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-",
        LandingServerIcon: <ServerStackIcon className='w-5 h-4'/>,
        CreateDeployIcon: <ClipboardDocumentListIcon className='w-5 h-4'/>,
        SSHUsers: <UserGroupIcon className='w-5 h-4'/>,
        EditSSHRsr: <PencilSquareIcon className='w-5 h-4'/>,
    },
    {
        ResourceName : "test_server101",
        Key : <KeyIcon className="w-4 h-4 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-",
        LandingServerIcon: <ServerStackIcon className='w-5 h-4'/>,
        CreateDeployIcon: <ClipboardDocumentListIcon className='w-5 h-4'/>,
        SSHUsers: <UserGroupIcon className='w-5 h-4'/>,
        EditSSHRsr: <PencilSquareIcon className='w-5 h-4'/>,
    },
    {
        ResourceName : "test_server102",
        Key : <KeyIcon className="w-4 h-4 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-",
        LandingServerIcon: <ServerStackIcon className='w-5 h-4'/>,
        CreateDeployIcon: <ClipboardDocumentListIcon className='w-5 h-4'/>,
        SSHUsers: <UserGroupIcon className='w-5 h-4'/>,
        EditSSHRsr: <PencilSquareIcon className='w-5 h-4'/>,
    },
    {
        ResourceName : "test_server103",
        Key : <KeyIcon className="w-4 h-4 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-",
        LandingServerIcon: <ServerStackIcon className='w-5 h-4'/>,
        CreateDeployIcon: <ClipboardDocumentListIcon className='w-5 h-4'/>,
        SSHUsers: <UserGroupIcon className='w-5 h-4'/>,
        EditSSHRsr: <PencilSquareIcon className='w-5 h-4'/>,
    },
    {
        ResourceName : "test_server104",
        Key : <KeyIcon className="w-4 h-4 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-",
        LandingServerIcon: <ServerStackIcon className='w-5 h-4'/>,
        CreateDeployIcon: <ClipboardDocumentListIcon className='w-5 h-4'/>,
        SSHUsers: <UserGroupIcon className='w-5 h-4'/>,
        EditSSHRsr: <PencilSquareIcon className='w-5 h-4'/>,
    },
    {
        ResourceName : "test_server105",
        Key : <KeyIcon className="w-4 h-4 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-",
        LandingServerIcon: <ServerStackIcon className='w-5 h-4'/>,
        CreateDeployIcon: <ClipboardDocumentListIcon className='w-5 h-4'/>,
        SSHUsers: <UserGroupIcon className='w-5 h-4'/>,
        EditSSHRsr: <PencilSquareIcon className='w-5 h-4'/>,
    },
    {
        ResourceName : "test_server101",
        Key : <KeyIcon className="w-4 h-4 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-",
        LandingServerIcon: <ServerStackIcon className='w-5 h-4'/>,
        CreateDeployIcon: <ClipboardDocumentListIcon className='w-5 h-4'/>,
        SSHUsers: <UserGroupIcon className='w-5 h-4'/>,
        EditSSHRsr: <PencilSquareIcon className='w-5 h-4'/>,
    },
    {
        ResourceName : "test_server102",
        Key : <KeyIcon className="w-4 h-4 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-",
        LandingServerIcon: <ServerStackIcon className='w-5 h-4'/>,
        CreateDeployIcon: <ClipboardDocumentListIcon className='w-5 h-4'/>,
        SSHUsers: <UserGroupIcon className='w-5 h-4'/>,
        EditSSHRsr: <PencilSquareIcon className='w-5 h-4'/>,
    },
    {
        ResourceName : "test_server103",
        Key : <KeyIcon className="w-4 h-4 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-",
        LandingServerIcon: <ServerStackIcon className='w-5 h-4'/>,
        CreateDeployIcon: <ClipboardDocumentListIcon className='w-5 h-4'/>,
        SSHUsers: <UserGroupIcon className='w-5 h-4'/>,
        EditSSHRsr: <PencilSquareIcon className='w-5 h-4'/>,
    },
    {
        ResourceName : "test_server104",
        Key : <KeyIcon className="w-4 h-4 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-",
        LandingServerIcon: <ServerStackIcon className='w-5 h-4'/>,
        CreateDeployIcon: <ClipboardDocumentListIcon className='w-5 h-4'/>,
        SSHUsers: <UserGroupIcon className='w-5 h-4'/>,
        EditSSHRsr: <PencilSquareIcon className='w-5 h-4'/>,
    },
    {
        ResourceName : "test_server105",
        Key : <KeyIcon className="w-4 h-4 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-",
        LandingServerIcon: <ServerStackIcon className='w-5 h-4'/>,
        CreateDeployIcon: <ClipboardDocumentListIcon className='w-5 h-4'/>,
        SSHUsers: <UserGroupIcon className='w-5 h-4'/>,
        EditSSHRsr: <PencilSquareIcon className='w-5 h-4'/>,
    },
    {
        ResourceName : "sandil test",
        Key : <KeyIcon className="w-4 h-4 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-",
        LandingServerIcon: <ServerStackIcon className='w-5 h-4'/>,
        CreateDeployIcon: <ClipboardDocumentListIcon className='w-5 h-4'/>,
        SSHUsers: <UserGroupIcon className='w-5 h-4'/>,
        EditSSHRsr: <PencilSquareIcon className='w-5 h-4'/>,
    }
];

const [records,setRecords] = useState(data);
const [isOpenModal,setIsOpenModal] = useState(false);
const [isOpenModalDelete,setIsOpenModalDelete] = useState(false);
const [isOpenModalALS,setIsOpenModalALS] = useState(false);
const [isOpenModalEnumerate,setIsOpenModalEnumerate] = useState(false);
const [isOpenModalCreateDeploy,setIsOpenModalCreateDeploy] = useState(false);
const [isOpenModalSSHUser,setIsOpenModalSSHUser] = useState(false);
const [isOpenModalSSHRsr,setIsOpenModalSSHRsr] = useState(false);
const [credentialType,setCredentialType] = useState("password"); 

const helpTxt = [
    "Navigate to the Discovery tab and discover SSH servers.",
    "Enter the credentials of a user account by clicking the Credentials icon in the left corner of the screen against the required server. All private keys of the corresponding user account, and all user accounts of the server are enumerated automatically.",
    "To create unique key pairs, and deploy them to all user accounts of a server, click the Create and Deploy icon.",
    "If you want to manage the user account using only SSH key pairs, add a new user to the server by clicking the Resource Users icon. You can also view the user accounts available in the server using this icon."
]

function handleFilter(event){
    const newData = records.filter(row=>{
        return row.ResourceName.toLowerCase().includes(event.target.value.toLowerCase())
    })
    setRecords(newData)
}

	return (
        <>
            <div className="px-4 sm:px-6 lg:px-8">
            <div className='flex flex-wrap gap-3 my-4'>
                <button className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2" onClick={()=>setIsOpenModalEnumerate(true)}>
                    <span>Enumerate</span>
                </button>
                <button className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2" onClick={()=>setIsOpenModal(true)}>
                    <span>Credentials</span>
                </button>
                <button className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2" onClick={()=>setIsOpenModalALS(true)}>
                    <span>Associate Landing Server</span>
                </button>
                <Link to={'/ssh/resource-group'} className="hover:shadow-lg transition text-red-600 duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2 ml-auto" >
                    <RectangleStackIcon className='w-4 h-4'/>
                </Link>
            </div>
            <div className='max-w-[500px] w-full mb-5 block'>
                <div className="flex">
                    <label htmlFor="search-dropdown" className="mb-2 text-sm font-medium text-gray-900 sr-only">Your Email</label>
                    <div className="relative w-full">
                        <input type="search" id="search-dropdown" className="block p-3 w-full z-20 text-sm text-gray-900 bg-gray-50 rounded-full border-s-2 border border-gray-300 focus:ring-blue-500 focus:border-blue-500" placeholder="Search..."/>
                        <button type="button" className="absolute top-0 end-0 p-3 text-sm font-medium h-full focus:ring-4 focus:outline-none focus:ring-blue-300">
                            <MagnifyingGlassIcon className='w-4 h-4'/>
                            <span className="sr-only">Search</span>
                        </button>
                    </div>
                </div>
            </div>
            
            <DataTable 
                columns={columns} 
                data={records} 
                fixedHeader
                pagination 
                selectableRows 
            />
            </div>
            
            <HelpBox helpTxt={helpTxt}/>

            <Modal title={'Credentials'} isOpenModal={isOpenModal} setIsOpenModal={setIsOpenModal}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Login Name
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Credential Type
                    </label>
                    <div className="flex w-full">
                        <legend className="sr-only">Credential Type</legend>
                        <div className="flex w-full gap-7">
                            <div className="flex items-center">
                                <input
                                    id="password"
                                    name="discover-by"
                                    type="radio"
                                    defaultChecked={credentialType === 'password'}
                                    value="password"
                                    onClick={() => setCredentialType('password')}
                                    className="accent-theme-red1 h-4 w-4 border-gray-300"
                                />
                                <label htmlFor="password" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                    Password
                                </label>
                            </div>
                            <div className="flex items-center">
                                <input
                                    id="import-Key"
                                    name="discover-by"
                                    type="radio"
                                    defaultChecked={credentialType === 'import-Key'}
                                    value="import-Key"
                                    onClick={() => setCredentialType('import-Key')}
                                    className="accent-theme-red1 h-4 w-4 border-gray-300 text-theme-red1 focus:ring-theme-red1"
                                />
                                <label htmlFor="import-Key" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                    Import Key
                                </label>
                            </div>
                            <div className="flex items-center">
                                <input
                                    id="select-key"
                                    name="discover-by"
                                    type="radio"
                                    defaultChecked={credentialType === 'select-key'}
                                    value="select-key"
                                    onClick={() => setCredentialType('select-key')}
                                    className="accent-theme-red1 h-4 w-4 border-gray-300 text-theme-red1 focus:ring-theme-red1"
                                />
                                <label htmlFor="select-key" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                    Select Key
                                </label>
                            </div>
                            
                        </div>
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Login Password
                    </label>
                    
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="">&nbsp;</label>
                    <div className="relative flex items-start max-w-xs w-full">
                        <div className="flex h-6 items-center">
                            <input
                            id="comments"
                            aria-describedby="comments-description"
                            name="comments"
                            type="checkbox"
                            className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                            />
                        </div>
                        <div className="ml-3 text-sm leading-6">
                            <span> Root / Administrator</span>
                        </div>
                    </div>
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Save</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>

            <Modal title={'Landing Server Association'} isOpenModal={isOpenModalALS} setIsOpenModal={setIsOpenModalALS}>
                <div className="grid grid-cols-[100%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                        Landing Server
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full max-w-sm">
                        <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                            <option value="RSA">RSA</option>
                            <option value="DSA">DSA</option>
                            <option value="ECDSA">ECDSA</option>
                            <option value="ED25519">ED25519</option>
                        </select>
                    </div>
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Associate</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>
            
            <Modal title={'Confirmation'} isOpenModal={isOpenModalDelete} setIsOpenModal={setIsOpenModalDelete}>
                <div className="grid grid-cols-[100%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                        Delete selected resource(s)?
                    </label>
                    <div className="relative flex items-start max-w-xs w-full">
                        <div className="flex h-6 items-center">
                            <input
                            id="comments"
                            aria-describedby="comments-description"
                            name="comments"
                            type="checkbox"
                            className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                            />
                        </div>
                        <div className="ml-3 text-sm leading-6">
                            <span> Delete without disassociating the key </span>
                        </div>
                    </div>
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Ok</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>
            <Modal title={'Available User Credentials'} isOpenModal={isOpenModalEnumerate} setIsOpenModal={setIsOpenModalEnumerate}>
                <div className="grid grid-cols-[100%] max-w-xl mb-7 gap-y-5 items-center">
                    <div className="relative flex items-start w-full items-center">
                        <div className="flex rounded-md overflow-hidden ring-1 ring-inset ring-gray-300 w-full max-w-sm border">
                            <div id="dropdownSearch" className="z-10 bg-white w-full">
                                <ul className="h-48 px-3 pb-3 overflow-y-auto text-sm text-gray-700" aria-labelledby="dropdownSearchButton">
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-1" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-1" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">User Name</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-2" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-2" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Test</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-3" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-3" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (Milan) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-4" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-4" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">AWS GovCloud (US-East)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-5" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-5" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Canada (Central)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-6" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-6" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (Frankfurt) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-7" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-7" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US ISO West</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-8" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-8" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US West (N. California) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-9" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-9" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US West (Oregon) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-10" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-10" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Africa (Cape Town) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-11" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-11" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (Paris) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-12" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-12" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (Stockholm) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-13" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-13" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (London)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-14" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-14" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (Ireland)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-15" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-15" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Osaka) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-16" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-16" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Seoul)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-17" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-17" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Tokyo)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-18" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-18" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Middle East (Bahrain)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-19" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-19" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">South America (Sao Paulo)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-20" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-20" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Hong Kong)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-21" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-21" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">China (Beijing)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-22" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-22" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">AWS GovCloud (US)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-23" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-23" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Singapore) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-24" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-24" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Sydney)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-25" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-25" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US ISO East </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-26" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-26" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US East (N. Virginia)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-27" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-27" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US East (Ohio)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-28" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-28" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">China (Ningxia)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-29" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-29" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US ISOB East (Ohio) </label>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                        </div>
                        <div className="ml-3 text-sm leading-6 w-60">
                            &nbsp;
                        </div>
                    </div>
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Enumerate</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div> 
                
            </Modal>
            
            <Modal title={'Create and Deploy'} isOpenModal={isOpenModalCreateDeploy} setIsOpenModal={setIsOpenModalCreateDeploy}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Key Comment
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Key Type
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full max-w-sm">
                        <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                            <option value="RSA">RSA</option>
                            <option value="DSA">DSA</option>
                            <option value="ECDSA">ECDSA</option>
                            <option value="ED25519">ED25519</option>
                        </select>
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Key Length
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full max-w-sm">
                        <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                            <option value="1024">1024</option>
                            <option value="2048">2048</option>
                            <option value="4096">4096</option>
                        </select>
                    </div>
                    
                   
                    <label htmlFor="">&nbsp;</label>
                    <div className="relative flex items-start max-w-xs w-full">
                        <div className="flex h-6 items-center">
                            <input
                            id="comments"
                            aria-describedby="comments-description"
                            name="comments"
                            type="checkbox"
                            className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                            />
                        </div>
                        <div className="ml-3 text-sm leading-6">
                            <span> Root / Administrator</span>
                        </div>
                    </div>
                    <label htmlFor="">&nbsp;</label>
                    <div className="relative flex items-start max-w-xs w-full">
                        <small>(Use this option if the direct root user login is disabled. Enabling this option elevates a user login from a non-root user to a root user.)</small>
                    </div>
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Deploy</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>
            <Modal title={'SSH Users'} isOpenModal={isOpenModalSSHUser} setIsOpenModal={setIsOpenModalSSHUser}>
                <div className='w-full pt-5'>
                    <div className='max-w-[500px] w-full mb-5 block'>
                        <div className="flex">
                            <label htmlFor="search-dropdown" className="mb-2 text-sm font-medium text-gray-900 sr-only">Your Email</label>
                            <div className="relative w-full">
                                <input type="search" id="search-dropdown" className="block p-3 w-full z-20 text-sm text-gray-900 bg-gray-50 rounded-full border-s-2 border border-gray-300 focus:ring-blue-500 focus:border-blue-500" placeholder="Search..."/>
                                <button type="button" className="absolute top-0 end-0 p-3 text-sm font-medium h-full focus:ring-4 focus:outline-none focus:ring-blue-300">
                                    <MagnifyingGlassIcon className='w-4 h-4'/>
                                    <span className="sr-only">Search</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <DataTable 
                        columns={userColumn} 
                        data={userData} 
                        fixedHeader
                        pagination 
                        selectableRows 
                    />
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Associate</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>
            <Modal title={'Edit SSH Resource'} isOpenModal={isOpenModalSSHRsr} setIsOpenModal={setIsOpenModalSSHRsr}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Resource Name
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Description
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <textarea
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                            rows={3}
                        ></textarea>
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        IP Address
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Port
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Path
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Landing Server
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full max-w-sm">
                        <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                            <option value="RSA">RSA</option>
                            <option value="DSA">DSA</option>
                            <option value="ECDSA">ECDSA</option>
                            <option value="ED25519">ED25519</option>
                        </select>
                    </div>
                    
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Save</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>
        </>
		
	);
};

