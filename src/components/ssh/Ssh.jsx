import { useOutletContext } from 'react-router-dom'
import Header from '../../components/Header'

export default function Ssh(){
    const [masterTab, setMasterTab, sidebarOpen, setSidebarOpen, setSidebarShrink, sidebarShrink] = useOutletContext();
    return (
    <div>
        <div className={`${sidebarShrink ? "lg:pl-72" : "lg:pl-24" } bg-theme-gray2`}>
            <div className='bg-theme-gray2'>
                <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
            </div>
            <div className='bg-white'>
                <main className="py-10">
                    <div className="px-4 sm:px-6 lg:px-8 flex justify-between items-center">
                        <ul className='flex gap-y-3 gap-x-7 h-10 w-full border border-b border-t-0 border-l-0 border-r-0 mb-5'>
                            <li className={`
                                ${masterTab == "ssh-server" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}
                                ${masterTab == "ssh-resource" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}
                                `}>
                                <a href="javascript:;" onClick={()=>setMasterTab("ssh-server")} className={`${masterTab == "ssh-server" ? "text-black": "text-default-gray"}`}>
                                    SSH Server
                                </a>
                            </li>
                            <li className={`
                                ${masterTab == "ssh-keys" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""} 
                                ${masterTab == "ssh-keys-create" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}
                                ${masterTab == "ssh-key-history" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}
                                ${masterTab == "ssh-key-group" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}
                                ${masterTab == "add-key-group" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}
                                ${masterTab == "ssh-key-aa" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}
                                ${masterTab == "ssh-key-ra" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}
                                `}>
                                <a href="javascript:;" onClick={()=>setMasterTab("ssh-keys")} className={`${masterTab == "ssh-keys" ? "text-black": "text-default-gray"}`}>
                                    SSH Keys
                                </a>
                            </li>
                            <li className={`${masterTab == "ssh-users" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}`}>
                                <a href="javascript:;" onClick={()=>setMasterTab("ssh-users")} className={`${masterTab == "ssh-users" ? "text-black": "text-default-gray"}`}>
                                    SSH Users
                                </a>
                            </li>
                            <li className={`${masterTab == "discovered-keys" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}`}>
                                <a href="javascript:;" onClick={()=>setMasterTab("discovered-keys")} className={`${masterTab == "discovered-keys" ? "text-black": "text-default-gray"}`}>
                                    Discovered Keys
                                </a>
                            </li>
                            <li className={`${masterTab == "landing-servers" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}`}>
                                <a href="javascript:;" onClick={()=>setMasterTab("landing-servers")} className={`${masterTab == "landing-servers" ? "text-black": "text-default-gray"}`}>
                                    Landing Servers
                                </a>
                            </li>
                        </ul>
                    </div>
                    {/* {masterTab == "ssh-server" && 
                        <SshServer setMasterTab={setMasterTab}/>
                    }
                    {masterTab == "ssh-resource" && 
                        <ResourceGroup setMasterTab={setMasterTab}/>
                    }
                    {masterTab == "add-new-resource-group" && 
                        <AddNewResourceGroup setMasterTab={setMasterTab}/>
                    }
                    {masterTab == "ssh-keys" && 
                        <SshKeys setMasterTab={setMasterTab}/>
                    }
                    {masterTab == "ssh-keys-create" && 
                        <CreateSSHKey setMasterTab={setMasterTab}/>
                    }
                    {masterTab == "ssh-key-history" && 
                        <KeyHistory setMasterTab={setMasterTab}/>
                    }
                    {masterTab == "ssh-key-group" && 
                        <KeyGroup setMasterTab={setMasterTab}/>
                    }
                    {masterTab == "add-key-group" && 
                        <AddKeyGroup setMasterTab={setMasterTab}/>
                    }
                    {masterTab == "ssh-key-aa" && 
                        <KeyAssociationAudit setMasterTab={setMasterTab}/>
                    }
                    {masterTab == "ssh-key-ra" && 
                        <KeyRotationAudit setMasterTab={setMasterTab}/>
                    }
                    {masterTab == "ssh-users" && 
                        <SshUsers />
                    }
                    {masterTab == "discovered-keys" && 
                        <Discoveredkeys />
                    }
                    {masterTab == "landing-servers" && 
                        <LandingServers />
                    } */}
                </main>
            </div>
        </div>
    </div>                
    )
}