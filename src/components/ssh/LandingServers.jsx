import React, { useMemo, useState,Fragment} from 'react';
import DataTable from 'react-data-table-component';
import { EyeIcon, MagnifyingGlassIcon, PencilSquareIcon} from '@heroicons/react/24/outline';
import Modal from '../Modal';
import ListBox from '../ListBox';

export default function LandingServers(){
    const columns = [
        {
            name: 'Landing Server Name',
            selector: row => row.LandingServerName,
            sortable: true,
        },
        {
            name: 'Primary Server',
            selector: row => row.PrimaryServer,
            sortable: true,
        },
        {
            name: 'Secondary Server',
            selector: row => row.SecondaryServer,
            sortable: true,
        },
        {
            name: 'Description',
            selector: row => row.Description,
            sortable: true,
        },
        {
            name: 'Created By',
            selector: row => row.CreatedBy,
            sortable: true,
        },
        {
            name: '',
            selector: row => row.Edit,
            sortable: true,
            maxWidth: "30px",
            minWidth: "50px"
        },
       
    ];      

const data = [
    {
        LandingServerName:'private1',
        PrimaryServer:"user-name1",
        SecondaryServer:'30',
        Description:'30',
        CreatedBy:'30',
        Edit: <PencilSquareIcon className='w-5 h-4'/>,
    },
    {
        LandingServerName:'private1',
        PrimaryServer:"user-name1",
        SecondaryServer:'30',
        Description:'30',
        CreatedBy:'30',
        Edit: <PencilSquareIcon className='w-5 h-4'/>,
    },
    {
        LandingServerName:'private1',
        PrimaryServer:"user-name1",
        SecondaryServer:'30',
        Description:'30',
        CreatedBy:'30',
        Edit: <PencilSquareIcon className='w-5 h-4'/>,
    },
    {
        LandingServerName:'private1',
        PrimaryServer:"user-name1",
        SecondaryServer:'30',
        Description:'30',
        CreatedBy:'30',
        Edit: <PencilSquareIcon className='w-5 h-4'/>,
    },
];



const [records,setRecords] = useState(data);

let [isOpenModal, setIsOpenModal] = useState(false)


function handleFilter(event){
    const newData = records.filter(row=>{
        return row.ResourceName.toLowerCase().includes(event.target.value.toLowerCase())
    })
    setRecords(newData)
}

	return (
        <>
            <div className="px-4 sm:px-6 lg:px-8">
            <div className='flex flex-wrap gap-3 my-4'>
                <button onClick={()=>setIsOpenModal(true)} className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                    <span>Add</span>
                </button>
                <button className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                    <span>Delete</span>
                </button>
                
            </div>
            <div className='max-w-[500px] w-full mb-5 block'>
                <div className="flex">
                    <label htmlFor="search-dropdown" className="mb-2 text-sm font-medium text-gray-900 sr-only">Your Email</label>
                    <div className="relative w-full">
                        <input type="search" id="search-dropdown" className="block p-3 w-full z-20 text-sm text-gray-900 bg-gray-50 rounded-full border-s-2 border border-gray-300 focus:ring-blue-500 focus:border-blue-500" placeholder="Search..." required />
                        <button type="submit" className="absolute top-0 end-0 p-3 text-sm font-medium h-full focus:ring-4 focus:outline-none focus:ring-blue-300">
                            <MagnifyingGlassIcon className='w-4 h-4'/>
                            <span className="sr-only">Search</span>
                        </button>
                    </div>
                </div>
            </div>
            
            <DataTable 
                columns={columns} 
                data={records} 
                fixedHeader
                pagination 
                selectableRows 
            />

            <Modal title={'Add Landing Server'} isOpenModal={isOpenModal} setIsOpenModal={setIsOpenModal}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Landing Server Name
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Description
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <textarea
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                            rows="3"
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Primary Server
                    </label>
                    <div className="flex gap-4">
                        <select className="rounded-md shadow-sm ring-1 py-2 pl-1 ring-inset ring-gray-300 max-w-md w-full">
                            <option value="1">test1</option>
                            <option value="2">test2</option>
                            <option value="3" selected>test3</option>
                            <option value="4">test4</option>
                        </select>
                        <select className="rounded-md shadow-sm ring-1 py-2 pl-1 ring-inset ring-gray-300 max-w-md w-full">
                            <option value="1" selected>test1</option>
                            <option value="2">test2</option>
                            <option value="3">test3</option>
                            <option value="4">test4</option>
                        </select>
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Secondary Server
                    </label>
                    <div className="flex gap-4">
                        <select className="rounded-md shadow-sm ring-1 py-2 pl-1 ring-inset ring-gray-300 max-w-md w-full">
                            <option value="1">test1</option>
                            <option value="2">test2</option>
                            <option value="3">test3</option>
                            <option value="4" selected>test4</option>
                        </select>
                        <select className="rounded-md shadow-sm ring-1 py-2 pl-1 ring-inset ring-gray-300 max-w-md w-full">
                            <option value="1">test1</option>
                            <option value="2" selected>test2</option>
                            <option value="3">test3</option>
                            <option value="4">test4</option>
                        </select>
                    </div>
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Save</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>
            </div>
        </>
		
	);
};

