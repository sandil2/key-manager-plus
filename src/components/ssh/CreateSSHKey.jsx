import React, { useMemo, useState } from 'react';
import DataTable from 'react-data-table-component';
import { ArrowLeftIcon } from '@heroicons/react/24/outline';
import HelpBox from '../HelpBox';
import { Link } from 'react-router-dom';

const helpTxt = [
    "For ED25519 key type, make sure the deployed server(s) have an OpenSSH of version 6.5 or above installed.",
];

export default function CreateSSHKeys({setMasterTab}){

	return (
        <>
            <div className="px-4 sm:px-6 lg:px-8">    
                <h1 className='text-xl theme-gray8 my-3 flex'>
                    <span>Create SSH Key</span>
                    <small className='ml-auto'>
                        <Link to="/ssh/keys" className="hover:shadow-lg transition duration-300 ease-in-out bg-theme-gray2 rounded-full flex px-5 py-2 gap-3 items-center border border-theme-gray2 flex text-xs font-light">
                            <ArrowLeftIcon className="w-4 h-4"/> <span> Back</span>
                        </Link>
                    </small>
                </h1>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-2">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Key Name
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                </div>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-2">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        SSH Key Passphrase
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                </div>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-2">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Key Comment
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                </div>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-2">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Key Type
                    </label>
                    <div className='flex justify-between rounded-md shadow-sm ring-1 focus:ring-0 ring-inset ring-gray-300 w-full'>
                        <select
                        className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                        >
                            <option>Windows</option>
                            <option>Linux/Mac OS</option>
                        </select>
                    </div>
                </div>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-2">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Key Length
                    </label>
                    <div className='flex justify-between rounded-md shadow-sm ring-1 focus:ring-0 ring-inset ring-gray-300 w-full'>
                        <select
                        className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                        >
                            <option>Windows</option>
                            <option>Linux/Mac OS</option>
                        </select>
                    </div>
                </div>
            </div>
            <div className="px-4 sm:px-6 lg:px-8 py-3">
                <div className='flex'>
                    <button className='hover:shadow-lg transition duration-300 ease-in-out bg-black text-white hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center'>
                        <span>Create key</span>
                    </button>
                    <Link to="/ssh/keys" className='hover:shadow-lg transition duration-300 ease-in-out hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center'>
                        <span>Cancel</span>
                    </Link>
                </div>
                
            </div>

            <HelpBox helpTxt={helpTxt}/>
        </>
		
	);
};

