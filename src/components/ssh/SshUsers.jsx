import React, { useMemo, useState,Fragment} from 'react';
import { Menu, Transition } from '@headlessui/react'
import DataTable from 'react-data-table-component';
import { KeyIcon, TrashIcon,ArrowDownOnSquareIcon,EyeIcon,UserGroupIcon, RectangleGroupIcon, MagnifyingGlassIcon, ArrowPathRoundedSquareIcon, PencilSquareIcon
 } from '@heroicons/react/24/outline';
import Modal from '../Modal';
import Dropdown from '../Dropdown';
import { Link } from 'react-router-dom';

export default function SshUsers({setMasterTab}){

    const columns = [
        {
            name: 'User Name',
            selector: row => row.UserName,
            sortable: true,
        },
        {
            name: 'Resource Name',
            selector: row => row.ResourceName,
            sortable: true,
        },
        {
            name: 'Associated Key',
            selector: row => row.AssociatedKey,
            sortable: true,
        },
        {
            name: '',
            selector: row => row.View,
            sortable: true,
            maxWidth: "30px",
            minWidth: "50px"
        },
        {
            name: '',
            selector: row => row.PushKey,
            sortable: true,
            maxWidth: "30px",
            minWidth: "50px"
        },
        {
            name: '',
            selector: row => row.Export,
            sortable: true,
            maxWidth: "30px",
            minWidth: "50px"
        },
    ];      

const data = [
    {
        UserName:"user-name1",
        ResourceName:'private1',
        AssociatedKey:'30',
        View: <EyeIcon className='w-5 h-4'/>,
        PushKey: <KeyIcon className='w-5 h-4'/>,
        Export: <ArrowDownOnSquareIcon className='w-5 h-4'/>,
    },
    {
        UserName:"user-name2",
        ResourceName:'private2',
        AssociatedKey:'30',
        View: <EyeIcon className='w-5 h-4'/>,
        PushKey: <KeyIcon className='w-5 h-4'/>,
        Export: <ArrowDownOnSquareIcon className='w-5 h-4'/>,
    },
    {
        UserName:"user-name3",
        ResourceName:'private3',
        AssociatedKey:'30',
        View: <EyeIcon className='w-5 h-4'/>,
        PushKey: <KeyIcon className='w-5 h-4'/>,
        Export: <ArrowDownOnSquareIcon className='w-5 h-4'/>,
    },
    {
        UserName:"user-name4",
        ResourceName:'private4',
        AssociatedKey:'30',
        View: <EyeIcon className='w-5 h-4'/>,
        PushKey: <KeyIcon className='w-5 h-4'/>,
        Export: <ArrowDownOnSquareIcon className='w-5 h-4'/>,
    },
];



const [records,setRecords] = useState(data);
const [createSSHK,setCreateSSHK] = useState(false);
const [isOpenModal,setIsOpenModal] = useState(false);
const [isOpenModalCD,setIsOpenModalCD] = useState(false);
const [isOpenModalPKA,setIsOpenModalPKA] = useState(false);
const [isOpenModalCMD,setIsOpenModalCMD] = useState(false);
const [isOpenModalCG,setIsOpenModalCG] = useState(false);
const [isOpenModalEUP,setIsOpenModalEUP] = useState(false);
const [isOpenModalTF,setIsOpenModalTF] = useState(false);
const [isOpenModalDelete,setIsOpenModalDelete] = useState(false);
const [isOpenModalDelete2,setIsOpenModalDelete2] = useState(false);
const [isOpenModalImpCred,setIsOpenModalImpCred] = useState(false);


function handleFilter(event){
    const newData = records.filter(row=>{
        return row.ResourceName.toLowerCase().includes(event.target.value.toLowerCase())
    })
    setRecords(newData)
}

	return (
        <>
            <div className="px-4 sm:px-6 lg:px-8">
            <div className='flex flex-wrap gap-3 my-4'>
                <button className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2" onClick={()=>(setIsOpenModal(true))}>
                    <span>Credentials</span>
                </button>
                <button className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2" onClick={()=>setIsOpenModalPKA(true)}>
                    <span>Associate</span>
                </button>
                <button className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2" onClick={()=>setIsOpenModalCD(true)}>
                    <span>Create and Deploy</span>
                </button>
                <div className='z-[10]'>
                    <Dropdown DropText={"More"} >
                        <div className="px-1 py-1 " role="none">
                            <Menu.Item onClick={()=>(setIsOpenModalCMD(true))}>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Add Command
                                </button>
                                )}
                            </Menu.Item>
                            <Menu.Item onClick={()=>(setIsOpenModalCG(true))}>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Create Group
                                </button>
                                )}
                            </Menu.Item>
                            <Menu.Item onClick={()=>(setIsOpenModalEUP(true))}>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Edit User Path
                                </button>
                                )} 
                            </Menu.Item>
                            <Menu.Item onClick={()=>(setIsOpenModalTF(true))}>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Transfer File
                                </button>
                                )} 
                            </Menu.Item>
                            <Menu.Item onClick={()=>(setIsOpenModalDelete(true))}>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Dissociate
                                </button>
                                )} 
                            </Menu.Item>
                            <Menu.Item onClick={()=>(setIsOpenModalDelete2(true))}>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Delete
                                </button>
                                )}
                            </Menu.Item>
                        </div>
                    </Dropdown>
                </div>
                
                <Link to="/ssh/user-group" className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2 ml-auto">
                    <RectangleGroupIcon className='w-4 h-4'/>
                </Link>
                <button className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2" onClick={()=>setIsOpenModalImpCred(true)}>
                    <PencilSquareIcon className='w-4 h-4'/>
                </button>
                <button className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                    <ArrowPathRoundedSquareIcon className='w-4 h-4'/>
                </button>


            </div>
            <div className='max-w-[500px] w-full mb-5 block'>
                <div className="flex">
                    <label htmlFor="search-dropdown" className="mb-2 text-sm font-medium text-gray-900 sr-only">Your Email</label>
                    <div className="relative w-full">
                        <input type="search" id="search-dropdown" className="block p-3 w-full z-20 text-sm text-gray-900 bg-gray-50 rounded-full border-s-2 border border-gray-300 focus:ring-blue-500 focus:border-blue-500" placeholder="Search..." required />
                        <button type="submit" className="absolute top-0 end-0 p-3 text-sm font-medium h-full focus:ring-4 focus:outline-none focus:ring-blue-300">
                            <MagnifyingGlassIcon className='w-4 h-4'/>
                            <span className="sr-only">Search</span>
                        </button>
                    </div>
                </div>
            </div>
            
            <DataTable 
                columns={columns} 
                data={records} 
                fixedHeader
                pagination 
                selectableRows 
            />
            </div>
            
            <Modal title={'Credentials'} isOpenModal={isOpenModal} setIsOpenModal={setIsOpenModal}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Login Password
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Save</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>
            
            <Modal title={'Public Key Association'} isOpenModal={isOpenModalPKA} setIsOpenModal={setIsOpenModalPKA}>
                <div className={`${createSSHK ? "hidden" : ""}`}>
                    <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            Select Key
                        </label>
                        <div className="flex justify-center items-center rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full max-w-sm">
                            <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                <option value="RSA">RSA</option>
                                <option value="DSA">DSA</option>
                                <option value="ECDSA">ECDSA</option>
                                <option value="ED25519">ED25519</option>
                            </select>
                            <a href="javascript:;" className="px-3" onClick={()=>setCreateSSHK(true)}>
                                <KeyIcon className='w-4 h-4'/>
                            </a>
                        </div>
                        
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            &nbsp;
                        </label>
                        <div className="relative flex items-start max-w-xs w-full">
                            <div className="flex h-6 items-center">
                                <input
                                id="comments"
                                aria-describedby="comments-description"
                                name="comments"
                                type="checkbox"
                                className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                />
                            </div>
                            <div className="ml-3 text-sm leading-6">
                                <span> Elevate to "root" user. </span>
                                
                            </div>
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            &nbsp;
                        </label>
                        <div>
                            <small> (Use this option if the direct root user login is disabled. Enabling this option elevates a user login from a non-root user to a root user.)</small>
                        </div>
                    
                    </div>
                    <p className='pb-4 text-sm'>
                        If you choose the "Elevate to root user" option, ensure the resource has both "root" and non-root user credentials.
                    </p>
                    <hr />
                    <div className='w-full flex gap-5 pt-5'>
                        <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Associate</button>
                        <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                    </div>  
                </div>  
                <div className={`${createSSHK ? "" : "hidden"}`}>
                    <h1 className='mb-5'>Create SSH Key</h1>
                    <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            Key Name
                        </label>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                            <input
                                type="text"
                                autoComplete=""
                                className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                placeholder=""
                            />
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            SSH Key Passphrase
                        </label>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                            <input
                                type="text"
                                autoComplete=""
                                className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                placeholder=""
                            />
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            Key Comment
                        </label>
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                            <input
                                type="text"
                                autoComplete=""
                                className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                placeholder=""
                            />
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            Key Type
                        </label>
                        <div className="flex justify-center items-center rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full max-w-sm">
                            <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                <option value="RSA">RSA</option>
                                <option value="DSA">DSA</option>
                                <option value="ECDSA">ECDSA</option>
                                <option value="ED25519">ED25519</option>
                            </select>
                            
                        </div>
                        <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                            Key Length
                        </label>
                        <div className="flex justify-center items-center rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full max-w-sm">
                            <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                <option value="RSA">RSA</option>
                                <option value="DSA">DSA</option>
                                <option value="ECDSA">ECDSA</option>
                                <option value="ED25519">ED25519</option>
                            </select>
                        </div>

                        
                    </div>
                    <hr />
                    <div className='w-full flex gap-5 pt-5'>
                        <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Save</button>
                        <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full' onClick={()=>setCreateSSHK(false)}>Cancel</button>
                    </div>  
                </div>
            </Modal>
            <Modal title={'Create and Deploy'} isOpenModal={isOpenModalCD} setIsOpenModal={setIsOpenModalCD}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Key Comment
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Key Type
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full max-w-sm">
                        <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                            <option value="RSA">RSA</option>
                            <option value="DSA">DSA</option>
                            <option value="ECDSA">ECDSA</option>
                            <option value="ED25519">ED25519</option>
                        </select>
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Key Length
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full max-w-sm">
                        <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                            <option value="RSA">RSA</option>
                            <option value="DSA">DSA</option>
                            <option value="ECDSA">ECDSA</option>
                            <option value="ED25519">ED25519</option>
                        </select>
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        &nbsp;
                    </label>
                    <div className="relative flex items-start max-w-xs w-full">
                        <div className="flex h-6 items-center">
                            <input
                            id="comments"
                            aria-describedby="comments-description"
                            name="comments"
                            type="checkbox"
                            className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                            />
                        </div>
                        <div className="ml-3 text-sm leading-6">
                            <span> Elevate to "root" user. </span>
                            
                        </div>
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        &nbsp;
                    </label>
                    <div>
                        <small> (Use this option if the direct root user login is disabled. Enabling this option elevates a user login from a non-root user to a root user.)</small>
                    </div>
                   
                </div>
                <p className='pb-4 text-sm'>
                    Deploy option generates unique key pairs for each user. Each key pair is protected by unique, auto generated passphrases. The public keys will be deployed in the corresponding ssh servers.
                </p>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Deploy</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>
            <Modal title={'Add Command'} isOpenModal={isOpenModalCMD} setIsOpenModal={setIsOpenModalCMD}>
                <div className="grid grid-cols-[35%_65%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Key Name
                    </label>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Command
                    </label>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        key1
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Push</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
                <div className='w-full flex gap-5 pt-5'>
                    <small> Enter commands that needs to be added to authorized_key file along with the public key
                        <ul className='pl-3 list-disc'>
                            <li>command="usr/local/bin/script.sh"</li>
                            <li>from="host1/ip1,host2/ip2",command="usr/local/bin/script.sh"</li>
                            <li>from="ip1",no-pty,no-agent-forwarding,no-port-forwarding</li>
                        </ul>
                    </small>
                </div>  
            </Modal>

            <Modal title={'Create Group'} isOpenModal={isOpenModalCG} setIsOpenModal={setIsOpenModalCG}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Group Name
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Description
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                   
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Create</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>
            <Modal title={'User Directory Path'} isOpenModal={isOpenModalEUP} setIsOpenModal={setIsOpenModalEUP}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Path
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Save</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>
            <Modal title={'Transfer File'} isOpenModal={isOpenModalTF} setIsOpenModal={setIsOpenModalTF}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        File Location
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="file"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Path
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Transfer</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div> 
                <div className='w-full flex gap-5 pt-5'>
                <small>Any existing file with the same name will be replaced with the new one.Maximum file size supported is 16MB</small>    
                </div> 
            </Modal>
            <Modal title={'Confirmation'} isOpenModal={isOpenModalDelete} setIsOpenModal={setIsOpenModalDelete}>
                <div className="grid grid-cols-[100%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                        Dissociate key1 key from selected User?
                    </label>
                   
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Ok</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>
            <Modal title={'Confirmation'} isOpenModal={isOpenModalDelete2} setIsOpenModal={setIsOpenModalDelete2}>
                <div className="grid grid-cols-[100%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                        Delete selected keys?
                    </label>
                    <div className="relative flex items-start max-w-xs w-full">
                        <div className="flex h-6 items-center">
                            <input
                            id="comments"
                            aria-describedby="comments-description"
                            name="comments"
                            type="checkbox"
                            className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                            />
                        </div>
                        <div className="ml-3 text-sm leading-6">
                            <span> Delete without disassociating the key</span>
                        </div>
                    </div>
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Ok</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>
            <Modal title={'Import Credentials'} isOpenModal={isOpenModalImpCred} setIsOpenModal={setIsOpenModalImpCred}>
                <div className="grid grid-cols-[100%] max-w-xl mb-7 gap-y-5 items-center">
                     <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        File Location
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="file"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Import</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  

                <div className='w-full gap-5 pt-5 text-sm'>
                    <small>To simultaneously u  pdate credentials for multiple servers, upload a text file comprising of comma separated values (CSVs) in the following format:<br/>
                    <ul className='list-disc pl-3'>
                        <li>{"Resource_name,User_name,Password,Account_type<optional>"}</li>
                        <li>Account_type: Root / Admin</li>
                        <li>eg., test-server,test-user,password,root</li>
                        <li>test-server1,test-user1,password</li>
                    </ul>
                    </small>
                </div>

                <div>
                    
                </div>
            </Modal>
        </>
		
	);
};

