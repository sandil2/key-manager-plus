import React, { useMemo, useState } from 'react';
import DataTable from 'react-data-table-component';
import { ArrowLeftIcon, MagnifyingGlassIcon } from '@heroicons/react/24/outline';
import { Link } from 'react-router-dom';

 const columns = [
        {
            name: 'Group Name',
            selector: row => row.GroupName,
            sortable: false,
            maxWidth: "30px"
        },
        {
            name: 'Description',
            selector: row => row.Description,
            sortable: true,
        },
        {
            name: 'Created By',
            selector: row => row.CreatedBy,
            sortable: true,
        },
        {
            name: 'Last Modified By',
            selector: row => row.LastModifiedBy,
            sortable: true,
        },
    ];      

const data = [
    {
        GroupName : "Group 1",
        Description : "No Description",
        CreatedBy : "Tomy",
        LastModifiedBy :"yesterday",
    },
    {
        GroupName : "Group 2",
        Description : "No Description",
        CreatedBy : "Tomy",
        LastModifiedBy :"yesterday",
    },
    {
        GroupName : "Group 3",
        Description : "No Description",
        CreatedBy : "Tomy",
        LastModifiedBy :"yesterday",
    },
    {
        GroupName : "Group 4",
        Description : "No Description",
        CreatedBy : "Tomy",
        LastModifiedBy :"yesterday",
    },
    {
        GroupName : "Group 5",
        Description : "No Description",
        CreatedBy : "Tomy",
        LastModifiedBy :"yesterday",
    },
    {
        GroupName : "Group 6",
        Description : "No Description",
        CreatedBy : "Tomy",
        LastModifiedBy :"yesterday",
    },
    {
        GroupName : "Group 7",
        Description : "No Description",
        CreatedBy : "Tomy",
        LastModifiedBy :"yesterday",
    },
    
    
];


export default function ResourceGroup({setMasterTab}){
    
	return (
        <>
            
            <div className="px-4 sm:px-6 lg:px-8">    
                <h1 className='text-xl theme-gray8 my-3 flex'>
                    <span>Resource Group</span>
                    <small className='ml-auto'>
                        <Link to="/ssh" className="hover:shadow-lg transition duration-300 ease-in-out bg-theme-gray2 rounded-full flex px-5 py-2 gap-3 items-center border border-theme-gray2 flex text-xs font-light">
                            <ArrowLeftIcon className="w-4 h-4"/> <span> Back</span>
                        </Link>
                    </small>
                </h1>

                <div className='flex flex-wrap gap-3 my-4'>
                    <Link to="/ssh/add-key-group" className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                        <span>Add Group</span>
                    </Link>
                    <button className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                        <span>Delete</span>
                    </button>
                    
                </div>
                <div className='max-w-[500px] w-full mb-5 block'>
                    <div className="flex">
                        <label htmlFor="search-dropdown" className="mb-2 text-sm font-medium text-gray-900 sr-only">Your Email</label>
                        <div className="relative w-full">
                            <input type="search" id="search-dropdown" className="block p-3 w-full z-20 text-sm text-gray-900 bg-gray-50 rounded-full border-s-2 border border-gray-300 focus:ring-blue-500 focus:border-blue-500" placeholder="Search..."/>
                            <button type="button" className="absolute top-0 end-0 p-3 text-sm font-medium h-full focus:ring-4 focus:outline-none focus:ring-blue-300">
                                <MagnifyingGlassIcon className='w-4 h-4'/>
                                <span className="sr-only">Search</span>
                            </button>
                        </div>
                    </div>
                </div>
                <DataTable 
                    columns={columns} 
                    data={data} 
                    fixedHeader
                    pagination 
                    selectableRows 
                />
            </div>
        </>
		
	);
};

