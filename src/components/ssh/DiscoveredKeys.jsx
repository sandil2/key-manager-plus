import React, { useMemo, useState,Fragment} from 'react';
import { Menu, Transition } from '@headlessui/react'
import DataTable from 'react-data-table-component';
import { KeyIcon, TrashIcon,ArrowDownOnSquareIcon,EyeIcon,UserGroupIcon, RectangleGroupIcon, MagnifyingGlassIcon, ArrowPathRoundedSquareIcon, PencilSquareIcon
 } from '@heroicons/react/24/outline';
import Dropdown from '../Dropdown';
import HelpBox from '../HelpBox';

export default function Discoveredkeys(){
    
    const helpTxt = [
        "Discovered Keys are the SSH private keys available in the discovered SSH server's user accounts. Provide access to the relevant user accounts, to manage the discovered keys using Key Manager Plus.",
        "You can import the keys by clicking the Import button. You will be prompted for a passphrase, wherever applicable.",
        "The imported keys can be managed from the SSH → SSH keys tab."
    ]

    const columns = [
        {
            name: 'User Name',
            selector: row => row.UserName,
            sortable: true,
        },
        {
            name: 'Resource Name',
            selector: row => row.ResourceName,
            sortable: true,
        },
        {
            name: 'Discovered Key',
            selector: row => row.DiscoveredKeys,
            sortable: true,
        },
        {
            name: '',
            selector: row => row.View,
            sortable: true,
            maxWidth: "30px",
            minWidth: "50px"
        },
        {
            name: '',
            selector: row => row.PushKey,
            sortable: true,
            maxWidth: "30px",
            minWidth: "50px"
        },
        {
            name: '',
            selector: row => row.Export,
            sortable: true,
            maxWidth: "30px",
            minWidth: "50px"
        },
    ];      

const data = [
    {
        ResourceName:'private1',
        UserName:"user-name1",
        DiscoveredKeys:'30',
        View: <EyeIcon className='w-5 h-4'/>,
        PushKey: <KeyIcon className='w-5 h-4'/>,
        Export: <ArrowDownOnSquareIcon className='w-5 h-4'/>,
    },
    {
        ResourceName:'private2',
        UserName:"user-name2",
        DiscoveredKey:'30',
        View: <EyeIcon className='w-5 h-4'/>,
        PushKey: <KeyIcon className='w-5 h-4'/>,
        Export: <ArrowDownOnSquareIcon className='w-5 h-4'/>,
    },
    {
        ResourceName:'private3',
        UserName:"user-name3",
        DiscoveredKey:'30',
        View: <EyeIcon className='w-5 h-4'/>,
        PushKey: <KeyIcon className='w-5 h-4'/>,
        Export: <ArrowDownOnSquareIcon className='w-5 h-4'/>,
    },
    {
        ResourceName:'private4',
        UserName:"user-name4",
        DiscoveredKey:'30',
        View: <EyeIcon className='w-5 h-4'/>,
        PushKey: <KeyIcon className='w-5 h-4'/>,
        Export: <ArrowDownOnSquareIcon className='w-5 h-4'/>,
    },
];



const [records,setRecords] = useState(data);


function handleFilter(event){
    const newData = records.filter(row=>{
        return row.ResourceName.toLowerCase().includes(event.target.value.toLowerCase())
    })
    setRecords(newData)
}

	return (
        <>
            <div className="px-4 sm:px-6 lg:px-8">
            <div className='flex flex-wrap gap-3 my-4'>
                <button className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                    <span>Import</span>
                </button>
                
                <div className='z-[10] ml-auto'>
                    <Dropdown DropText={"Export"}>
                        <div className="px-1 py-1 " role="none">
                            <Menu.Item>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    PDF
                                </button>
                                )}
                            </Menu.Item>
                            <Menu.Item>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Email
                                </button>
                                )} 
                            </Menu.Item>
                        </div>
                    </Dropdown>
                </div>
            </div>
            <div className='max-w-[500px] w-full mb-5 block'>
                <div className="flex">
                    <label htmlFor="search-dropdown" className="mb-2 text-sm font-medium text-gray-900 sr-only">Your Email</label>
                    <div className="relative w-full">
                        <input type="search" id="search-dropdown" className="block p-3 w-full z-20 text-sm text-gray-900 bg-gray-50 rounded-full border-s-2 border border-gray-300 focus:ring-blue-500 focus:border-blue-500" placeholder="Search..." required />
                        <button type="submit" className="absolute top-0 end-0 p-3 text-sm font-medium h-full focus:ring-4 focus:outline-none focus:ring-blue-300">
                            <MagnifyingGlassIcon className='w-4 h-4'/>
                            <span className="sr-only">Search</span>
                        </button>
                    </div>
                </div>
            </div>
            
            <DataTable 
                columns={columns} 
                data={records} 
                fixedHeader
                pagination 
                selectableRows 
            />
            </div>

            <HelpBox helpTxt={helpTxt}/>
        </>
		
	);
};

