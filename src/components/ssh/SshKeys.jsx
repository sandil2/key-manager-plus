import React, { useMemo, useState } from 'react';
import { Menu, Transition } from '@headlessui/react'
import DataTable from 'react-data-table-component';
import { KeyIcon, TrashIcon,ArrowDownOnSquareIcon,EyeIcon,InformationCircleIcon, RectangleGroupIcon, MagnifyingGlassIcon, ArrowPathRoundedSquareIcon, PencilSquareIcon
 } from '@heroicons/react/24/outline';
import Modal from '../Modal';
import Dropdown from '../Dropdown';
import { Link } from 'react-router-dom';

export default function SshKeys({setMasterTab}){
    const columns = [
        {
            name: 'Key Name',
            selector: row => row.KeyName,
            sortable: false,
            maxWidth: "30px"
        },
        {
            name: 'Key Type',
            selector: row => row.KeyType,
            sortable: true,
        },
        {
            name: 'Key Length',
            selector: row => row.KeyLength,
            sortable: true,
        },
        {
            name: 'Finger Print',
            selector: row => row.FingerPrint,
            sortable: true,
        },
        {
            name: 'Created By',
            selector: row => row.CreatedBy,
            sortable: true,
        },
        {
            name: 'Age',
            selector: row => row.Age,
            sortable: true,
        },
        {
            name: 'Address1',
            selector: row => row.Address1,
            sortable: true,
        },
        {
            name: 'Address2',
            selector: row => row.Address2,
            sortable: true,
        },
        {
            name: 'Date2',
            selector: row => row.Date2,
            sortable: true,
        },
        {
            name: 'Date1',
            selector: row => row.Date1,
            sortable: true,
        },
        {
            name: 'SampleValues',
            selector: row => row.SampleValues,
            sortable: true,
        },
        {
            name: 'Email',
            selector: row => row.Email,
            sortable: true,
        },
        {
            name: 'Phone Number',
            selector: row => row.PhoneNumber,
            sortable: true,
        },
        {
            name: '',
            selector: row => row.View,
            sortable: true,
            maxWidth: "30px",
            minWidth: "50px"
        },
        {
            name: '',
            selector: row => row.PushKey,
            sortable: true,
            maxWidth: "30px",
            minWidth: "50px"
        },
        {
            name: '',
            selector: row => row.Export,
            sortable: true,
            maxWidth: "30px",
            minWidth: "50px"
        },
    ];      

const data = [
    {
        KeyName:"key-name1",
        KeyType:'private',
        KeyLength:'30',
        FingerPrint:"no",
        CreatedBy:"me",
        Age:'41',
        Address1:"Address1",
        Address2:"Address2",
        Date2:"17//7/17",
        Date1:"12/12/12",
        SampleValues:"SampleValues",
        Email: "test@test.com",
        PhoneNumber: 1234567890,
        View: <EyeIcon className='w-5 h-4'/>,
        PushKey: <KeyIcon className='w-5 h-4'/>,
        Export: <ArrowDownOnSquareIcon className='w-5 h-4'/>,
    },
    {
        KeyName:"key-name1",
        KeyType:'private',
        KeyLength:'30',
        FingerPrint:"no",
        CreatedBy:"me",
        Age:'41',
        Address1:"Address1",
        Address2:"Address2",
        Date2:"17//7/17",
        Date1:"12/12/12",
        SampleValues:"SampleValues",
        Email: "test@test.com",
        PhoneNumber: 1234567890,
        View: <EyeIcon className='w-5 h-4'/>,
        PushKey: <KeyIcon className='w-5 h-4'/>,
        Export: <ArrowDownOnSquareIcon className='w-5 h-4'/>,
    },
    {
        KeyName:"key-name1",
        KeyType:'private',
        KeyLength:'30',
        FingerPrint:"no",
        CreatedBy:"me",
        Age:'41',
        Address1:"Address1",
        Address2:"Address2",
        Date2:"17//7/17",
        Date1:"12/12/12",
        SampleValues:"SampleValues",
        Email: "test@test.com",
        PhoneNumber: 1234567890,
        View: <EyeIcon className='w-5 h-4'/>,
        PushKey: <KeyIcon className='w-5 h-4'/>,
        Export: <ArrowDownOnSquareIcon className='w-5 h-4'/>,
    },
    {
        KeyName:"key-name1",
        KeyType:'private',
        KeyLength:'30',
        FingerPrint:"no",
        CreatedBy:"me",
        Age:'41',
        Address1:"Address1",
        Address2:"Address2",
        Date2:"17//7/17",
        Date1:"12/12/12",
        SampleValues:"SampleValues",
        Email: "test@test.com",
        PhoneNumber: 1234567890,
        View: <EyeIcon className='w-5 h-4'/>,
        PushKey: <KeyIcon className='w-5 h-4'/>,
        Export: <ArrowDownOnSquareIcon className='w-5 h-4'/>,
    },
    
];

const [records,setRecords] = useState(data);
const [isOpenModal,setIsOpenModal] = useState(false);
const [isOpenModalCG,setIsOpenModalCG] = useState(false);
const [isOpenModalDelete,setIsOpenModalDelete] = useState(false);
const [isOpenModalDelete2,setIsOpenModalDelete2] = useState(false);
const [isOpenModalImpK,setIsOpenModalImpK] = useState(false);
const [isOpenModalEditSK,setIsOpenModalEditSK] = useState(false);
const [isOpenModalPKA,setIsOpenModalPKA] = useState(false);

function handleFilter(event){
    const newData = records.filter(row=>{
        return row.ResourceName.toLowerCase().includes(event.target.value.toLowerCase())
    })
    setRecords(newData)
}

	return (
        <>
            <div className="px-4 sm:px-6 lg:px-8">
            <div className='flex flex-wrap gap-3 my-4'>
                <Link to="/ssh/create-ssh-key"className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                    <span>Create</span>
                </Link>
                <button className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2" onClick={()=>setIsOpenModalPKA(true)}>
                    <span>Associate</span>
                </button>
                <Link to="/ssh/key-history" className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2" >
                    <span>Key History</span>
                </Link>
                <button className="hover:shadow-lg transition text-red-600 duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2" onClick={()=>setIsOpenModal(true)}>
                    <span>Rotate </span><TrashIcon className='w-4 h-4'/>
                </button>
                <div className='z-[10]'>
                    <Dropdown DropText={"More"} >
                        <div className="px-1 py-1 " role="none">
                            <Menu.Item onClick={()=>(setIsOpenModalCG(true))}>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Create Group
                                </button>
                                )}
                            </Menu.Item>
                            <Menu.Item onClick={()=>(setIsOpenModalDelete(true))}>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Dissociate
                                </button>
                                )} 
                            </Menu.Item>
                            <Menu.Item onClick={()=>(setIsOpenModalImpK(true))}>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                Import Key
                                </button>
                                )}
                            </Menu.Item>
                            <Menu.Item onClick={()=>(setIsOpenModalEditSK(true))}>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Edit
                                </button>)}
                            </Menu.Item>
                            <Menu.Item onClick={()=>(setIsOpenModalDelete2(true))}>
                                {({ active }) => (
                                <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                    Delete
                                </button>
                                )}
                            </Menu.Item>
                        </div>
                    </Dropdown>
                </div>
                <Link to="/ssh/key-group" className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2 ml-auto" >
                    <RectangleGroupIcon className='w-4 h-4'/>
                </Link>
                <Link to="/ssh/key-association-audit" className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                    <PencilSquareIcon className='w-4 h-4'/>
                </Link>
                <Link to="/ssh/key-rotation-audit" className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                    <ArrowPathRoundedSquareIcon className='w-4 h-4'/>
                </Link>
            </div>
            <div className='max-w-[500px] w-full mb-5 block'>
                <div className="flex">
                    <label htmlFor="search-dropdown" className="mb-2 text-sm font-medium text-gray-900 sr-only">Your Email</label>
                    <div className="relative w-full">
                        <input type="search" id="search-dropdown" className="block p-3 w-full z-20 text-sm text-gray-900 bg-gray-50 rounded-full border-s-2 border border-gray-300 focus:ring-blue-500 focus:border-blue-500" placeholder="Search..." required />
                        <button type="submit" className="absolute top-0 end-0 p-3 text-sm font-medium h-full focus:ring-4 focus:outline-none focus:ring-blue-300">
                            <MagnifyingGlassIcon className='w-4 h-4'/>
                            <span className="sr-only">Search</span>
                        </button>
                    </div>
                </div>
            </div>
            
            <DataTable 
                columns={columns} 
                data={records} 
                fixedHeader
                pagination 
                selectableRows 
            />
            </div>
            
            <Modal title={'Key Rotation'} isOpenModal={isOpenModal} setIsOpenModal={setIsOpenModal}>
                <label htmlFor="ip-address-range" className="block text-sm font-medium leading-6 text-gray-900 mb-7">
                        Push key file
                </label>
                <div className="grid grid-cols-[100%] max-w-xl mb-7 gap-y-5 items-center">
                    <div className="relative flex items-start max-w-xs w-full">
                        <div className="flex h-6 items-center">
                            <input
                            id="comments"
                            aria-describedby="comments-description"
                            name="comments"
                            type="checkbox"
                            className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                            />
                        </div>
                        <div className="ml-3 text-sm leading-6">
                            <span> Push private key file to remote user account </span>
                        </div>
                    </div>
                    <div className="relative flex items-start max-w-xs w-full">
                        <div className="flex h-6 items-center">
                            <input
                            id="comments"
                            aria-describedby="comments-description"
                            name="comments"
                            type="checkbox"
                            className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                            />
                        </div>
                        <div className="ml-3 text-sm leading-6">
                            <span> Push public key file to remote user account </span>
                        </div>
                    </div>
                    
                    <div className="relative flex items-start max-w-xs w-full">
                        <div className="flex h-6 items-center">
                            <input
                            id="comments"
                            aria-describedby="comments-description"
                            name="comments"
                            type="checkbox"
                            className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                            />
                        </div>
                        <div className="ml-3 text-sm leading-6">
                            <span> Use keyname as filename</span>
                        </div>
                    </div>
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Rotate</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>
            <Modal title={'Public Key Association'} isOpenModal={isOpenModalPKA} setIsOpenModal={setIsOpenModalPKA}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Search
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        &nbsp;
                    </label>
                    <div className="relative flex w-full items-center">
                        <div className="flex rounded-md overflow-hidden ring-1 ring-inset ring-gray-300 min-w-full border">
                            <div id="dropdownSearch" className="z-10 bg-white w-full">
                                <ul className="h-48 px-3 pb-3 overflow-y-auto text-sm text-gray-700 w-full" aria-labelledby="dropdownSearchButton">
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-1" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-1" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">User Name</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-2" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-2" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Test</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-3" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-3" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (Milan) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-4" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-4" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">AWS GovCloud (US-East)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-5" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-5" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Canada (Central)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-6" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-6" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (Frankfurt) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-7" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-7" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US ISO West</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-8" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-8" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US West (N. California) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-9" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-9" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US West (Oregon) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-10" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-10" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Africa (Cape Town) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-11" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-11" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (Paris) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-12" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-12" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (Stockholm) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-13" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-13" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (London)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-14" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-14" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (Ireland)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-15" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-15" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Osaka) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-16" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-16" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Seoul)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-17" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-17" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Tokyo)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-18" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-18" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Middle East (Bahrain)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-19" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-19" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">South America (Sao Paulo)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-20" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-20" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Hong Kong)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-21" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-21" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">China (Beijing)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-22" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-22" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">AWS GovCloud (US)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-23" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-23" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Singapore) </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-24" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-24" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Sydney)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-25" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-25" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US ISO East </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-26" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-26" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US East (N. Virginia)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-27" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-27" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US East (Ohio)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-28" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-28" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">China (Ningxia)</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                        <input id="checkbox-item-29" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                        <label htmlFor="checkbox-item-29" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US ISOB East (Ohio) </label>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                        </div>
                        <div className="ml-3 text-sm leading-6 w-60">
                            &nbsp;
                        </div>
                    </div>
                    <label htmlFor="">&nbsp;</label>
                    <div className="relative flex items-start max-w-xs w-full">
                        <div className="flex h-6 items-center">
                            <input
                            id="comments"
                            aria-describedby="comments-description"
                            name="comments"
                            type="checkbox"
                            className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                            />
                        </div>
                        <div className="ml-3 text-sm leading-6">
                            <span> Elevate to "root" user.</span>
                        </div>
                    </div>
                    <label htmlFor="">&nbsp;</label>
                    <small>(Use this option if the direct root user login is disabled. Enabling this option elevates a user login from a non-root user to a root user.)</small>
                </div>
                
                <hr />
                <div className='w-full flex gap-5 py-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Associate</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <InformationCircleIcon className="w-4 h-4" /><small>If you choose the "Elevate to root user" option, ensure the resource has both "root" and non-root user credentials.</small>
                </div>  


            </Modal>

            <Modal title={'Create Group'} isOpenModal={isOpenModalCG} setIsOpenModal={setIsOpenModalCG}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Group Name
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Description
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                   
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Create</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>
            <Modal title={'Confirmation'} isOpenModal={isOpenModalDelete2} setIsOpenModal={setIsOpenModalDelete2}>
                <div className="grid grid-cols-[100%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                        Delete selected keys?
                    </label>
                    <div className="relative flex items-start max-w-xs w-full">
                        <div className="flex h-6 items-center">
                            <input
                            id="comments"
                            aria-describedby="comments-description"
                            name="comments"
                            type="checkbox"
                            className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                            />
                        </div>
                        <div className="ml-3 text-sm leading-6">
                            <span> Delete without disassociating the key</span>
                        </div>
                    </div>
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Ok</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>
            <Modal title={'Confirmation'} isOpenModal={isOpenModalDelete} setIsOpenModal={setIsOpenModalDelete}>
                <div className="grid grid-cols-[100%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900">
                        Dissociate the User test of test_server_1 from selected Key?
                    </label>
                    <div className="relative flex items-start max-w-xs w-full">
                        <div className="flex h-6 items-center">
                            <input
                            id="comments"
                            aria-describedby="comments-description"
                            name="comments"
                            type="checkbox"
                            className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                            />
                        </div>
                        <div className="ml-3 text-sm leading-6">
                            <span> Disassociate key locally, if remote disassociation fails </span>
                        </div>
                    </div>
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Ok</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>
             <Modal title={'Import Key'} isOpenModal={isOpenModalImpK} setIsOpenModal={setIsOpenModalImpK}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        File Location

                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="file"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Key Name
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        SSH Key Passphrase
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Key Comment
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                   
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Add</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>
             <Modal title={'Edit SSH Key'} isOpenModal={isOpenModalEditSK} setIsOpenModal={setIsOpenModalEditSK}>
                <div className="grid grid-cols-[30%_70%] max-w-xl mb-7 gap-y-5 items-center">
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Comment
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        &nbsp;
                    </label>
                    <div className="relative flex items-start max-w-xs w-full">
                        <div className="flex h-6 items-center">
                            <input
                            id="comments"
                            aria-describedby="comments-description"
                            name="comments"
                            type="checkbox"
                            className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                            />
                        </div>
                        <div className="ml-3 text-sm leading-6">
                            <span> Update the comment in the 'authorized_keys' file of the associated user(s) account.</span>
                        </div>
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Address1
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Address2
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Phone number
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="text"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Date1
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="date"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Date2
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="date"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Email
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <input
                            type="email"
                            autoComplete=""
                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                            placeholder=""
                        />
                    </div>
                    <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                        Values
                    </label>
                    <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                        <select className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                            <option value="RSA">RSA</option>
                            <option value="DSA">DSA</option>
                            <option value="ECDSA">ECDSA</option>
                            <option value="ED25519">ED25519</option>
                        </select>
                    </div>
                   
                </div>
                <hr />
                <div className='w-full flex gap-5 pt-5'>
                    <button className='bg-black text-white text-sm py-2 px-7 rounded-full border border-black'>Add</button>
                    <button className='bg-white text-black text-sm py-2 px-7 border border-black rounded-full'>Cancel</button>
                </div>  
            </Modal>
        </>
		
	);
};

