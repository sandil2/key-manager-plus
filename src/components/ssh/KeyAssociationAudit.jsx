import React, { useMemo, useState } from 'react';
import DataTable from 'react-data-table-component';
import { MagnifyingGlassIcon, ArrowLeftIcon } from '@heroicons/react/24/outline';
import { Link } from 'react-router-dom';

 const columns = [
        {
            name: 'Key Name',
            selector: row => row.KeyName,
            sortable: false,
            maxWidth: "30px"
        },
        {
            name: 'Start Time',
            selector: row => row.StartTime,
            sortable: true,
        },
        {
            name: 'Rotated By',
            selector: row => row.RotatedBy,
            sortable: true,
        },
        {
            name: 'Finish Time',
            selector: row => row.FinishTime,
            sortable: true,
        },
        {
            name: 'Status',
            selector: row => row.status,
            sortable: true,
        }
    ];      

const data = [
    {
        KeyName : "1",
        StartTime : "Created",
        RotatedBy : "ba:de:c3:e0:64:3a:b6:54:81:c3:df:7f:98:46:ec:55",
        FinishTime :"",
        status : "10", 
    },
    {
        KeyName : "2",
        StartTime : "Created",
        RotatedBy : "ba:de:c3:e0:64:3a:b6:54:81:c3:df:7f:98:46:ec:55",
        FinishTime :"",
        status : "10", 
    },
    {
        KeyName : "3",
        StartTime : "Created",
        RotatedBy : "ba:de:c3:e0:64:3a:b6:54:81:c3:df:7f:98:46:ec:55",
        FinishTime :"",
        status : "10", 
    },
    {
        KeyName : "4",
        StartTime : "Created",
        RotatedBy : "ba:de:c3:e0:64:3a:b6:54:81:c3:df:7f:98:46:ec:55",
        FinishTime :"",
        status : "10", 
    },
    {
        KeyName : "5",
        StartTime : "Created",
        RotatedBy : "ba:de:c3:e0:64:3a:b6:54:81:c3:df:7f:98:46:ec:55",
        FinishTime :"",
        status : "10", 
    },
    
    
];


export default function KeyAssociationAudit({setMasterTab}){
    
	return (
        <>
            
            <div className="px-4 sm:px-6 lg:px-8">    
                <h1 className='text-xl theme-gray8 my-3 flex'>
                    <span>Key Association Audit</span>
                    <small className='ml-auto'>
                        <Link to="/ssh/keys" className="hover:shadow-lg transition duration-300 ease-in-out bg-theme-gray2 rounded-full flex px-5 py-2 gap-3 items-center border border-theme-gray2 flex text-xs font-light">
                            <ArrowLeftIcon className="w-4 h-4"/> <span> Back</span>
                        </Link>
                    </small>
                </h1>
                <div className='max-w-[500px] w-full mb-5 block'>
                    <div className="flex">
                        <label htmlFor="search-dropdown" className="mb-2 text-sm font-medium text-gray-900 sr-only">Your Email</label>
                        <div className="relative w-full">
                            <input type="search" id="search-dropdown" className="block p-3 w-full z-20 text-sm text-gray-900 bg-gray-50 rounded-full border-s-2 border border-gray-300 focus:ring-blue-500 focus:border-blue-500" placeholder="Search..."/>
                            <button type="button" className="absolute top-0 end-0 p-3 text-sm font-medium h-full focus:ring-4 focus:outline-none focus:ring-blue-300">
                                <MagnifyingGlassIcon className='w-4 h-4'/>
                                <span className="sr-only">Search</span>
                            </button>
                        </div>
                    </div>
                </div>
                <DataTable 
                    columns={columns} 
                    data={data} 
                    fixedHeader
                    pagination 
                    selectableRows 
                />
            </div>
        </>
		
	);
};

