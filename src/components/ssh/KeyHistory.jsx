import React, { useMemo, useState } from 'react';
import DataTable from 'react-data-table-component';
import { ArrowLeftIcon, MagnifyingGlassIcon } from '@heroicons/react/24/outline';
import { Link } from 'react-router-dom';

 const columns = [
        {
            name: 'Version',
            selector: row => row.Version,
            sortable: false,
            maxWidth: "30px"
        },
        {
            name: 'Description',
            selector: row => row.Description,
            sortable: true,
        },
        {
            name: 'Finger Print',
            selector: row => row.FingerPrint,
            sortable: true,
        },
        {
            name: 'Type',
            selector: row => row.Type,
            sortable: true,
        },
        {
            name: 'Key Length',
            selector: row => row.KeyLength,
            sortable: true,
        },
        {
            name: 'User Name',
            selector: row => row.UserName,
            sortable: true,
        },
        {
            name: 'Time',
            selector: row => row.Time,
            sortable: true,
        },
    ];      

const data = [
    {
        Version : "1",
        Description : "Created",
        FingerPrint : "ba:de:c3:e0:64:3a:b6:54:81:c3:df:7f:98:46:ec:55",
        Type :"",
        KeyLength : "10", 
        UserName: "demouser",
        Time: "Feb 9, 2017 16:00"
    },
    {
        Version : "2",
        Description : "Created",
        FingerPrint : "ba:de:c3:e0:64:3a:b6:54:81:c3:df:7f:98:46:ec:55",
        Type :"",
        KeyLength : "10", 
        UserName: "demouser",
        Time: "Feb 9, 2017 16:00"
    },
    {
        Version : "3",
        Description : "Created",
        FingerPrint : "ba:de:c3:e0:64:3a:b6:54:81:c3:df:7f:98:46:ec:55",
        Type :"",
        KeyLength : "10", 
        UserName: "demouser",
        Time: "Feb 9, 2017 16:00"
    },
    {
        Version : "4",
        Description : "Created",
        FingerPrint : "ba:de:c3:e0:64:3a:b6:54:81:c3:df:7f:98:46:ec:55",
        Type :"",
        KeyLength : "10", 
        UserName: "demouser",
        Time: "Feb 9, 2017 16:00"
    },
    {
        Version : "5",
        Description : "Created",
        FingerPrint : "ba:de:c3:e0:64:3a:b6:54:81:c3:df:7f:98:46:ec:55",
        Type :"",
        KeyLength : "10", 
        UserName: "demouser",
        Time: "Feb 9, 2017 16:00"
    },
    {
        Version : "6",
        Description : "Created",
        FingerPrint : "ba:de:c3:e0:64:3a:b6:54:81:c3:df:7f:98:46:ec:55",
        Type :"",
        KeyLength : "10", 
        UserName: "demouser",
        Time: "Feb 9, 2017 16:00"
    },
    {
        Version : "7",
        Description : "Created",
        FingerPrint : "ba:de:c3:e0:64:3a:b6:54:81:c3:df:7f:98:46:ec:55",
        Type :"",
        KeyLength : "10", 
        UserName: "demouser",
        Time: "Feb 9, 2017 16:00"
    },
    {
        Version : "8",
        Description : "Created",
        FingerPrint : "ba:de:c3:e0:64:3a:b6:54:81:c3:df:7f:98:46:ec:55",
        Type :"",
        KeyLength : "10", 
        UserName: "demouser",
        Time: "Feb 9, 2017 16:00"
    },
    
];


export default function KeyHistory({setMasterTab}){
    
	return (
        <>
            
            <div className="px-4 sm:px-6 lg:px-8">    
                <h1 className='text-xl theme-gray8 my-3 flex'>
                    <span>Key History</span>
                    <small className='ml-auto'>
                        <Link to="/ssh/keys" className="hover:shadow-lg transition duration-300 ease-in-out bg-theme-gray2 rounded-full flex px-5 py-2 gap-3 items-center border border-theme-gray2 flex text-xs font-light">
                            <ArrowLeftIcon className="w-4 h-4"/> <span> Back</span>
                        </Link>
                        
                    </small>
                </h1>
                <div className='max-w-[500px] w-full mb-5 block'>
                    <div className="flex">
                        <label htmlFor="search-dropdown" className="mb-2 text-sm font-medium text-gray-900 sr-only">Your Email</label>
                        <div className="relative w-full">
                            <input type="search" id="search-dropdown" className="block p-3 w-full z-20 text-sm text-gray-900 bg-gray-50 rounded-full border-s-2 border border-gray-300 focus:ring-blue-500 focus:border-blue-500" placeholder="Search..."/>
                            <button type="button" className="absolute top-0 end-0 p-3 text-sm font-medium h-full focus:ring-4 focus:outline-none focus:ring-blue-300">
                                <MagnifyingGlassIcon className='w-4 h-4'/>
                                <span className="sr-only">Search</span>
                            </button>
                        </div>
                    </div>
                </div>
                <DataTable 
                    columns={columns} 
                    data={data} 
                    fixedHeader
                    pagination 
                    selectableRows 
                />
            </div>
        </>
		
	);
};

