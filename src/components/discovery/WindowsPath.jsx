import { useOutletContext } from 'react-router-dom'
import Header from '../Header'
import HelpBox from '../HelpBox';
import { useState } from 'react';

export default function WindowsPath(){
    const [sidebarOpen, setSidebarOpen, setSidebarShrink, sidebarShrink] = useOutletContext();

    const helpTxt = [
        "Specify the server name, port number, user credentials, and the path of the load balancer from which certificates have to be discovered.Currently supports certificate discovery from Nginx and F5 load balancers.",
        "Choose the Select Key option to access password-less resources through key based authentication. Upload the private key associated with the required account and specify the key passphrase.",
        "Discover certificate list fetches all the certificates available in the specified path and allows you to choose certificates that need to be imported.",
        "Certificate files with extensions .keystore and .pfx are grouped separately under JKS / PKCS. <br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To import these certificates, click JKS / PKCS, choose the certificate files that you wish to import, provide the file passphrase and click Import.",
    ]

    return (
        <div>
        <div className={`${sidebarShrink ? "lg:pl-72" : "lg:pl-24" } bg-theme-gray2`}>
            <div className='bg-theme-gray2'>
                <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
            </div>
            <div className='bg-white'>
                <main className="py-10">
                    <div className="px-4 sm:px-6 lg:px-8 flex items-center gap-5">
                        <h1 className='text-xl'>Shared Path</h1>
                        <button className='hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2 ml-auto'>
                            <span>JKS/PKCS</span>
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 15" fill="currentColor" className="h-4 w-4 shrink-0">
                                    <path d="M5.588 7.262C5.74667 7.262 5.882 7.32267 5.994 7.444C6.11533 7.556 6.176 7.69133 6.176 7.85C6.176 8.00867 6.11533 8.14867 5.994 8.27C5.882 8.382 5.74667 8.438 5.588 8.438H3.25C3.09133 8.438 2.95133 8.382 2.83 8.27C2.718 8.14867 2.662 8.00867 2.662 7.85C2.662 7.69133 2.718 7.556 2.83 7.444C2.95133 7.32267 3.09133 7.262 3.25 7.262H5.588ZM7.912 1.438C8.22933 1.438 8.51867 1.51733 8.78 1.676C9.05067 1.83467 9.26533 2.04933 9.424 2.32C9.58267 2.58133 9.662 2.87067 9.662 3.188V10.188C9.662 10.5053 9.58267 10.7947 9.424 11.056C9.26533 11.3267 9.05067 11.5413 8.78 11.7C8.51867 11.8587 8.22933 11.938 7.912 11.938H2.088C1.77067 11.938 1.47667 11.8587 1.206 11.7C0.944667 11.5413 0.734667 11.3267 0.576 11.056C0.417334 10.7947 0.338 10.5053 0.338 10.188V3.188C0.338 2.87067 0.417334 2.58133 0.576 2.32C0.734667 2.04933 0.944667 1.83467 1.206 1.676C1.47667 1.51733 1.77067 1.438 2.088 1.438H2.774C2.89533 1.09267 3.10533 0.812666 3.404 0.598C3.70267 0.374 4.03867 0.262 4.412 0.262H5.588C5.96133 0.262 6.29733 0.374 6.596 0.598C6.89467 0.812666 7.10467 1.09267 7.226 1.438H7.912ZM3.838 2.012V2.6H6.162V2.012C6.162 1.85333 6.106 1.718 5.994 1.606C5.882 1.494 5.74667 1.438 5.588 1.438H4.412C4.25333 1.438 4.118 1.494 4.006 1.606C3.894 1.718 3.838 1.85333 3.838 2.012ZM8.5 10.188V3.188C8.5 3.02933 8.43933 2.894 8.318 2.782C8.206 2.66067 8.07067 2.6 7.912 2.6H7.338V3.188C7.338 3.34667 7.27733 3.482 7.156 3.594C7.044 3.706 6.90867 3.762 6.75 3.762H3.25C3.09133 3.762 2.95133 3.706 2.83 3.594C2.718 3.482 2.662 3.34667 2.662 3.188V2.6H2.088C1.92933 2.6 1.78933 2.66067 1.668 2.782C1.556 2.894 1.5 3.02933 1.5 3.188V10.188C1.5 10.3467 1.556 10.482 1.668 10.594C1.78933 10.706 1.92933 10.762 2.088 10.762H7.912C8.07067 10.762 8.206 10.706 8.318 10.594C8.43933 10.482 8.5 10.3467 8.5 10.188ZM6.75 4.938C6.89 4.95667 7.00667 5.022 7.1 5.134C7.20267 5.246 7.254 5.37667 7.254 5.526C7.254 5.666 7.20267 5.792 7.1 5.904C7.00667 6.016 6.89 6.08133 6.75 6.1H3.25C3.11 6.08133 2.98867 6.016 2.886 5.904C2.79267 5.792 2.746 5.666 2.746 5.526C2.746 5.37667 2.79267 5.246 2.886 5.134C2.98867 5.022 3.11 4.95667 3.25 4.938H6.75Z" />
                                </svg>
                            </span>  
                        </button>
                        <button className='hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2'>
                            <span>Discovery Audit</span>
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 15" fill="currentColor" className="h-4 w-4 shrink-0">
                                    <path d="M5.588 7.262C5.74667 7.262 5.882 7.32267 5.994 7.444C6.11533 7.556 6.176 7.69133 6.176 7.85C6.176 8.00867 6.11533 8.14867 5.994 8.27C5.882 8.382 5.74667 8.438 5.588 8.438H3.25C3.09133 8.438 2.95133 8.382 2.83 8.27C2.718 8.14867 2.662 8.00867 2.662 7.85C2.662 7.69133 2.718 7.556 2.83 7.444C2.95133 7.32267 3.09133 7.262 3.25 7.262H5.588ZM7.912 1.438C8.22933 1.438 8.51867 1.51733 8.78 1.676C9.05067 1.83467 9.26533 2.04933 9.424 2.32C9.58267 2.58133 9.662 2.87067 9.662 3.188V10.188C9.662 10.5053 9.58267 10.7947 9.424 11.056C9.26533 11.3267 9.05067 11.5413 8.78 11.7C8.51867 11.8587 8.22933 11.938 7.912 11.938H2.088C1.77067 11.938 1.47667 11.8587 1.206 11.7C0.944667 11.5413 0.734667 11.3267 0.576 11.056C0.417334 10.7947 0.338 10.5053 0.338 10.188V3.188C0.338 2.87067 0.417334 2.58133 0.576 2.32C0.734667 2.04933 0.944667 1.83467 1.206 1.676C1.47667 1.51733 1.77067 1.438 2.088 1.438H2.774C2.89533 1.09267 3.10533 0.812666 3.404 0.598C3.70267 0.374 4.03867 0.262 4.412 0.262H5.588C5.96133 0.262 6.29733 0.374 6.596 0.598C6.89467 0.812666 7.10467 1.09267 7.226 1.438H7.912ZM3.838 2.012V2.6H6.162V2.012C6.162 1.85333 6.106 1.718 5.994 1.606C5.882 1.494 5.74667 1.438 5.588 1.438H4.412C4.25333 1.438 4.118 1.494 4.006 1.606C3.894 1.718 3.838 1.85333 3.838 2.012ZM8.5 10.188V3.188C8.5 3.02933 8.43933 2.894 8.318 2.782C8.206 2.66067 8.07067 2.6 7.912 2.6H7.338V3.188C7.338 3.34667 7.27733 3.482 7.156 3.594C7.044 3.706 6.90867 3.762 6.75 3.762H3.25C3.09133 3.762 2.95133 3.706 2.83 3.594C2.718 3.482 2.662 3.34667 2.662 3.188V2.6H2.088C1.92933 2.6 1.78933 2.66067 1.668 2.782C1.556 2.894 1.5 3.02933 1.5 3.188V10.188C1.5 10.3467 1.556 10.482 1.668 10.594C1.78933 10.706 1.92933 10.762 2.088 10.762H7.912C8.07067 10.762 8.206 10.706 8.318 10.594C8.43933 10.482 8.5 10.3467 8.5 10.188ZM6.75 4.938C6.89 4.95667 7.00667 5.022 7.1 5.134C7.20267 5.246 7.254 5.37667 7.254 5.526C7.254 5.666 7.20267 5.792 7.1 5.904C7.00667 6.016 6.89 6.08133 6.75 6.1H3.25C3.11 6.08133 2.98867 6.016 2.886 5.904C2.79267 5.792 2.746 5.666 2.746 5.526C2.746 5.37667 2.79267 5.246 2.886 5.134C2.98867 5.022 3.11 4.95667 3.25 4.938H6.75Z" />
                                </svg>
                            </span>  
                        </button>
                    </div>

                    <div className="px-4 sm:px-6 lg:px-8 py-5">
                        <hr />
                    </div>  

                    <div className="px-4 sm:px-6 lg:px-8 py-3">
                        <div className="grid grid-cols-[30%_70%] max-w-lg mb-7">
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Type
                            </label>
                            <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full">
                                <select
                                className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                >
                                    <option>Windows</option>
                                    <option>Linux/Mac OS</option>
                                </select>
                            </div>
                        </div>
                        <div className="grid grid-cols-[30%_70%] max-w-lg mb-7 gap-y-2">
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Server Name
                            </label>
                            <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                <input
                                    type="text"
                                    autoComplete=""
                                    className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                    placeholder=""
                                />
                            </div>
                            <label htmlFor="">&nbsp;</label>
                            <div className="relative flex items-start max-w-xs w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <p id="comments-description" className="text-gray-500 text-xs flex">
                                        <span>Use service account credentials for authentication </span>
                                        <a href="#" data-tooltip-target="tooltip-default">
                                            <svg width="14" height="15" viewBox="0 0 14 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M6.52 9.592C6.648 9.45333 6.808 9.384 7 9.384C7.192 9.384 7.352 9.45333 7.48 9.592C7.608 9.72 7.672 9.88 7.672 10.072C7.672 10.1573 7.656 10.2373 7.624 10.312C7.56 10.4933 7.44267 10.6213 7.272 10.696C7.10133 10.76 6.92533 10.76 6.744 10.696C6.57333 10.6213 6.456 10.4933 6.392 10.312C6.36 10.2373 6.344 10.1573 6.344 10.072C6.344 10.0187 6.344 9.97067 6.344 9.928C6.35467 9.88533 6.36533 9.848 6.376 9.816C6.39733 9.77333 6.41867 9.736 6.44 9.704C6.46133 9.66133 6.488 9.624 6.52 9.592ZM7 0.728C8.20533 0.728 9.32533 1.032 10.36 1.64C11.3733 2.22667 12.1733 3.02667 12.76 4.04C13.368 5.07467 13.672 6.19467 13.672 7.4C13.672 8.60533 13.368 9.72533 12.76 10.76C12.1733 11.7733 11.3733 12.5733 10.36 13.16C9.32533 13.768 8.20533 14.072 7 14.072C5.79467 14.072 4.67467 13.768 3.64 13.16C2.62667 12.5733 1.82667 11.7733 1.24 10.76C0.632 9.72533 0.328 8.60533 0.328 7.4C0.328 6.19467 0.632 5.07467 1.24 4.04C1.82667 3.02667 2.62667 2.22667 3.64 1.64C4.67467 1.032 5.79467 0.728 7 0.728ZM7 12.728C7.96 12.728 8.856 12.488 9.688 12.008C10.488 11.528 11.128 10.888 11.608 10.088C12.088 9.256 12.328 8.36 12.328 7.4C12.328 6.44 12.088 5.544 11.608 4.712C11.128 3.912 10.488 3.272 9.688 2.792C8.856 2.312 7.96 2.072 7 2.072C6.04 2.072 5.144 2.312 4.312 2.792C3.512 3.272 2.872 3.912 2.392 4.712C1.912 5.544 1.672 6.44 1.672 7.4C1.672 8.36 1.912 9.256 2.392 10.088C2.872 10.888 3.512 11.528 4.312 12.008C5.144 12.488 6.04 12.728 7 12.728ZM7 4.072C7.48 4.08267 7.90133 4.24267 8.264 4.552C8.62667 4.86133 8.85067 5.256 8.936 5.736C9.02133 6.20533 8.94133 6.648 8.696 7.064C8.46133 7.48 8.12 7.77333 7.672 7.944V8.072C7.672 8.25333 7.60267 8.41333 7.464 8.552C7.336 8.68 7.18133 8.744 7 8.744C6.81867 8.744 6.65867 8.68 6.52 8.552C6.392 8.41333 6.328 8.25333 6.328 8.072V7.4C6.328 7.21867 6.392 7.064 6.52 6.936C6.65867 6.79733 6.81867 6.728 7 6.728C7.16 6.70667 7.29333 6.632 7.4 6.504C7.51733 6.376 7.576 6.232 7.576 6.072C7.576 5.90133 7.51733 5.752 7.4 5.624C7.29333 5.496 7.16 5.42133 7 5.4C6.88267 5.4 6.77067 5.432 6.664 5.496C6.568 5.54933 6.488 5.63467 6.424 5.752C6.37067 5.85867 6.29067 5.94933 6.184 6.024C6.07733 6.09867 5.96 6.14133 5.832 6.152C5.704 6.152 5.58133 6.12 5.464 6.056C5.35733 5.992 5.272 5.90133 5.208 5.784C5.144 5.66667 5.11733 5.544 5.128 5.416C5.14933 5.288 5.19733 5.17067 5.272 5.064C5.45333 4.75467 5.69867 4.51467 6.008 4.344C6.31733 4.16267 6.648 4.072 7 4.072Z" fill="#4F5B67"/>
                                            </svg>
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div className="grid grid-cols-[30%_70%] max-w-lg mb-7 gap-y-2">
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Path
                            </label>
                            <div className='flex'>
                                <div className="rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full">
                                    <input
                                        type="text"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6 w-full"
                                        placeholder=""
                                    />
                                </div>
                                <a href="#" className='text-xs m-2 w-[200px]'>Discover certificate list</a>
                            </div>
                        </div>
                       
                    </div>

                                             
                    <div className="px-4 sm:px-6 lg:px-8 py-5">
                        <hr />
                    </div>     

                    <div className="px-4 sm:px-6 lg:px-8 py-3">
                        <button className='hover:shadow-lg transition duration-300 ease-in-out bg-black text-white hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center'>
                            <span>Discover</span>
                        </button>
                    </div>
                    <div className="px-4 sm:px-6 lg:px-8 py-5">
                        <hr />
                    </div>        

                    <HelpBox helpTxt={helpTxt}/>                          
                </main>
            </div>  
        </div>
        
      </div> 
    )
}