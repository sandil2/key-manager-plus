import { Link, useOutletContext } from 'react-router-dom'
import Header from '../Header'
import HelpBox from '../HelpBox';
import { useState } from 'react';

const helpTxt = [
    "The MDM integration needs a valid API key with the necessary permissions. To generate the API server key, follow the below steps in the MDM portal: <br/> Go to Admin and click Generate Key under API Key Generation. <br/> In the pop-up, choose Third-party App as the Application, enter a Service Name, and under Permissions, select the following: CertMgmt (read and write), Devices (read and write), Profiles (read and write). <br/> Click Generate Key. Once the new API key is generated, use the copy icon to copy it.",
    "To complete the integration, specify the URL of the MDM server and paste the newly generated API Key.",
    "Click Import From MDM to start importing SSL certificates from the MDM server.",
    "Click Discovery Audit to view the status of the import.",
    "Note: MDM integration works with MDM release version 10.1.2011.02 and above only.",
];


export default function AWS(){

    const [sidebarOpen, setSidebarOpen, setSidebarShrink, sidebarShrink] = useOutletContext();

    const options = [
        {value: '', text: '---Select---'},
        {value: 'agent1', text: 'Agent1'},
        {value: 'agent2', text: 'Agent2'},
    ];

    const [selected, setSelected] = useState(options[0].value);
    
    const handleChange = event => {
        console.log(event.target.value);
        setSelected(event.target.value);
    };

    return (
        <div>
        <div className={`${sidebarShrink ? "lg:pl-72" : "lg:pl-24" } bg-theme-gray2`}>
            <div className='bg-theme-gray2'>
                <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
            </div>
            <div className='bg-white'>
                <main className="py-10">
                    <div className="px-4 sm:px-6 lg:px-8 flex justify-between items-center">
                        <h1 className='text-xl'>AWS Discovery</h1>
                        <button className='hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2'>
                        <span>Discovery Audit</span>
                        <span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 15" fill="currentColor" className="h-4 w-4 shrink-0">
                                <path d="M5.588 7.262C5.74667 7.262 5.882 7.32267 5.994 7.444C6.11533 7.556 6.176 7.69133 6.176 7.85C6.176 8.00867 6.11533 8.14867 5.994 8.27C5.882 8.382 5.74667 8.438 5.588 8.438H3.25C3.09133 8.438 2.95133 8.382 2.83 8.27C2.718 8.14867 2.662 8.00867 2.662 7.85C2.662 7.69133 2.718 7.556 2.83 7.444C2.95133 7.32267 3.09133 7.262 3.25 7.262H5.588ZM7.912 1.438C8.22933 1.438 8.51867 1.51733 8.78 1.676C9.05067 1.83467 9.26533 2.04933 9.424 2.32C9.58267 2.58133 9.662 2.87067 9.662 3.188V10.188C9.662 10.5053 9.58267 10.7947 9.424 11.056C9.26533 11.3267 9.05067 11.5413 8.78 11.7C8.51867 11.8587 8.22933 11.938 7.912 11.938H2.088C1.77067 11.938 1.47667 11.8587 1.206 11.7C0.944667 11.5413 0.734667 11.3267 0.576 11.056C0.417334 10.7947 0.338 10.5053 0.338 10.188V3.188C0.338 2.87067 0.417334 2.58133 0.576 2.32C0.734667 2.04933 0.944667 1.83467 1.206 1.676C1.47667 1.51733 1.77067 1.438 2.088 1.438H2.774C2.89533 1.09267 3.10533 0.812666 3.404 0.598C3.70267 0.374 4.03867 0.262 4.412 0.262H5.588C5.96133 0.262 6.29733 0.374 6.596 0.598C6.89467 0.812666 7.10467 1.09267 7.226 1.438H7.912ZM3.838 2.012V2.6H6.162V2.012C6.162 1.85333 6.106 1.718 5.994 1.606C5.882 1.494 5.74667 1.438 5.588 1.438H4.412C4.25333 1.438 4.118 1.494 4.006 1.606C3.894 1.718 3.838 1.85333 3.838 2.012ZM8.5 10.188V3.188C8.5 3.02933 8.43933 2.894 8.318 2.782C8.206 2.66067 8.07067 2.6 7.912 2.6H7.338V3.188C7.338 3.34667 7.27733 3.482 7.156 3.594C7.044 3.706 6.90867 3.762 6.75 3.762H3.25C3.09133 3.762 2.95133 3.706 2.83 3.594C2.718 3.482 2.662 3.34667 2.662 3.188V2.6H2.088C1.92933 2.6 1.78933 2.66067 1.668 2.782C1.556 2.894 1.5 3.02933 1.5 3.188V10.188C1.5 10.3467 1.556 10.482 1.668 10.594C1.78933 10.706 1.92933 10.762 2.088 10.762H7.912C8.07067 10.762 8.206 10.706 8.318 10.594C8.43933 10.482 8.5 10.3467 8.5 10.188ZM6.75 4.938C6.89 4.95667 7.00667 5.022 7.1 5.134C7.20267 5.246 7.254 5.37667 7.254 5.526C7.254 5.666 7.20267 5.792 7.1 5.904C7.00667 6.016 6.89 6.08133 6.75 6.1H3.25C3.11 6.08133 2.98867 6.016 2.886 5.904C2.79267 5.792 2.746 5.666 2.746 5.526C2.746 5.37667 2.79267 5.246 2.886 5.134C2.98867 5.022 3.11 4.95667 3.25 4.938H6.75Z" />
                            </svg>
                        </span>  
                        </button>
                    </div>
                    <div className="px-4 sm:px-6 lg:px-8 py-5">
                        <hr />
                    </div>  
                    <div className="px-4 sm:px-6 lg:px-8 py-3">
                        <div className="grid grid-cols-[30%_70%] max-w-2xl gap-y-3 items-center">
                            <label htmlFor="">Credential Name</label>
                            <div className="relative flex items-start w-full items-center">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full max-w-sm">
                                     <select value={selected} onChange={handleChange} className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                        {options.map(option => (
                                        <option key={option.value} value={option.value}>
                                            {option.text}
                                        </option>
                                        ))}
                                    </select>
                                </div>
                                <div className="ml-3 text-sm leading-6 w-60">
                                    <Link to="/" className='hover:text-theme-red1 underline'>Add Azure credential </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="px-4 sm:px-6 lg:px-8 py-3">
                        <div className="grid grid-cols-[30%_70%] max-w-2xl gap-y-3 items-center">
                            <label htmlFor="">AWS Service</label>
                            <div className="relative flex items-start w-full items-center">
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full max-w-sm">
                                     <select value={selected} onChange={handleChange} className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6">
                                        {options.map(option => (
                                        <option key={option.value} value={option.value}>
                                            {option.text}
                                        </option>
                                        ))}
                                    </select>
                                </div>
                                <div className="ml-3 text-sm leading-6 w-60">
                                    &nbsp;
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="px-4 sm:px-6 lg:px-8 py-3">
                        <div className="grid grid-cols-[30%_70%] max-w-2xl gap-y-3 items-center">
                            <label htmlFor="">Region</label>
                            <div className="relative flex items-start w-full items-center">
                                <div className="flex rounded-md overflow-hidden ring-1 ring-inset ring-gray-300 w-full max-w-sm border">
                                    <div id="dropdownSearch" className="z-10 bg-white w-full">
                                        <div className="p-3">
                                        <label htmlFor="input-group-search" className="sr-only">Search</label>
                                        <div className="relative">
                                            <div className="absolute inset-y-0 start-0 flex items-center ps-3 pointer-events-none">
                                            <svg className="w-4 h-4 text-gray-500" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                                                <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"/>
                                            </svg>
                                            </div>
                                            <input type="text" id="input-group-search" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full ps-10 p-2.5" placeholder="Search user"/>
                                        </div>
                                        </div>
                                        <ul className="h-48 px-3 pb-3 overflow-y-auto text-sm text-gray-700" aria-labelledby="dropdownSearchButton">
                                            <li>
                                                <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                                <input id="checkbox-item-1" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                                <label htmlFor="checkbox-item-1" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Select All</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                                <input id="checkbox-item-2" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                                <label htmlFor="checkbox-item-2" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Mumbai)</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                                <input id="checkbox-item-3" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                                <label htmlFor="checkbox-item-3" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (Milan) </label>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                                <input id="checkbox-item-4" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                                <label htmlFor="checkbox-item-4" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">AWS GovCloud (US-East)</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                                <input id="checkbox-item-5" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                                <label htmlFor="checkbox-item-5" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Canada (Central)</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                                <input id="checkbox-item-6" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                                <label htmlFor="checkbox-item-6" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (Frankfurt) </label>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                                <input id="checkbox-item-7" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                                <label htmlFor="checkbox-item-7" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US ISO West</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                                <input id="checkbox-item-8" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                                <label htmlFor="checkbox-item-8" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US West (N. California) </label>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                                <input id="checkbox-item-9" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                                <label htmlFor="checkbox-item-9" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US West (Oregon) </label>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                                <input id="checkbox-item-10" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                                <label htmlFor="checkbox-item-10" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Africa (Cape Town) </label>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                                <input id="checkbox-item-11" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                                <label htmlFor="checkbox-item-11" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (Paris) </label>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                                <input id="checkbox-item-12" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                                <label htmlFor="checkbox-item-12" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (Stockholm) </label>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                                <input id="checkbox-item-13" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                                <label htmlFor="checkbox-item-13" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (London)</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                                <input id="checkbox-item-14" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                                <label htmlFor="checkbox-item-14" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">EU (Ireland)</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                                <input id="checkbox-item-15" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                                <label htmlFor="checkbox-item-15" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Osaka) </label>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                                <input id="checkbox-item-16" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                                <label htmlFor="checkbox-item-16" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Seoul)</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                                <input id="checkbox-item-17" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                                <label htmlFor="checkbox-item-17" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Tokyo)</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                                <input id="checkbox-item-18" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                                <label htmlFor="checkbox-item-18" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Middle East (Bahrain)</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                                <input id="checkbox-item-19" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                                <label htmlFor="checkbox-item-19" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">South America (Sao Paulo)</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                                <input id="checkbox-item-20" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                                <label htmlFor="checkbox-item-20" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Hong Kong)</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                                <input id="checkbox-item-21" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                                <label htmlFor="checkbox-item-21" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">China (Beijing)</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                                <input id="checkbox-item-22" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                                <label htmlFor="checkbox-item-22" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">AWS GovCloud (US)</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                                <input id="checkbox-item-23" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                                <label htmlFor="checkbox-item-23" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Singapore) </label>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                                <input id="checkbox-item-24" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                                <label htmlFor="checkbox-item-24" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">Asia Pacific (Sydney)</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                                <input id="checkbox-item-25" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                                <label htmlFor="checkbox-item-25" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US ISO East </label>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                                <input id="checkbox-item-26" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                                <label htmlFor="checkbox-item-26" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US East (N. Virginia)</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                                <input id="checkbox-item-27" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                                <label htmlFor="checkbox-item-27" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US East (Ohio)</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                                <input id="checkbox-item-28" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                                <label htmlFor="checkbox-item-28" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">China (Ningxia)</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="flex items-center p-2 rounded hover:bg-gray-100">
                                                <input id="checkbox-item-29" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 focus:ring-2"/>
                                                <label htmlFor="checkbox-item-29" className="w-full ms-2 text-sm font-medium text-gray-900 rounded">US ISOB East (Ohio) </label>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>

                                </div>
                                <div className="ml-3 text-sm leading-6 w-60">
                                    &nbsp;
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="px-4 sm:px-6 lg:px-8 py-3">
                        <div className="grid grid-cols-[30%_70%] max-w-2xl gap-y-3 items-center">
                            <label htmlFor="">&nbsp;</label>
                            <div className="relative flex items-start max-w-xs w-full">
                                <div className="flex h-6 items-center">
                                    <input
                                    id="comments"
                                    aria-describedby="comments-description"
                                    name="comments"
                                    type="checkbox"
                                    className="accent-theme-red1 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                    />
                                </div>
                                <div className="ml-3 text-sm leading-6">
                                    <span>Import Previous Versions </span>
                                </div>
                            </div>
                        </div>
                    </div>
                                       
                    <div className="px-4 sm:px-6 lg:px-8 py-5">
                        <hr />
                    </div>                                
                    <div className="px-4 sm:px-6 lg:px-8 py-3">
                        <button className='hover:shadow-lg transition duration-300 ease-in-out bg-black text-white hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center'>
                            <span>Import</span>
                            {/* <span>
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 15" fill="currentColor" className="h-4 w-4 shrink-0">
                                <path d="M8.912 3.188C9.052 3.20667 9.16867 3.272 9.262 3.384C9.36467 3.496 9.416 3.62667 9.416 3.776C9.416 3.916 9.36467 4.042 9.262 4.154C9.16867 4.266 9.052 4.33133 8.912 4.35H3.088C2.948 4.33133 2.82667 4.266 2.724 4.154C2.63067 4.042 2.584 3.916 2.584 3.776C2.584 3.62667 2.63067 3.496 2.724 3.384C2.82667 3.272 2.948 3.20667 3.088 3.188H8.912ZM8.912 5.512C9.07067 5.512 9.206 5.57267 9.318 5.694C9.43933 5.806 9.5 5.94133 9.5 6.1C9.5 6.25867 9.43933 6.39867 9.318 6.52C9.206 6.632 9.07067 6.688 8.912 6.688H3.088C2.92933 6.688 2.78933 6.632 2.668 6.52C2.556 6.39867 2.5 6.25867 2.5 6.1C2.5 5.94133 2.556 5.806 2.668 5.694C2.78933 5.57267 2.92933 5.512 3.088 5.512H8.912ZM10.088 0.262C10.4053 0.262 10.6947 0.341333 10.956 0.499999C11.2267 0.658666 11.4413 0.873333 11.6 1.144C11.7587 1.40533 11.838 1.69467 11.838 2.012V11.35C11.838 11.4713 11.8053 11.5787 11.74 11.672C11.6747 11.7653 11.586 11.8353 11.474 11.882C11.3993 11.9193 11.3247 11.938 11.25 11.938C11.082 11.938 10.942 11.882 10.83 11.77L8.674 9.6H1.912C1.59467 9.6 1.30067 9.52067 1.03 9.362C0.768667 9.20333 0.558667 8.99333 0.4 8.732C0.241333 8.46133 0.162 8.16733 0.162 7.85V2.012C0.162 1.69467 0.241333 1.40533 0.4 1.144C0.558667 0.873333 0.768667 0.658666 1.03 0.499999C1.30067 0.341333 1.59467 0.262 1.912 0.262H10.088ZM10.662 9.95V2.012C10.662 1.85333 10.606 1.718 10.494 1.606C10.382 1.494 10.2467 1.438 10.088 1.438H1.912C1.75333 1.438 1.618 1.494 1.506 1.606C1.394 1.718 1.338 1.85333 1.338 2.012V7.85C1.338 8.00867 1.394 8.14867 1.506 8.27C1.618 8.382 1.75333 8.438 1.912 8.438H8.912C9.08 8.438 9.22 8.494 9.332 8.606L10.662 9.95Z" />
                                </svg>
                            </span>   */}
                        </button>
                    </div>

                    <HelpBox helpTxt={helpTxt}/>
                    
                </main>
            </div>  
        </div>
        
      </div> 
    )
}