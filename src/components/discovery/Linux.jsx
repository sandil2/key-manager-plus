import { useOutletContext } from 'react-router-dom'
import Header from '../Header'
import HelpBox from '../HelpBox';
import { useState } from 'react';

export default function Linux(){
    const [sidebarOpen, setSidebarOpen, setSidebarShrink, sidebarShrink] = useOutletContext();
    const [password, setPassword] = useState('password');

    const helpTxt = [
        "Specify the server name, port number, user credentials, and the path of the load balancer from which certificates have to be discovered. currently supports certificate discovery from Nginx and F5 load balancers.",
        "Choose the Select Key option to access password-less resources through key based authentication. Upload the private key associated with the required account and specify the key passphrase.",
        "Discover certificate list fetches all the certificates available in the specified path and allows you to choose certificates that need to be imported.",
        "Certificate files with extensions .keystore and .pfx are grouped separately under JKS / PKCS. <br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To import these certificates, click JKS / PKCS, choose the certificate files that you wish to import, provide the file passphrase and click Import.",
    ]

    return (
        <div>
        <div className={`${sidebarShrink ? "lg:pl-72" : "lg:pl-24" } bg-theme-gray2`}>
            <div className='bg-theme-gray2'>
                <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
            </div>
            <div className='bg-white'>
                <main className="py-10">
                    <div className="px-4 sm:px-6 lg:px-8 flex items-center gap-5">
                        <h1 className='text-xl'>Load Balancer Certificate Discovery</h1>
                        <button className='hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2 ml-auto'>
                            <span>JKS/PKCS</span>
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 15" fill="currentColor" className="h-4 w-4 shrink-0">
                                    <path d="M5.588 7.262C5.74667 7.262 5.882 7.32267 5.994 7.444C6.11533 7.556 6.176 7.69133 6.176 7.85C6.176 8.00867 6.11533 8.14867 5.994 8.27C5.882 8.382 5.74667 8.438 5.588 8.438H3.25C3.09133 8.438 2.95133 8.382 2.83 8.27C2.718 8.14867 2.662 8.00867 2.662 7.85C2.662 7.69133 2.718 7.556 2.83 7.444C2.95133 7.32267 3.09133 7.262 3.25 7.262H5.588ZM7.912 1.438C8.22933 1.438 8.51867 1.51733 8.78 1.676C9.05067 1.83467 9.26533 2.04933 9.424 2.32C9.58267 2.58133 9.662 2.87067 9.662 3.188V10.188C9.662 10.5053 9.58267 10.7947 9.424 11.056C9.26533 11.3267 9.05067 11.5413 8.78 11.7C8.51867 11.8587 8.22933 11.938 7.912 11.938H2.088C1.77067 11.938 1.47667 11.8587 1.206 11.7C0.944667 11.5413 0.734667 11.3267 0.576 11.056C0.417334 10.7947 0.338 10.5053 0.338 10.188V3.188C0.338 2.87067 0.417334 2.58133 0.576 2.32C0.734667 2.04933 0.944667 1.83467 1.206 1.676C1.47667 1.51733 1.77067 1.438 2.088 1.438H2.774C2.89533 1.09267 3.10533 0.812666 3.404 0.598C3.70267 0.374 4.03867 0.262 4.412 0.262H5.588C5.96133 0.262 6.29733 0.374 6.596 0.598C6.89467 0.812666 7.10467 1.09267 7.226 1.438H7.912ZM3.838 2.012V2.6H6.162V2.012C6.162 1.85333 6.106 1.718 5.994 1.606C5.882 1.494 5.74667 1.438 5.588 1.438H4.412C4.25333 1.438 4.118 1.494 4.006 1.606C3.894 1.718 3.838 1.85333 3.838 2.012ZM8.5 10.188V3.188C8.5 3.02933 8.43933 2.894 8.318 2.782C8.206 2.66067 8.07067 2.6 7.912 2.6H7.338V3.188C7.338 3.34667 7.27733 3.482 7.156 3.594C7.044 3.706 6.90867 3.762 6.75 3.762H3.25C3.09133 3.762 2.95133 3.706 2.83 3.594C2.718 3.482 2.662 3.34667 2.662 3.188V2.6H2.088C1.92933 2.6 1.78933 2.66067 1.668 2.782C1.556 2.894 1.5 3.02933 1.5 3.188V10.188C1.5 10.3467 1.556 10.482 1.668 10.594C1.78933 10.706 1.92933 10.762 2.088 10.762H7.912C8.07067 10.762 8.206 10.706 8.318 10.594C8.43933 10.482 8.5 10.3467 8.5 10.188ZM6.75 4.938C6.89 4.95667 7.00667 5.022 7.1 5.134C7.20267 5.246 7.254 5.37667 7.254 5.526C7.254 5.666 7.20267 5.792 7.1 5.904C7.00667 6.016 6.89 6.08133 6.75 6.1H3.25C3.11 6.08133 2.98867 6.016 2.886 5.904C2.79267 5.792 2.746 5.666 2.746 5.526C2.746 5.37667 2.79267 5.246 2.886 5.134C2.98867 5.022 3.11 4.95667 3.25 4.938H6.75Z" />
                                </svg>
                            </span>  
                        </button>
                        <button className='hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2'>
                            <span>Discovery Audit</span>
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 15" fill="currentColor" className="h-4 w-4 shrink-0">
                                    <path d="M5.588 7.262C5.74667 7.262 5.882 7.32267 5.994 7.444C6.11533 7.556 6.176 7.69133 6.176 7.85C6.176 8.00867 6.11533 8.14867 5.994 8.27C5.882 8.382 5.74667 8.438 5.588 8.438H3.25C3.09133 8.438 2.95133 8.382 2.83 8.27C2.718 8.14867 2.662 8.00867 2.662 7.85C2.662 7.69133 2.718 7.556 2.83 7.444C2.95133 7.32267 3.09133 7.262 3.25 7.262H5.588ZM7.912 1.438C8.22933 1.438 8.51867 1.51733 8.78 1.676C9.05067 1.83467 9.26533 2.04933 9.424 2.32C9.58267 2.58133 9.662 2.87067 9.662 3.188V10.188C9.662 10.5053 9.58267 10.7947 9.424 11.056C9.26533 11.3267 9.05067 11.5413 8.78 11.7C8.51867 11.8587 8.22933 11.938 7.912 11.938H2.088C1.77067 11.938 1.47667 11.8587 1.206 11.7C0.944667 11.5413 0.734667 11.3267 0.576 11.056C0.417334 10.7947 0.338 10.5053 0.338 10.188V3.188C0.338 2.87067 0.417334 2.58133 0.576 2.32C0.734667 2.04933 0.944667 1.83467 1.206 1.676C1.47667 1.51733 1.77067 1.438 2.088 1.438H2.774C2.89533 1.09267 3.10533 0.812666 3.404 0.598C3.70267 0.374 4.03867 0.262 4.412 0.262H5.588C5.96133 0.262 6.29733 0.374 6.596 0.598C6.89467 0.812666 7.10467 1.09267 7.226 1.438H7.912ZM3.838 2.012V2.6H6.162V2.012C6.162 1.85333 6.106 1.718 5.994 1.606C5.882 1.494 5.74667 1.438 5.588 1.438H4.412C4.25333 1.438 4.118 1.494 4.006 1.606C3.894 1.718 3.838 1.85333 3.838 2.012ZM8.5 10.188V3.188C8.5 3.02933 8.43933 2.894 8.318 2.782C8.206 2.66067 8.07067 2.6 7.912 2.6H7.338V3.188C7.338 3.34667 7.27733 3.482 7.156 3.594C7.044 3.706 6.90867 3.762 6.75 3.762H3.25C3.09133 3.762 2.95133 3.706 2.83 3.594C2.718 3.482 2.662 3.34667 2.662 3.188V2.6H2.088C1.92933 2.6 1.78933 2.66067 1.668 2.782C1.556 2.894 1.5 3.02933 1.5 3.188V10.188C1.5 10.3467 1.556 10.482 1.668 10.594C1.78933 10.706 1.92933 10.762 2.088 10.762H7.912C8.07067 10.762 8.206 10.706 8.318 10.594C8.43933 10.482 8.5 10.3467 8.5 10.188ZM6.75 4.938C6.89 4.95667 7.00667 5.022 7.1 5.134C7.20267 5.246 7.254 5.37667 7.254 5.526C7.254 5.666 7.20267 5.792 7.1 5.904C7.00667 6.016 6.89 6.08133 6.75 6.1H3.25C3.11 6.08133 2.98867 6.016 2.886 5.904C2.79267 5.792 2.746 5.666 2.746 5.526C2.746 5.37667 2.79267 5.246 2.886 5.134C2.98867 5.022 3.11 4.95667 3.25 4.938H6.75Z" />
                                </svg>
                            </span>  
                        </button>
                    </div>

                    <div className="px-4 sm:px-6 lg:px-8 py-5">
                        <hr />
                    </div>  

                    <div className="px-4 sm:px-6 lg:px-8 py-3">
                        <div className="grid grid-cols-[30%_70%] max-w-lg mb-7">
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Type
                            </label>
                            <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full">
                                <select
                                className="block flex-1 border-0 bg-transparent py-2 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                >
                                    <option>General</option>
                                    <option>BIG-IP F5</option>
                                    <option>Citrix</option>
                                    <option>Fortigate Firewall</option>
                                </select>
                            </div>
                        </div>
                        <div className="grid grid-cols-[30%_70%] max-w-lg mb-7">
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Server Name
                            </label>
                            <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                <input
                                    type="text"
                                    autoComplete=""
                                    className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                    placeholder=""
                                />
                            </div>
                        </div>
                        <div className="grid grid-cols-[30%_70%] max-w-lg mb-7">
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Port
                            </label>
                            <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                <input
                                    type="text"
                                    autoComplete=""
                                    className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                    placeholder=""
                                />
                            </div>
                        </div>
                        <div className="grid grid-cols-[30%_70%] max-w-lg mb-7">
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                User Name
                            </label>
                            <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                <input
                                    type="text"
                                    autoComplete=""
                                    className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                    placeholder=""
                                />
                            </div>
                        </div>
                        <div className="grid grid-cols-[30%_70%] max-w-lg mb-7">
                            <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                Credential Type
                            </label>
                            <div className="flex max-w-md w-full">
                                <legend className="sr-only">Credential Type</legend>
                                <div className="space-y-4 sm:flex sm:items-center sm:space-x-10 sm:space-y-0">
                                    <div className="flex items-center">
                                        <input
                                            id="password"
                                            name="password"
                                            type="radio"
                                            defaultChecked={password === 'password'}
                                            value="password"
                                            onClick={() => setPassword('password')}
                                            className="accent-theme-red1 h-4 w-4 border-gray-300"
                                        />
                                        <label htmlFor="password" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                            Password
                                        </label>
                                    </div>
                                    <div className="flex items-center">
                                        <input
                                            id="select-key"
                                            name="password"
                                            type="radio"
                                            defaultChecked={password === 'select-key'}
                                            value="select-key"
                                            onClick={() => setPassword('select-key')}
                                            className="accent-theme-red1 h-4 w-4 border-gray-300 text-theme-red1 focus:ring-theme-red1"
                                        />
                                        <label htmlFor="select-key" className="ml-3 block text-sm font-medium leading-6 text-gray-900">
                                            Select Key
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* password */}
                        {password == "password" &&
                            <div className="grid grid-cols-[30%_70%] max-w-lg gap-y-7">
                                <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                    Password
                                </label>
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="text"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>

                                <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                    Path
                                </label>
                                <div className='flex'>
                                    <div className="rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full">
                                        <input
                                            type="text"
                                            autoComplete=""
                                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6 w-full"
                                            placeholder=""
                                        />
                                    </div>
                                    <a href="#" className='text-xs m-2 w-[200px]'>Discover certificate list</a>
                                </div>
                            </div>
                        }
                        
                        {/* select key */}
                        {password == "select-key" &&
                            <div className="grid grid-cols-[30%_70%] max-w-lg gap-y-7">
                                <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                    User PrivateKey
                                </label>
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="file"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>
                                <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                    Passphrase
                                </label>
                                <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 max-w-md w-full">
                                    <input
                                        type="text"
                                        autoComplete=""
                                        className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                        placeholder=""
                                    />
                                </div>

                                <label htmlFor="ip-address-range" className="text-sm font-medium leading-6 text-gray-900 w-36">
                                    Path
                                </label>
                                <div className='flex'>
                                    <div className="rounded-md shadow-sm ring-1 ring-inset ring-gray-300 w-full">
                                        <input
                                            type="text"
                                            autoComplete=""
                                            className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6 w-full"
                                            placeholder=""
                                        />
                                    </div>
                                    <a href="#" className='text-xs m-2 w-[200px]'>Discover certificate list</a>
                                </div>
                            </div>
                        }
                    </div>


                                             
                    <div className="px-4 sm:px-6 lg:px-8 py-5">
                        <hr />
                    </div>     

                    <div className="px-4 sm:px-6 lg:px-8 py-3">
                        <button className='hover:shadow-lg transition duration-300 ease-in-out bg-black text-white hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center'>
                            <span>Discover</span>
                        </button>
                    </div>
                    <div className="px-4 sm:px-6 lg:px-8 py-5">
                        <hr />
                    </div>        

                    <HelpBox helpTxt={helpTxt}/>                          
                </main>
            </div>  
        </div>
        
      </div> 
    )
}