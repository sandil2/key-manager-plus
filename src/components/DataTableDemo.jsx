import React, { useMemo, useState } from 'react';
import DataTable from 'react-data-table-component';
import { KeyIcon, TrashIcon, BookOpenIcon,MagnifyingGlassIcon } from '@heroicons/react/24/outline';

export default function DataTableDemo(){
    const columns = [
        {
            name: 'Keys',
            selector: row => row.Key,
            sortable: false,
            maxWidth: "30px"
        },
        {
            name: 'Resource Name',
            selector: row => row.ResourceName,
            sortable: true,
        },
        {
            name: 'Description',
            selector: row => row.Description,
            sortable: true,
        },
        {
            name: 'IP Address',
            selector: row => row.IPAddress,
            sortable: true,
        },
        {
            name: 'Created Time',
            selector: row => row.CreatedTime,
            sortable: true,
        },
        {
            name: 'Landing Server',
            selector: row => row.LandingServer,
            sortable: true,
        },
    ];      

const data = [
    {
        ResourceName : "test_server101",
        Key : <KeyIcon className="w-6 h-6 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-"
    },
    {
        ResourceName : "test_server102",
        Key : <KeyIcon className="w-6 h-6 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-"
    },
    {
        ResourceName : "test_server103",
        Key : <KeyIcon className="w-6 h-6 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-"
    },
    {
        ResourceName : "test_server104",
        Key : <KeyIcon className="w-6 h-6 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-"
    },
    {
        ResourceName : "test_server105",
        Key : <KeyIcon className="w-6 h-6 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-"
    },
    {
        ResourceName : "test_server101",
        Key : <KeyIcon className="w-6 h-6 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-"
    },
    {
        ResourceName : "test_server102",
        Key : <KeyIcon className="w-6 h-6 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-"
    },
    {
        ResourceName : "test_server103",
        Key : <KeyIcon className="w-6 h-6 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-"
    },
    {
        ResourceName : "test_server104",
        Key : <KeyIcon className="w-6 h-6 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-"
    },
    {
        ResourceName : "test_server105",
        Key : <KeyIcon className="w-6 h-6 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-"
    },
    {
        ResourceName : "test_server101",
        Key : <KeyIcon className="w-6 h-6 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-"
    },
    {
        ResourceName : "test_server102",
        Key : <KeyIcon className="w-6 h-6 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-"
    },
    {
        ResourceName : "test_server103",
        Key : <KeyIcon className="w-6 h-6 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-"
    },
    {
        ResourceName : "test_server104",
        Key : <KeyIcon className="w-6 h-6 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-"
    },
    {
        ResourceName : "test_server105",
        Key : <KeyIcon className="w-6 h-6 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-"
    },
    {
        ResourceName : "sandil test",
        Key : <KeyIcon className="w-6 h-6 text-black-700"/>,
        Description : "NA",
        IPAddress :"192.168.15.25",
        CreatedTime : "Feb 9, 2017 15:52", 
        LandingServer: "-"
    }
];

const [records,setRecords] = useState(data);

function handleFilter(event){
    const newData = records.filter(row=>{
        return row.ResourceName.toLowerCase().includes(event.target.value.toLowerCase())
    })
    setRecords(newData)
}

	return (
        <>
            <div className='flex flex-wrap gap-3 my-4'>
                <button className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                    <span>Enumerate</span>
                </button>
                <button className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                    <span>Credentials</span>
                </button>
                <button className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                    <span>Associate Landing Server</span>
                </button>
                <button className="hover:shadow-lg transition text-red-600 duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2 ml-auto">
                    <span>Delete </span><TrashIcon className='w-4 h-4'/>
                </button>
                <button className="hover:shadow-lg transition duration-300 ease-in-out bg-white text-theme-gray3 hover:text-theme-red1 hover:bg-white rounded-full flex px-5 py-3 gap-3 items-center border border-theme-gray2">
                    <span>Associate Landing Server</span>
                    <BookOpenIcon className='w-4 h-4'/>
                </button>
            </div>
            <div className='max-w-[500px] w-full mb-5 block'>
                <div className="flex">
                    <label htmlFor="search-dropdown" className="mb-2 text-sm font-medium text-gray-900 sr-only">Your Email</label>
                    <div className="relative w-full">
                        <input type="search" id="search-dropdown" className="block p-3 w-full z-20 text-sm text-gray-900 bg-gray-50 rounded-full border-s-2 border border-gray-300 focus:ring-blue-500 focus:border-blue-500" placeholder="Search..." required />
                        <button type="submit" className="absolute top-0 end-0 p-3 text-sm font-medium h-full focus:ring-4 focus:outline-none focus:ring-blue-300">
                            <MagnifyingGlassIcon className='w-4 h-4'/>
                            <span className="sr-only">Search</span>
                        </button>
                    </div>
                </div>
            </div>
            
            <DataTable 
                columns={columns} 
                data={records} 
                fixedHeader
                pagination 
                selectableRows 
            />
        </>
		
	);
};

