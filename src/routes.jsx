import React from "react";
import { Routes, BrowserRouter, Route, redirect, Navigate } from "react-router-dom";
// layout
import DashboardLayout from '../src/layouts/DashboardLayout';
import DiscoveryLayout from '../src/layouts/DiscoveryLayout';
import TokensLayout from '../src/layouts/TokensLayout';
// layout

import Dashboard from "./components/dashboard/Dashboard";
import Discovery from "./components/discovery/Discovery";
import DiccoverSsl from "./components/discovery/Ssl";
import Smtp from "./components/discovery/Smtp";
import Linux from "./components/discovery/Linux";
import WindowsPath from "./components/discovery/WindowsPath";
import Ad from "./components/discovery/Ad";
import CertStore from "./components/discovery/CertStore";
import Agent from "./components/discovery/Agent";
import MDM from "./components/discovery/ManageEngineMDM";
import Azure from "./components/discovery/Azure";
import AWS from "./components/discovery/AWS";

import Tokens from "./components/tokens/Tokens";
import SelectedToken from "./components/tokens/SelectedToken";

import SSHLayout from "./layouts/SSHLayout";
import Ssh from "./components/ssh/Ssh";

import SSLLayout from "./layouts/SSLLayout";
import Ssl from "./components/ssl/Ssl";
import SshServer from "./components/ssh/SshServer";
import SshKeys from "./components/ssh/SshKeys";
import SshUsers from "./components/ssh/SshUsers";
import DiscoveredKeys from "./components/ssh/DiscoveredKeys";
import LandingServers from "./components/ssh/LandingServers";
import SshResourceGroup from "./components/ssh/ResourceGroup";
import SshAddKeyGroup from "./components/ssh/AddKeyGroup";
import SshAddResourceGroup from "./components/ssh/AddResourceGroup";
import CreateSSHKey from "./components/ssh/CreateSSHKey";
import SshKeyHistory from "./components/ssh/KeyHistory";
import SshKeyGroup from "./components/ssh/KeyGroup";
import SshUserGroup from "./components/ssh/UserGroup";
import SshAddUserGroup from "./components/ssh/AddUserGroup";
import KeyAssociationAudit from "./components/ssh/KeyAssociationAudit";
import KeyRotationAudit from "./components/ssh/KeyRotationAudit";
import SslCertificate from "./components/ssl/SslCertificate";
import SslCreateCertificate from "./components/ssl/SslCreateCertificate";
import SslCertificatePrivateCA from "./components/ssl/SslCertificatePrivateCA";
import SslCSR from "./components/ssl/SslCSR";
import SslCertificateWindowsAgents from "./components/ssl/SslCertificateWindowsAgents";
import SslRootCertificate from "./components/ssl/SslRootCertificate";
import SslCertificateGroup from "./components/ssl/SslCertificateGroup";
import AddCertificateGroup from "./components/ssl/AddCertificateGroup";
import CreateCsr from "./components/ssl/CreateCsr";
import CsrTemplate from "./components/ssl/CsrTemplate";
import CertificateRequest from "./components/ssl/CertificateRequest";
import ACMELetsEncrypt from "./components/ssl/ACMELetsEncrypt";
import ACMEBuyPassGoSSL from "./components/ssl/ACMEBuyPassGoSSL";
import ACMEZeroSSL from "./components/ssl/ACMEZeroSSL";
import SslGoDaddy from "./components/ssl/SslGoDaddy";
import SslStore from "./components/ssl/SslStore";
import SslStoreSectigo from "./components/ssl/SslStoreSectigo";
import SslStoreSymantec from "./components/ssl/SslStoreSymantec";
import SslStoreDigiCert from "./components/ssl/SslStoreDigiCert";
import SslDigiCert from "./components/ssl/SslDigiCert";
import SslGlobalSign from "./components/ssl/SslGlobalSign";
import GlobalCreateOrder from "./components/ssl/GlobalCreateOrder";
import DigiCertCreateOrder from "./components/ssl/DigiCertCreateOrder";
import SslMDMCertificate from "./components/ssl/SslMDMCertificate";
import SslAWSCertificate from "./components/ssl/SslAWSCertificate";
import SslSectigo from "./components/ssl/SslSectigo";
import LetsEncryptCR from "./components/ssl/LetsEncryptCR";
import BuyPassGoCR from "./components/ssl/ByPassGoCR";
import ZeroSSLCR from "./components/ssl/ZeroSSLCR";
import LetsEncryptManage from "./components/ssl/LetsEncryptManage";
import BuyPassGoSSLManage from "./components/ssl/BuyPassGoSSLManage";
import ZeroSSLManage from "./components/ssl/ZeroSSLManage";
import GodaddyOrderCertificate from "./components/ssl/GodaddyOrderCertificate";
import StoreDigiCertOrderCertificate from "./components/ssl/StoreDigiCertOrderCertificate";
import StoreSymantecOrderCertificate from "./components/ssl/StoreSymantecOrderCertificate";
import StoreSectigoOrderCertificate from "./components/ssl/StoreSectigoOrderCertificate";
import StoreOrderCertificate from "./components/ssl/StoreOrderCertificate";
import DigiCertManage from "./components/ssl/DigiCertManage";
import GlobalSignManage from "./components/ssl/GlobalSignManage";
import MDMDeployment from "./components/ssl/MDMDeployment";
import AWSPublic from "./components/ssl/AWSPublic";
import AWSPrivate from "./components/ssl/AWSPrivate";
import AWSRequestStatus from "./components/ssl/AWSRequestStatus";
import AWSDiscovery from "./components/ssl/AWSDiscovery";
import AWSManage from "./components/ssl/AWSManage";
import SslMSCA from "./components/ssl/SslMSCA";
import SectigoCreateOrder from "./components/ssl/SectigoCreateOrder";
import SectigoManage from "./components/ssl/SectigoManage";
import SslAzure from "./components/ssl/SslAzure";
import AzureRequest from "./components/ssl/AzureRequest";
import AzureManage from "./components/ssl/AzureManage";
import AzureSecrets from "./components/ssl/AzureSecrets";
import SslKubernetes from "./components/ssl/SslKubernetes";
import KubernetesManage from "./components/ssl/KubernetesManage";
import SslEntrust from "./components/ssl/SslEntrust";
import EntrustManage from "./components/ssl/EntrustManage";
import EntrustRequest from "./components/ssl/EntrustRequest";
import KeyVaultLayout from "./layouts/KeyVaultLayout";
import KeyVault from "./components/key-vault/KeyVault";
import PgpKeys from "./components/key-vault/PgpKeys";
import AuditLayout from "./layouts/AuditLayout";
import Audit from "./components/audit/Audit";
import ReportsLayout from "./layouts/ReportsLayout";
import Reports from "./components/reports/Reports";
import SSHResourceReport from "./components/reports/SSHResourceReport";
import LandingServersReport from "./components/reports/LandingServersReport";
import PrivateKeyReport from "./components/reports/PrivateKeyReport";
import KeyRotationReport from "./components/reports/KeyRotationReport";
import KeyDeploymentReport from "./components/reports/KeyDeploymentReport";
import ServerAccessReport from "./components/reports/ServerAccessReport";
import SshUsersReport from "./components/reports/SshUsersReport";
import SSLCertificatesReport from "./components/reports/SSLCertificatesReport";
import SSLRequestReport from "./components/reports/SSLRequestReport";
import WildcardSSLCertificatesReport from "./components/reports/WildcardSSLCertificatesReport";
import DeployedServers from "./components/reports/DeployedServers";
import CertificatesSyncStatusReport from "./components/reports/CertificatesSyncStatusReport";
import ADUserCertificatesReport from "./components/reports/ADUserCertificatesReport";
import CertificateSignReport from "./components/reports/CertificateSignReport";
import SHA1CertificateReport from "./components/reports/SHA1CertificateReport";
import DeploymentReport from "./components/reports/DeploymentReport";
import SSLVulnerabilityReport from "./components/reports/SSLVulnerabilityReport";
import CertificateRenewalReport from "./components/reports/CertificateRenewalReport";
import AWSCertificateReport from "./components/reports/AWSCertificateReport";
import AWSCertificateRequestReport from "./components/reports/AWSCertificateRequestReport";
import AzureCertificatesReport from "./components/reports/AzureCertificatesReport";
import AzureCertificateRequestsReport from "./components/reports/AzureCertificateRequestsReport";
import KubernetesTLSSecretsReport from "./components/reports/KubernetesTLSSecretsReport";
import LoadBalancerDeploymentReport from "./components/reports/LoadBalancerDeploymentReport";
import AzureTLSSecretsReport from "./components/reports/AzureTLSSecretsReport";
import MDMCertificatesReport from "./components/reports/MDMCertificatesReport";
import MSCARevokeandDeleteReport from "./components/reports/MSCARevokeandDeleteReport";
import MSCACertificatesReport from "./components/reports/MSCACertificatesReport";
import JenkinsAccessReport from "./components/reports/JenkinsAccessReport";
import PrivateCAReport from "./components/reports/PrivateCAReport";
import LetsEncryptRequestsReport from "./components/reports/LetsEncryptRequestsReport";
import LetsEncryptCertificatesReport from "./components/reports/LetsEncryptCertificatesReport";
import BuypassGoSSLRequestsReport from "./components/reports/BuypassGoSSLRequestsReport";
import BuypassGoSSLCertificatesReport from "./components/reports/BuypassGoSSLCertificatesReport";
import ZeroSSLRequestsReport from "./components/reports/ZeroSSLRequestsReport";
import ZeroSSLCertificatesReport from "./components/reports/ZeroSSLCertificatesReport";
import ACMERequestsReport from "./components/reports/ACMERequestsReport";
import ACMECertificatesReport from "./components/reports/ACMECertificatesReport";
import GoDaddyOrdersReport from "./components/reports/GoDaddyOrdersReport";
import TheSSLStoreOrdersReport from "./components/reports/TheSSLStoreOrdersReport";
import DigiCertOrdersReport from "./components/reports/DigiCertOrdersReport";
import GlobalSignOrdersReport from "./components/reports/GlobalSignOrdersReport";
import SectigoOrdersReport from "./components/reports/SectigoOrdersReport";
import EntrustOrdersReport from "./components/reports/EntrustOrdersReport";

import AllKeysReport from "./components/reports/AllKeysReport";
import AuditReport from "./components/reports/AuditReport";
import KeyVaultReport from "./components/reports/KeyVaultReport";
import PGPKeysReport from "./components/reports/PGPKeysReport";

import SettingsLayout from "./layouts/SettingsLayout";
import UMLayout from "./layouts/UmLayout";
import Users from "./components/settings/user-management/Users";
import CreateUser from "./components/settings/user-management/CreateUser";
import EditUser from "./components/settings/user-management/EditUser";
import LDAPServers from "./components/settings/user-management/LDAPServers";
import ActiveDirectoryImport from "./components/settings/user-management/ActiveDirectoryImport";
import RadiusServer from "./components/settings/user-management/RadiusServer";
import Sso from "./components/settings/user-management/Sso";
import MailServerSettings from "./components/settings/general-settings/MailServerSettings";
import ProxyServerSettings from "./components/settings/general-settings/ProxyServerSettings";
import SNMP from "./components/settings/general-settings/SNMP";
import SysLog from "./components/settings/general-settings/SysLog";
import DatabaseBackup from "./components/settings/general-settings/DatabaseBackup";
import Rebrand from "./components/settings/general-settings/Rebrand";
import APIKey from "./components/settings/general-settings/APIKey";
import DashboardSettings from "./components/settings/general-settings/DashboardSettings";
import ServerCertificate from "./components/settings/general-settings/ServerCertificate";
import METracker from "./components/settings/general-settings/METracker";
import PurgeAuditTrails from "./components/settings/privacy-settings/PurgeAuditTrails";
import PasswordProtectionforExports from "./components/settings/privacy-settings/PasswordProtectionforExports";
import DataPrivacyforExports from "./components/settings/privacy-settings/DataPrivacyforExports";
import UnmappedEmailIds from "./components/settings/privacy-settings/UnmappedEmailIds";
import PolicyConfiguration from "./components/settings/ssh/PolicyConfiguration";
import CertificateHistory from "./components/settings/ssl/CertificateHistory ";
import SSLVulnerability from "./components/settings/ssl/SSLVulnerability";
import SSLFingerprint from "./components/settings/ssl/SSLFingerprint";
import CertificateRenewal from "./components/settings/ssl/CertificateRenewal";
import CertificatesSyncStatus from "./components/settings/ssl/CertificatesSyncStatus";
import ACMEProviders from "./components/settings/ssl/ACMEProviders";
import ExcludedCertificates from "./components/settings/ssl/ExcludedCertificates";
import IISBinding from "./components/settings/ssl/IISBinding";
import SigningApprovalSettings from "./components/settings/ssl/SigningApprovalSettings";
import NotificationPolicy from "./components/settings/notification/NotificationPolicy";
import AuditNotification from "./components/settings/notification/AuditNotification";
import AdditionalFields from "./components/settings/AdditionalFields";
import ChangePassword from "./components/settings/ChangePassword";
import TFAuth from "./components/settings/TFAuth";
import Ticketing from "./components/settings/Ticketing";
import ServerDetails from "./components/settings/ServerDetails";
import DomainWhoIsData from "./components/settings/DomainWhoIsData";
import Schedule from "./components/schedule/Schedule";
import AddSchedule from "./components/schedule/AddSchedule";
import AuditSchedule from "./components/schedule/AuditSchedule";
import ParseCSR from "./components/tools/ParseCSR";
import ParseCertificate from "./components/tools/ParseCertificate";
import ConvertCertificate from "./components/tools/ConvertCertificate";
import ScanVulnerabilities from "./components/tools/ScanVulnerabilities";

export default function Router({ setLoader }){
  return (
    <React.Suspense fallback={
        <div className="grid place-items-center fixed inset-0">Loading...</div>
      }
    >
        <BrowserRouter>
            <Routes>
                <Route element={<DashboardLayout />}>
                    <Route index element={<Dashboard setLoader={setLoader} />} />
                </Route>
                <Route path={"discovery"} element={<DiscoveryLayout />}>
                    <Route index element={<Navigate to="/discovery/ssh" replace />}></Route>
                    <Route path={"ssh"} element={<Discovery setLoader={setLoader} />} />
                    <Route path={"ssl"} element={<DiccoverSsl setLoader={setLoader} />} />
                    <Route path={"smtp"} element={<Smtp setLoader={setLoader} />} />
                    <Route path={"linux"} element={<Linux setLoader={setLoader} />} />
                    <Route path={"windows-path"} element={<WindowsPath setLoader={setLoader} />} />
                    <Route path={"ad"} element={<Ad setLoader={setLoader} />} />
                    <Route path={"cert-store"} element={<CertStore setLoader={setLoader} />} />
                    <Route path={"agent"} element={<Agent setLoader={setLoader} />} />
                    <Route path={"aws"} element={<AWS setLoader={setLoader} />} />
                    <Route path={"mdm"} element={<MDM setLoader={setLoader} />} />
                    <Route path={"azure"} element={<Azure setLoader={setLoader} />} />
                </Route>
                <Route path={"tokens"} element={<TokensLayout setLoader={setLoader} />}>
                    <Route index element={<Tokens setLoader={setLoader}/>}></Route>
                    <Route path={"selected"} element={<SelectedToken setLoader={setLoader}/>}></Route>
                </Route>
                <Route path={"ssh"} element={<SSHLayout setLoader={setLoader} />}>
                    <Route index element={<Navigate to="/ssh/server" replace />}></Route>
                    <Route path={"server"}element={<SshServer setLoader={setLoader}/>}></Route>
                        <Route path={"resource-group"} element={<SshResourceGroup setLoader={setLoader}/>}></Route>
                        <Route path={"add-resource-group"} element={<SshAddResourceGroup setLoader={setLoader}/>}></Route>
                    <Route path={"keys"}element={<SshKeys setLoader={setLoader}/>}></Route>
                        <Route path={"create-ssh-key"} element={<CreateSSHKey setLoader={setLoader}/>}></Route>
                        <Route path={"key-history"} element={<SshKeyHistory setLoader={setLoader}/>}></Route>
                        <Route path={"key-group"} element={<SshKeyGroup setLoader={setLoader}/>}></Route>
                        <Route path={"add-key-group"} element={<SshAddKeyGroup setLoader={setLoader}/>}></Route>
                        <Route path={"key-association-audit"} element={<KeyAssociationAudit setLoader={setLoader}/>}></Route>
                        <Route path={"key-rotation-audit"} element={<KeyRotationAudit setLoader={setLoader}/>}></Route>
                    <Route path={"users"}element={<SshUsers setLoader={setLoader}/>}></Route>
                        <Route path={"user-group"}element={<SshUserGroup setLoader={setLoader}/>}></Route>
                        <Route path={"add-user-group"}element={<SshAddUserGroup setLoader={setLoader}/>}></Route>
                    <Route path={"discovered-keys"}element={<DiscoveredKeys setLoader={setLoader}/>}></Route>
                    <Route path={"landing-servers"}element={<LandingServers setLoader={setLoader}/>}></Route>
                </Route>
                <Route path={"ssl"} element={<SSLLayout setLoader={setLoader} />}>
                    <Route index element={<Navigate to="/ssl/certificate" replace />}></Route>
                    <Route path={"certificate"} element={<SslCertificate setLoader={setLoader}/>}></Route>
                        <Route path={"create-certificate"} element={<SslCreateCertificate setLoader={setLoader}/>}></Route>
                        <Route path={"private-ca"} element={<SslCertificatePrivateCA setLoader={setLoader}/>}></Route>
                        <Route path={"windows-agents"} element={<SslCertificateWindowsAgents setLoader={setLoader}/>}></Route>
                        <Route path={"root-certificate"} element={<SslRootCertificate setLoader={setLoader}/>}></Route>
                        <Route path={"certificate-group"} element={<SslCertificateGroup setLoader={setLoader}/>}></Route>
                        <Route path={"add-certificate-group"} element={<AddCertificateGroup setLoader={setLoader}/>}></Route>
                    <Route path={"csr"} element={<SslCSR setLoader={setLoader}/>}></Route>
                        <Route path={"create-csr"} element={<CreateCsr setLoader={setLoader}/>}></Route>
                        <Route path={"csr-template"} element={<CsrTemplate setLoader={setLoader}/>}></Route>
                        <Route path={"certificate-request"} element={<CertificateRequest setLoader={setLoader}/>}></Route>
                            <Route path={"acme-lets-encrypt"} element={<ACMELetsEncrypt setLoader={setLoader}/>}></Route>
                                <Route path={"lets-encrypt-cr"} element={<LetsEncryptCR setLoader={setLoader}/>}></Route>
                                <Route path={"lets-encrypt-manage"} element={<LetsEncryptManage setLoader={setLoader}/>}></Route>
                            <Route path={"acme-buy-pass-go-ssl"} element={<ACMEBuyPassGoSSL setLoader={setLoader}/>}></Route>
                                <Route path={"buy-pass-go-cr"} element={<BuyPassGoCR setLoader={setLoader}/>}></Route>
                                <Route path={"buy-pass-go-manage"} element={<BuyPassGoSSLManage setLoader={setLoader}/>}></Route>
                            <Route path={"acme-zero-ssl"} element={<ACMEZeroSSL setLoader={setLoader}/>}></Route>
                                <Route path={"zero-ssl-cr"} element={<ZeroSSLCR setLoader={setLoader}/>}></Route>
                                <Route path={"zero-ssl-manage"} element={<ZeroSSLManage setLoader={setLoader}/>}></Route>
                    <Route path={"godaddy"} element={<SslGoDaddy setLoader={setLoader}/>}></Route>
                        <Route path={"godaddy-order-certificate"} element={<GodaddyOrderCertificate setLoader={setLoader}/>}></Route>
                    <Route path={"store"} element={<SslStore setLoader={setLoader}/>}></Route>
                        <Route path={"store-order-certificate"} element={<StoreOrderCertificate setLoader={setLoader}/>}></Route>
                    <Route path={"store-sectigo"} element={<SslStoreSectigo setLoader={setLoader}/>}></Route>
                        <Route path={"store-sectigo-order-certificate"} element={<StoreSectigoOrderCertificate setLoader={setLoader}/>}></Route>
                    <Route path={"store-symantec"} element={<SslStoreSymantec setLoader={setLoader}/>}></Route>
                        <Route path={"store-symantec-order-certificate"} element={<StoreSymantecOrderCertificate setLoader={setLoader}/>}></Route>
                    <Route path={"store-digi-cert"} element={<SslStoreDigiCert setLoader={setLoader}/>}></Route>
                        <Route path={"store-digi-cert-order-certificate"} element={<StoreDigiCertOrderCertificate setLoader={setLoader}/>}></Route>
                    <Route path={"digi-cert"} element={<SslDigiCert setLoader={setLoader}/>}></Route>
                        <Route path={"digicert-order"} element={<DigiCertCreateOrder setLoader={setLoader}/>}></Route>
                        <Route path={"digi-cert-manage"} element={<DigiCertManage setLoader={setLoader}/>}></Route>
                    <Route path={"global-sign"} element={<SslGlobalSign setLoader={setLoader}/>}></Route>
                        <Route path={"global-sign-order"} element={<GlobalCreateOrder setLoader={setLoader}/>}></Route>
                        <Route path={"global-sign-manage"} element={<GlobalSignManage setLoader={setLoader}/>}></Route>
                    <Route path={"mdm"} element={<SslMDMCertificate setLoader={setLoader}/>}></Route>
                        <Route path={"mdm-deployment"} element={<MDMDeployment setLoader={setLoader}/>}></Route>
                    <Route path={"aws"} element={<SslAWSCertificate setLoader={setLoader}/>}></Route>
                        <Route path={"aws-public"} element={<AWSPublic setLoader={setLoader}/>}></Route>
                        <Route path={"aws-private"} element={<AWSPrivate setLoader={setLoader}/>}></Route>
                        <Route path={"aws-request-status"} element={<AWSRequestStatus setLoader={setLoader}/>}></Route>
                        <Route path={"aws-discovery"} element={<AWSDiscovery setLoader={setLoader}/>}></Route>
                        <Route path={"aws-manage"} element={<AWSManage setLoader={setLoader}/>}></Route>
                    <Route path={"msca"} element={<SslMSCA setLoader={setLoader}/>}></Route>
                    <Route path={"sectigo"} element={<SslSectigo setLoader={setLoader}/>}></Route>
                        <Route path={"sectigo-order"} element={<SectigoCreateOrder setLoader={setLoader}/>}></Route>
                        <Route path={"sectigo-manage"} element={<SectigoManage setLoader={setLoader}/>}></Route>
                    <Route path={"azure"} element={<SslAzure setLoader={setLoader}/>}></Route>    
                        <Route path={"azure-request"} element={<AzureRequest setLoader={setLoader}/>}></Route>  
                        <Route path={"azure-secrets"} element={<AzureSecrets setLoader={setLoader}/>}></Route>  
                        <Route path={"azure-manage"} element={<AzureManage setLoader={setLoader}/>}></Route>  
                    <Route path={"kubernetes"} element={<SslKubernetes setLoader={setLoader}/>}></Route>  
                        <Route path={"kubernetes-manage"} element={<KubernetesManage setLoader={setLoader}/>}></Route>  
                    <Route path={"entrust"} element={<SslEntrust setLoader={setLoader}/>}></Route>  
                        <Route path={"entrust-request"} element={<EntrustRequest setLoader={setLoader}/>}></Route>
                        <Route path={"entrust-manage"} element={<EntrustManage setLoader={setLoader}/>}></Route>  
                </Route>
                <Route path={"key-vault"} element={<KeyVaultLayout setLoader={setLoader} />}>
                    <Route index element={<Navigate to="/key-vault/store-key" replace />}></Route>
                    <Route path={"store-key"} element={<KeyVault setLoader={setLoader} />} />
                    <Route path={"pgp-keys"} element={<PgpKeys setLoader={setLoader}/>} />
                </Route>
                <Route path={"audit"} element={<AuditLayout setLoader={setLoader} />}>
                    <Route index element={<Audit to="/audit" replace />}></Route>
                </Route>
                <Route path={"reports"} element={<ReportsLayout setLoader={setLoader} />}>
                    <Route index element={<Reports setLoader={setLoader} />} />
                    <Route path={"ssh-resource-report"} element={<SSHResourceReport setLoader={setLoader} />} />
                    <Route path={"landing-servers-report"} element={<LandingServersReport setLoader={setLoader} />} />
                    <Route path={"private-key-report"} element={<PrivateKeyReport setLoader={setLoader} />} />
                    <Route path={"key-rotation-report"} element={<KeyRotationReport setLoader={setLoader} />} />
                    <Route path={"key-deployment-report"} element={<KeyDeploymentReport setLoader={setLoader} />} />
                    <Route path={"server-access-report"} element={<ServerAccessReport setLoader={setLoader} />} />
                    <Route path={"ssh-users-report"} element={<SshUsersReport setLoader={setLoader} />} />
                    <Route path={"ssl-certificates-report"} element={<SSLCertificatesReport setLoader={setLoader} />} />
                    <Route path={"ssl-request-report"} element={<SSLRequestReport setLoader={setLoader} />} />
                    <Route path={"wildcard-ssl-certificates-report"} element={<WildcardSSLCertificatesReport setLoader={setLoader} />} />
                    <Route path={"deployed-servers"} element={<DeployedServers setLoader={setLoader} />} />
                    <Route path={"certificates-sync-status-report"} element={<CertificatesSyncStatusReport setLoader={setLoader} />} />
                    <Route path={"ad-user-certificates-report"} element={<ADUserCertificatesReport setLoader={setLoader} />} />
                    <Route path={"certificate-sign-report"} element={<CertificateSignReport setLoader={setLoader} />} />
                    <Route path={"sha1-certificate-report"} element={<SHA1CertificateReport setLoader={setLoader} />} />
                    <Route path={"deployment-report"} element={<DeploymentReport setLoader={setLoader} />} />
                    <Route path={"ssl-vulnerability-report"} element={<SSLVulnerabilityReport setLoader={setLoader} />} />
                    <Route path={"certificate-renewal-report"} element={<CertificateRenewalReport setLoader={setLoader} />} />
                    <Route path={"aws-certificate-report"} element={<AWSCertificateReport setLoader={setLoader} />} />
                    <Route path={"aws-certificate-request-report"} element={<AWSCertificateRequestReport setLoader={setLoader} />} />
                    <Route path={"azure-certificates-report"} element={<AzureCertificatesReport setLoader={setLoader} />} />
                    <Route path={"azure-certificate-requests-report"} element={<AzureCertificateRequestsReport setLoader={setLoader} />} />
                    <Route path={"kubernetes-tls-secrets-report"} element={<KubernetesTLSSecretsReport setLoader={setLoader} />} />
                    <Route path={"loadbalancer-deployment-report"} element={<LoadBalancerDeploymentReport setLoader={setLoader} />} />
                    <Route path={"azure-tls-secrets-report"} element={<AzureTLSSecretsReport setLoader={setLoader} />} />
                    <Route path={"mdm-certificates-report"} element={<MDMCertificatesReport setLoader={setLoader} />} />
                    <Route path={"msca-revoke-and-delete-report"} element={<MSCARevokeandDeleteReport setLoader={setLoader} />} />
                    <Route path={"msca-certificates-report"} element={<MSCACertificatesReport setLoader={setLoader} />} />
                    <Route path={"jenkins-access-report"} element={<JenkinsAccessReport setLoader={setLoader} />} />
                    <Route path={"private-ca-report"} element={<PrivateCAReport setLoader={setLoader} />} />
                    
                    <Route path={"lets-encrypt-requests-report"}  element={<LetsEncryptRequestsReport setLoader={setLoader} />} />
                    <Route path={"lets-encrypt-certificates-report"}  element={<LetsEncryptCertificatesReport setLoader={setLoader} />} />
                    <Route path={"buypass-go-ssl-requests-report"}  element={<BuypassGoSSLRequestsReport setLoader={setLoader} />} />
                    <Route path={"buypass-go-ssl-certificates-report"}  element={<BuypassGoSSLCertificatesReport setLoader={setLoader} />} />
                    <Route path={"zerossl-requests-report"}  element={<ZeroSSLRequestsReport setLoader={setLoader} />} />
                    <Route path={"zerossl-certificates-report"}  element={<ZeroSSLCertificatesReport setLoader={setLoader} />} />
                    <Route path={"acme-requests-report"}  element={<ACMERequestsReport setLoader={setLoader} />} />
                    <Route path={"acme-certificates-report"}  element={<ACMECertificatesReport setLoader={setLoader} />} />
                    <Route path={"godaddy-orders-report"}  element={<GoDaddyOrdersReport setLoader={setLoader} />} />
                    <Route path={"the-ssl-store-orders-report"}  element={<TheSSLStoreOrdersReport setLoader={setLoader} />} />
                    <Route path={"digiCert-orders-report"}  element={<DigiCertOrdersReport setLoader={setLoader} />} />
                    <Route path={"globalsign-orders-report"}  element={<GlobalSignOrdersReport setLoader={setLoader} />} />
                    <Route path={"sectigo-orders-report"}  element={<SectigoOrdersReport setLoader={setLoader} />} />
                    <Route path={"entrust-orders-report"}  element={<EntrustOrdersReport setLoader={setLoader} />} />

                    <Route path={"all-keys-report"}  element={<AllKeysReport setLoader={setLoader} />} />
                    <Route path={"audit-report"}  element={<AuditReport setLoader={setLoader} />} />
                    <Route path={"key-vault-report"}  element={<KeyVaultReport setLoader={setLoader} />} />
                    <Route path={"pgp-keys-report"}  element={<PGPKeysReport setLoader={setLoader} />} />
                </Route>

                <Route path={"settings"} element={<SettingsLayout setLoader={setLoader} />}>
                    <Route index element={<Navigate to="/settings/user-management/users" replace />}></Route>
                    <Route path={"user-management"} element={<UMLayout setLoader={setLoader} />}>
                        <Route index element={<Navigate to="/settings/user-management/users" replace />}></Route>
                        <Route path={"users"} element={<Users setLoader={setLoader} />}></Route>
                            <Route path={"create-user"} element={<CreateUser setLoader={setLoader} />}></Route>
                            <Route path={"edit-user"} element={<EditUser setLoader={setLoader} />}></Route>
                        <Route path={"ldap"} element={<LDAPServers setLoader={setLoader} />}></Route>
                        <Route path={"active-directory-import"} element={<ActiveDirectoryImport setLoader={setLoader} />}></Route>
                        <Route path={"radius"} element={<RadiusServer setLoader={setLoader} />}></Route>
                        <Route path={"sso"} element={<Sso setLoader={setLoader} />}></Route>
                    </Route>
                    <Route path={"general-settings"} element={<UMLayout setLoader={setLoader} />}>
                        <Route index element={<Navigate to="/settings/general-settings/mail-server" replace />}></Route>
                        <Route path={"mail-server"} element={<MailServerSettings setLoader={setLoader} />}></Route>
                        <Route path={"proxy-server"} element={<ProxyServerSettings setLoader={setLoader} />}></Route>
                        <Route path={"snmp"} element={<SNMP setLoader={setLoader} />}></Route>
                        <Route path={"syslog"} element={<SysLog setLoader={setLoader} />}></Route>
                        <Route path={"database-backup"} element={<DatabaseBackup setLoader={setLoader} />}></Route>
                        <Route path={"rebrand"} element={<Rebrand setLoader={setLoader} />}></Route>
                        <Route path={"api-key"} element={<APIKey setLoader={setLoader} />}></Route>
                        <Route path={"dashboard-settings"} element={<DashboardSettings setLoader={setLoader} />}></Route>
                        <Route path={"server-certificate"} element={<ServerCertificate setLoader={setLoader} />}></Route>
                        <Route path={"me-tracker"} element={<METracker setLoader={setLoader} />}></Route>
                    </Route>
                    <Route path={"privacy-settings"} element={<UMLayout setLoader={setLoader} />}>
                        <Route index element={<Navigate to="/settings/privacy-settings/purge-audit-trails" replace />}></Route>
                        <Route path={"purge-audit-trails"} element={<PurgeAuditTrails setLoader={setLoader} />}></Route>
                        <Route path={"password-protection-for-exports"} element={<PasswordProtectionforExports setLoader={setLoader} />}></Route>
                        <Route path={"data-privacy-for-exports"} element={<DataPrivacyforExports setLoader={setLoader} />}></Route>
                        <Route path={"unmapped-email-ids"} element={<UnmappedEmailIds setLoader={setLoader} />}></Route>
                        
                    </Route>
                    <Route path={"ssh"} element={<UMLayout setLoader={setLoader} />}>
                        <Route index element={<Navigate to="/settings/ssh/policy-configuration" replace />}></Route>
                        <Route path={"policy-configuration"} element={<PolicyConfiguration setLoader={setLoader} />}></Route>
                    </Route>
                    <Route path={"ssl"} element={<UMLayout setLoader={setLoader} />}>
                        <Route index element={<Navigate to="/settings/ssl/certificate-history" replace />}></Route>
                        <Route path={"certificate-history"} element={<CertificateHistory setLoader={setLoader} />}></Route>
                        <Route path={"ssl-vulnerability"} element={<SSLVulnerability setLoader={setLoader} />}></Route>
                        <Route path={"ssl-fingerprint"} element={<SSLFingerprint setLoader={setLoader} />}></Route>
                        <Route path={"certificate-renewal"} element={<CertificateRenewal setLoader={setLoader} />}></Route>
                        <Route path={"certificates-sync-status"} element={<CertificatesSyncStatus setLoader={setLoader} />}></Route>
                        <Route path={"acme-providers"} element={<ACMEProviders setLoader={setLoader} />}></Route>
                        <Route path={"excluded-certificates"} element={<ExcludedCertificates setLoader={setLoader} />}></Route>
                        <Route path={"iis-binding"} element={<IISBinding setLoader={setLoader} />}></Route>
                        <Route path={"signing-approval-settings"} element={<SigningApprovalSettings setLoader={setLoader} />}></Route>
                    </Route>
                    <Route path={"notification"} element={<UMLayout setLoader={setLoader} />}>
                        <Route index element={<Navigate to="/settings/notification/notification-policy" replace />}></Route>
                        <Route path={"notification-policy"} element={<NotificationPolicy setLoader={setLoader} />}></Route>
                        <Route path={"audit"} element={<AuditNotification setLoader={setLoader} />}></Route>
                    </Route>
                    <Route path={"additional-fields"} element={<AdditionalFields setLoader={setLoader} />}></Route>
                    <Route path={"change-password"} element={<ChangePassword setLoader={setLoader} />}></Route>
                    <Route path={"tf-auth"} element={<TFAuth setLoader={setLoader} />}></Route>
                    <Route path={"ticketing"} element={<Ticketing setLoader={setLoader} />}></Route>
                    <Route path={"server-details"} element={<ServerDetails setLoader={setLoader} />}></Route>
                    <Route path={"domainexpirydata"} element={<DomainWhoIsData setLoader={setLoader} />}></Route>
                    
                </Route>
                <Route path={"schedule"} element={<ReportsLayout setLoader={setLoader} />}>
                    <Route index element={<Schedule setLoader={setLoader} />} />
                    <Route path={"add-schedule"} element={<AddSchedule setLoader={setLoader} />} />
                    <Route path={"audit-schedule"} element={<AuditSchedule setLoader={setLoader} />} />
                </Route>
                <Route path={"tools"} element={<ReportsLayout setLoader={setLoader} />}>
                    <Route index element={<Navigate to="/tools/parse-csr" replace />}></Route>
                    <Route path={"parse-csr"} element={<ParseCSR setLoader={setLoader} />} />
                    <Route path={"parse-certificate"} element={<ParseCertificate setLoader={setLoader} />} />
                    <Route path={"convert-certificate"} element={<ConvertCertificate setLoader={setLoader} />} />
                    <Route path={"scan-vulnerabilities"} element={<ScanVulnerabilities setLoader={setLoader} />} />
                </Route>
            </Routes>
        </BrowserRouter>
    </React.Suspense>
  );
};





