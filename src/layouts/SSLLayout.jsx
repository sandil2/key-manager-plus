import React, { Component, useState } from "react";
import Sidebar from "../components/Sidebar";
import { Link, NavLink, Outlet } from "react-router-dom";
import Header from "../components/Header"

import acmeLogo from "../images/acme-logo.svg" 
import goDaddyLogo from "../images/goDaddy.svg" 
import sslStore from "../images/ssl-store.svg" 
import sectigoLogo from "../images/sectigo.svg" 
import symantecLogo from "../images/symantec.svg" 
import digiCertLogo from "../images/digicert_dynamic_blade.svg"
import letsEncryptLogo from "../images/lets-encrypt-logo.svg" 
import byPassLogo from "../images/buypass.svg" 
import zeroSslLogo from "../images/zero-ssl.svg" 
import globalSignLogo from "../images/globalsign-logo.svg"
import MDMLogo from "../images/me-mdm.svg"
import AWSLogo from "../images/aws.svg"
import MSCALogo from "../images/MicrosoftCertificateAuthority.svg"
import AzureLogo from "../images/azure.svg"
import kubernetesLogo from "../images/kubernetes.svg"
import entrustLogo from "../images/entrust.svg"
import DropdownBorderless from "../components/DropdownBorderless";
import { Menu } from "@headlessui/react";

export default function SSLLayout(){
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const [sidebarShrink, setSidebarShrink] = useState(true);
    const [masterTab, setMasterTab] = useState("ssl-certificate");

      const navigation = [
        {
            name:'Certificates',
            href:"certificate",
            icon:"",
        },
        {
            name:'CSR',
            href:"csr",
            icon:"",
        },
        {
            name:'ACME',
            href:"javascript:;",
            icon:acmeLogo,
            children:[
                {
                    name:"Let's Encrypt",
                    href:"/ssl/acme-lets-encrypt",
                    icon:letsEncryptLogo,
                },
                {
                    name:"BuyPass Go SSL",
                    href:"/ssl/acme-buy-pass-go-ssl",
                    icon:byPassLogo,
                },
                {
                    name:"ZeroSSL",
                    href:"/ssl/acme-zero-ssl",
                    icon:zeroSslLogo,
                },
            ]
        },
        {
            name:'GoDaddy',
            href:"/ssl/godaddy",
            icon:goDaddyLogo,
        },
        {
            name:'The SSL Store',
            href:"/ssl/store",
            icon:sslStore,
            children:[
                {
                    name:"All Store",
                    href:"/ssl/store",
                    icon:sslStore,
                },
                {
                    name:"Sectigo (formerly Comodo)",
                    href:"/ssl/store-sectigo",
                    icon:sectigoLogo,
                },
                {
                    name:"Symantec",
                    href:"/ssl/store-symantec",
                    icon:symantecLogo,
                },
                {
                    name:"DigiCert",
                    href:"/ssl/store-digi-cert",
                    icon:digiCertLogo,
                },
            ]
        },
        {
            name:'DigiCert',
            href:"/ssl/digi-cert",
            icon:digiCertLogo,
        },
        {
            name:'Global Sign',
            href:"/ssl/global-sign",
            icon:globalSignLogo,
        },
        {
            name:'MDM',
            href:"/ssl/mdm",
            icon:MDMLogo,
        },
        {
            name:'AWS',
            href:"/ssl/aws",
            icon:AWSLogo,
        },
        {
            name:'MSCA',
            href:"/ssl/msca",
            icon:MSCALogo,
        },
        {
            name:'Azure',
            href:"/ssl/azure",
            icon:AzureLogo,
            children:[
                {
                    name:'Azure Certificate',
                    href:"/ssl/azure",
                    icon:AzureLogo,
                },
                {
                    name:'Azure Secrets',
                    href:"/ssl/azure-secrets",
                    icon:AzureLogo,
                },
            ]
        },
        {
            name:'Sectigo',
            href:"/ssl/sectigo",
            icon:sectigoLogo,
        },
        {
            name:'Kubernetes',
            href:"/ssl/kubernetes",
            icon:kubernetesLogo,
        },
        {
            name:'Entrust',
            href:"/ssl/entrust",
            icon:entrustLogo,
        },
    ];
    return(
        <>
            <div>
                
                <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} setSidebarShrink={setSidebarShrink} sidebarShrink={sidebarShrink}/>
                <div className="w-full">
                    <div>
                        <div className={`${sidebarShrink ? "lg:pl-72" : "lg:pl-24" } bg-theme-gray2`}>
                            <div className='bg-theme-gray2'>
                                <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
                            </div>
                            <div className='bg-white'>
                                <main className="py-10">
                                    {/* <div className="pl-4 sm:pl-6 lg:pl-8 flex justify-between items-center pr-12 border border-b border-t-0 border-l-0 border-r-0">
                                        <ul className='flex gap-y-3 text-xs h-7 gap-x-3 w-full'>
                                            <li className={`
                                            ${masterTab == "ssl-certificate" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}
                                            ${masterTab == "ssl-create-certificate" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}
                                            ${masterTab == "ssl-create-certificate-private-ca" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}
                                            ${masterTab == "ssl-create-certificate-windows-agents" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}
                                            ${masterTab == "ssl-root-certificate" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}
                                            ${masterTab == "ssl-certificate-group" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}
                                            ${masterTab == "ssl-add-certificate-group" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}
                                            `}>
                                                <a href="javascript:;" onClick={()=>setMasterTab('ssl-certificate')}>
                                                    <div className='flex items-center'>
                                                        <div className='w-1 h-5 flex items-center pr-1'>&nbsp;</div> <span>Certificates</span>
                                                    </div>
                                                </a>
                                            </li>
                                            <li className={`
                                            ${masterTab == "ssl-csr" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}
                                            `}>
                                                <a href="javascript:;" onClick={()=> setMasterTab("ssl-csr")}>
                                                    <div className='flex items-center'>
                                                        <div className='w-1 h-5 flex items-center pr-1'>&nbsp;</div> <span>CSR</span>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <DropdownBorderless DropText={"ACME"} DropIcon={<img src={acmeLogo} alt="acme"/>}>
                                                    <div className="px-1 py-1 " role="none">
                                                        <Menu.Item>
                                                            {({ active }) => (
                                                            <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm items-center`}>
                                                                <div className='w-4 h-4 mr-1'><img src={letsEncryptLogo} alt="" /></div> Let's Encrypt
                                                            </button>
                                                            )}
                                                        </Menu.Item>
                                                        <Menu.Item>
                                                            {({ active }) => (
                                                            <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm items-center`}>
                                                                <div className='w-4 h-4 mr-1'><img src={byPassLogo} alt="" /></div> Buypass Go SSL
                                                            </button>
                                                            )} 
                                                        </Menu.Item>
                                                        <Menu.Item>
                                                            {({ active }) => (
                                                            <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm items-center`}>
                                                                <div className='w-4 h-4 mr-1'><img src={zeroSslLogo} alt="" /></div> ZeroSSL
                                                            </button>
                                                            )} 
                                                        </Menu.Item>
                                                    </div>
                                                </DropdownBorderless>
                                            </li>
                                            <li className={`
                                                ${masterTab == "ssl-godaddy" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}
                                            `}>
                                                <a href="javascript:;" onClick={()=> setMasterTab("ssl-godaddy")}>
                                                    <div className='flex items-center'>
                                                        <div className='w-5 h-5 flex items-center pr-1'><img src={goDaddyLogo} alt="" /></div> <span>GoDaddy</span>
                                                    </div>
                                                </a>
                                            </li>
                                            <li  className={`${masterTab == "ssl-store" ? "border-t-0 border-l-0 border-r-0 border border-b-theme-red1": ""}`}>
                                                <DropdownBorderless DropText={"The SSL Store"} DropIcon={<img src={sslStore} alt="acme"/>}>
                                                    <div className="px-1 py-1 " role="none">
                                                        <Menu.Item onClick={()=>setMasterTab("ssl-store")}>
                                                            {({ active }) => (
                                                            <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                                                <span>All SSL Stores</span>
                                                            </button>
                                                            )}
                                                        </Menu.Item>
                                                        <Menu.Item>
                                                            {({ active }) => (
                                                            <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                                                <div className='w-4 h-4 mr-1'><img src={sectigoLogo} alt="" /></div> <span>Sectigo (formerly Comodo)</span>
                                                            </button>
                                                            )}
                                                        </Menu.Item>
                                                        <Menu.Item>
                                                            {({ active }) => (
                                                            <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                                                <div className='w-4 h-4 mr-1'><img src={symantecLogo} alt="" /></div> <span>Symantec</span>
                                                            </button>
                                                            )} 
                                                        </Menu.Item>
                                                        <Menu.Item>
                                                            {({ active }) => (
                                                            <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                                                <div className='w-4 h-4 mr-1'><img src={digiCertLogo} alt="" /></div> <span>DigiCert</span>
                                                            </button>
                                                            )} 
                                                        </Menu.Item>
                                                    </div>
                                                </DropdownBorderless>
                                            </li>
                                            <li>
                                                <div className='flex items-center'>
                                                    <div className='w-5 h-5 flex items-center pr-1'><img src={digiCertLogo} alt="" /></div> <span>DigiCert</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div className='flex items-center'>
                                                    <div className='w-5 h-5 flex items-center pr-1'><img src={globalSignLogo} alt="" /></div> <span>GlobalSign</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div className='flex items-center'>
                                                    <div className='w-5 h-5 flex items-center pr-1'><img src={MDMLogo} alt="" /></div> <span>MDM</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div className='flex items-center'>
                                                    <div className='w-5 h-5 flex items-center pr-1'><img src={AWSLogo} alt="" /></div> <span>AWS</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div className='flex items-center'>
                                                    <div className='w-5 h-5 flex items-center pr-1'><img src={MSCALogo} alt="" /></div> <span>MSCA</span>
                                                </div>
                                            </li>
                                            <li>
                                                <DropdownBorderless DropText={"ACME"} DropIcon={<img src={AzureLogo} alt="azure"/>}>
                                                    <div className="px-1 py-1 " role="none">
                                                        <Menu.Item>
                                                            {({ active }) => (
                                                            <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                                                <div className='w-4 h-4 mr-1'><img src={AzureLogo} alt="" /></div> <span>Azure Certificates</span>
                                                            </button>
                                                            )}
                                                        </Menu.Item>
                                                        <Menu.Item>
                                                            {({ active }) => (
                                                            <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                                                                <div className='w-4 h-4 mr-1'><img src={AzureLogo} alt="" /></div> <span>Azure Secrets</span>
                                                            </button>
                                                            )} 
                                                        </Menu.Item>
                                                    </div>
                                                </DropdownBorderless>
                                            </li>
                                            <li>
                                                <div className='flex items-center'>
                                                    <div className='w-4 h-5 flex items-center pr-1'><img src={sectigoLogo} alt="" /></div> <span>Sectigo</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div className='flex items-center'>
                                                    <div className='w-6 h-5 flex items-center pr-1'><img src={kubernetesLogo} alt="" /></div> <span>Kubernetes</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div className='flex items-center'>
                                                    <div className='w-6 h-5 flex items-center pr-1'><img src={entrustLogo} alt="" /></div> <span>Entrust</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div> */}
                                     <div className="px-4 sm:px-6 lg:px-8 flex justify-between items-center z-[20] relative">
                                        <ul role="list" className="flex text-xs gap-y-6 gap-x-3 w-full border border-b border-t-0 border-l-0 border-r-0 section-menu">
                                            {navigation.map((item) => (
                                                <li key={item.name}>
                                                    {!item.children && 
                                                        <NavLink
                                                            to={item.href}
                                                            onClick={()=>{
                                                            
                                                            }}
                                                            className={`pb-1 flex items-center`}
                                                            >
                                                            {item.icon && 
                                                                <div className='w-4 h-5 flex items-center pr-1'>
                                                                    <img src={item.icon} alt="" />
                                                                </div> 
                                                            }
                                                            <span>{item.name}</span>
                                                        </NavLink>
                                                    }
                                                    
                                                    {item.children &&
                                                        <DropdownBorderless DropText={item.name} DropIcon={<img src={item.icon} alt="azure"/>}>
                                                        {item.children.map((itm) => (
                                                            <>
                                                            <div className="px-1 py-1" role="none">
                                                                <Menu.Item>
                                                                    {({ active }) => (
                                                                        <Link to={itm.href}>
                                                                            <button className={`${active ? 'bg-[#448ec6] text-white' : 'text-gray-900'} group flex w-full items-center rounded-md px-2 py-2 text-xs`}>
                                                                                    <div className='w-5 h-5 mr-1 bg-white rounded p-1'><img src={itm.icon} className="h-full" alt="" /></div> <span>{itm.name}</span>
                                                                            </button>
                                                                        </Link>
                                                                    )}
                                                                </Menu.Item>
                                                            </div>
                                                            </>   
                                                        ))}
                                                        </DropdownBorderless>
                                                    }
                                                </li>
                                            ))}
                                        </ul>
                                    </div>
                                    <Outlet context={[masterTab, setMasterTab, sidebarOpen, setSidebarOpen, setSidebarShrink, sidebarShrink]} />
                                </main>    
                            </div>
                        </div>
                    </div>            
                    {/* <Outlet context={[masterTab, setMasterTab, sidebarOpen, setSidebarOpen, setSidebarShrink, sidebarShrink]} /> */}
                </div>
            </div>
        </>)
}