import React, { Component, useState } from "react";
import Sidebar from "../components/Sidebar";
import { NavLink, Outlet } from "react-router-dom";
import Header from "../components/Header";

export default function KeyVaultLayout(){
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const [sidebarShrink, setSidebarShrink] = useState(true);
    const [masterTab, setMasterTab] = useState("ssh-server");

    const navigation = [
        {
            name:'Key Vault',
            href:"/key-vault/store-key"
        },
        {
            name:'PGP Keys',
            href:"/key-vault/pgp-keys"
        },
    ];
    return(
        <>
            <div>
                <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} setSidebarShrink={setSidebarShrink} sidebarShrink={sidebarShrink}/>
                <div className="w-full">
                    <div>
                        <div className={`${sidebarShrink ? "lg:pl-72" : "lg:pl-24" } bg-theme-gray2`}>
                            <div className='bg-theme-gray2'>
                                <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
                            </div>
                            <div className='bg-white'>
                                <main className="py-10">
                                    <div className="px-4 sm:px-6 lg:px-8 flex justify-between items-center">
                                        <ul role="list" className="pb-3 flex gap-y-6 gap-x-7 w-full border border-b border-t-0 border-l-0 border-r-0 section-menu">
                                            {navigation.map((item) => (
                                            // {console.log(item.children[0].href)}
                                                <li key={item.name}>
                                                    <NavLink
                                                        to={item.href}
                                                        onClick={()=>{
                                                        
                                                        }}
                                                        className={`pb-3`}
                                                        >
                                                        <span>{item.name}</span>
                                                    </NavLink>
                                                </li>
                                            ))}
                                        </ul>
                                    </div>
                                    <Outlet context={[masterTab, setMasterTab, sidebarOpen, setSidebarOpen, setSidebarShrink, sidebarShrink]} />
                                </main>
                            </div>
                        </div>
                    </div>        
                </div>
            </div>
        </>)
}