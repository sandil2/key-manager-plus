import React, { Component, useState } from "react";
import Sidebar from "../components/Sidebar";
import { NavLink, Outlet } from "react-router-dom";
import Header from "../components/Header";

export default function SettingsLayout(){
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const [sidebarShrink, setSidebarShrink] = useState(true);
    const [masterTab, setMasterTab] = useState("ssh-server");

    return(
        <>
            <div>
                <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} setSidebarShrink={setSidebarShrink} sidebarShrink={sidebarShrink}/>
                <div className="w-full">
                    <div>
                        <div className={`${sidebarShrink ? "lg:pl-72" : "lg:pl-24" } bg-theme-gray2`}>
                            <div className='bg-theme-gray2'>
                                <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
                            </div>
                            <div className='bg-white'>
                                <main className="py-10">
                                    <Outlet context={[masterTab, setMasterTab, sidebarOpen, setSidebarOpen, setSidebarShrink, sidebarShrink]} />
                                </main>
                            </div>
                        </div>
                    </div>        
                </div>
            </div>
        </>)
}