import React, { Component, useState } from "react";
import Sidebar from "../components/Sidebar";
import { NavLink, Outlet } from "react-router-dom";
import Header from "../components/Header";

export default function UMLayout(){
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const [sidebarShrink, setSidebarShrink] = useState(true);
    const [masterTab, setMasterTab] = useState("ssh-server");

    return(
        <>
            <div>
               <Outlet context={[masterTab, setMasterTab, sidebarOpen, setSidebarOpen, setSidebarShrink, sidebarShrink]} />
            </div>
        </>)
}