import React, { Component, useState } from "react";
import Sidebar from "../components/Sidebar";
import { Outlet } from "react-router-dom";

export default function DiscoveryLayout(){
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const [sidebarShrink, setSidebarShrink] = useState(true);
    return(
         <div>
            <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} setSidebarShrink={setSidebarShrink} sidebarShrink={sidebarShrink}/>
            <div className="w-full">
                <Outlet context={[sidebarOpen, setSidebarOpen, setSidebarShrink, sidebarShrink]} />
            </div>
        </div>
    )
}