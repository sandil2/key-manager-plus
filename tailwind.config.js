/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",

    // Or if using `src` directory:
    "./src/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    fontFamily:{
       sans: [
        '"Inter", sans-serif',
        {
          fontFeatureSettings: '"cv11", "ss01"',
          fontVariationSettings: '"opsz" 32'
        },
      ],
    },
    extend: {
      colors:{
        'text-default-gray':"#4F5B67",
        'theme-gray1':'#f8f7fa',
        'theme-gray2':'#F1F1F1',
        'theme-gray3':'#717D8A',
        'theme-gray4':'#242D35',
        'theme-gray5':'#FAFAFA',
        'theme-gray6':'#373F47',
        'theme-gray7':'#F7F7FB',
        'theme-gray8':'#0C1116',
        'theme-red1':'#FF513A',
        'theme-blue1':'#448ec6',
      },
      // lineHeight: {
      //   'extra-loose': '2.5',
      //   '12': '4rem',
      // }
    },
  },
  plugins: [],
}
